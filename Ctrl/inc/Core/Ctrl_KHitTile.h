#ifndef _CTRL_CORE_KHITTILE_H_
#define _CTRL_CORE_KHITTILE_H_
/**
 * @file Ctrl_KHitTile.h
 * @author Trasgo Group
 * @brief KhitTile structure definitions.
 * @version 2.1
 * @date 2021-04-26
 * 
 * @copyright This software is provided to enhance knowledge and encourage progress in the scientific
 * community. It should be used only for research and educational purposes. Any reproduction
 * or use for commercial purpose, public redistribution, in source or binary forms, with or 
 * without modifications, is NOT ALLOWED without the previous authorization of the copyright 
 * holder. The origin of this software must not be misrepresented; you must not claim that you
 * wrote the original software. If you use this software for any purpose (e.g. publication),
 * a reference to the software package and the authors must be included.
 * 
 * @copyright THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDER AND CONTRIBUTORS "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 * THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * @copyright Copyright (c) 2007-2020, Trasgo Group, Universidad de Valladolid.
 * All rights reserved.
 * 
 * @copyright More information on http://trasgo.infor.uva.es/
 */

#define CTRL_FPGA_STRUCT_ALIGNMENT 16
///@cond INTERNAL
#ifndef CTRL_FPGA_KERNEL_FILE

#ifdef _CTRL_ARCH_PYNQ_
#include "pynq_api.h"
#endif

/**
 * Stripped down version of an abstract \e HitTile so it`s more suitable for use in kernels
 */
typedef struct {
	void		*data;			/**< Pointer to the data held by the tile. */
	int			origAcumCard[4];/**< Dimension accumulated cardinalities. */
	int			card[3];		/**< Dimension cardinalities. */
	int			offset;		/**< Offset to original data. For hierarchical subselections. */
#ifdef _CTRL_ARCH_PYNQ_
    PYNQ_SHARED_MEMORY pynq_shared_memory;
#endif
} KHitTile;
#endif

/* @author: Gabriel Rodriguez-Canal
   @brief: KHitTile wrapper to isolate tile coordinates and offset from data, as structures with pointers cannot be passed as arguments */
#ifdef _CTRL_ARCH_FPGA_
	typedef struct __attribute__((packed)) __attribute__((aligned(CTRL_FPGA_STRUCT_ALIGNMENT))) {
		int		origAcumCard[4];
		int 	card[3];
		int		offset;
	} KHitTile_fpga_wrapper;
#endif
#ifdef _CTRL_ARCH_OPENCL_GPU_
typedef struct {
	int		origAcumCard[4];
	int 	card[3];
	int		offset;
} KHitTile_opencl_wrapper;
#endif


#ifndef CTRL_FPGA_KERNEL_FILE
/**
 * Generate particular polymorphic types of KHitTile as well as appropiate wrappers for OpenCL GPU and FPGA.
 * @hideinitializer
 * 
 * @param type Name of a valid native or derived C type.
 * 
 * @see Ctrl_NewType, KHitTile
 */
#ifdef _CTRL_ARCH_PYNQ_
#define hit_ktileNewType( type ) \
    typedef HitTile_##type to_HitTile_KHitTile_##type; \
	typedef struct { \
		volatile type	*data; \
		int		origAcumCard[4]; \
		int 	card[3]; \
		int		offset; \
        PYNQ_SHARED_MEMORY pynq_shared_memory; \
	} KHitTile_##type;\
	typedef struct { \
		int origAcumCard[4]; \
		int card[3]; \
		int offset; \
	} fpga_wrapper_KHitTile_##type; \
	CTRL_KTILE_VARS(type); 
#else
#define hit_ktileNewType( type ) \
	typedef struct { \
		type	*data; \
		int		origAcumCard[4]; \
		int 	card[3]; \
		int		offset; \
	} KHitTile_##type;\
	typedef struct { \
		int origAcumCard[4]; \
		int card[3]; \
		int offset; \
	} fpga_wrapper_KHitTile_##type; \
	CTRL_KTILE_VARS(type);
#endif // _CTRL_ARCH_PYNQ_

#if !defined(_CTRL_KERNELS_H_) && !defined(_CTRL_ARCH_PYNQ_)
#define CTRL_KTILE_VARS(type) \
	const char *raw_ktile_KHitTile_##type = CTRL_KERNEL_STRINGIFY( type ); \
	const char *raw_def_ktile_KHitTile_##type = CTRL_KERNEL_STRINGIFY( \
		typedef struct { \
			__global type	*data; \
			int		origAcumCard[4]; \
			int 	card[3]; \
		} KHitTile_##type##_write; \
		typedef struct { \
			__global const type	*data; \
			int		origAcumCard[4]; \
			int 	card[3]; \
		} KHitTile_##type##_read; \
		typedef struct { \
			int		origAcumCard[4]; \
			int 	card[3]; \
			int		offset; \
		} KHitTile_##type##_wrapper; \
	); \
	bool raw_added_ktile_KHitTile_##type = false;
#else // _CTRL_KERNELS_H_
#define CTRL_KTILE_VARS(type)
#endif // _CTRL_KERNELS_H_

#else // CTRL_FPGA_KERNEL_FILE

#if !defined( CTRL_HOST_COMPILE ) && !defined( CTRL_FPGA_BIN_NAME )
	#define hit_ktileNewType( type ) \
		typedef struct __attribute__((packed)) __attribute__((aligned(CTRL_FPGA_STRUCT_ALIGNMENT))) { \
			__global type	*data; \
			int		origAcumCard[4]; \
			int 	card[3]; \
			int		offset; \
		} KHitTile_##type; \
		\
		typedef struct { \
			int 	origAcumCard[4]; \
			int		card[3]; \
			int		offset; \
		} fpga_wrapper_KHitTile_##type; \
		\
		typedef __global void *restrict data_KHitTile_##type;
#endif // !CTRL_HOST_COMPILE && !CTRL_FPGA_BIN_NAME

#endif // CTRL_FPGA_KERNEL_FILE
/* #else
	#define hit_ktileNewType( type ) \
		typedef struct __attribute__((packed)) __attribute__((aligned(CTRL_FPGA_STRUCT_ALIGNMENT))) { \
			type	* data; \
			int	origAcumCard[4]; \
			int 	card[3]; \
			KHitShape shape; \
		} KHitTile_##type; */
//#endif

///@endcond
#endif // _CTRL_CORE_KHITTILE_H_
