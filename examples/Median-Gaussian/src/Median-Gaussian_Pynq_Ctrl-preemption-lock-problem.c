/*
 * <license>
 * 
 * Controller v2.1
 * 
 * This software is provided to enhance knowledge and encourage progress in the scientific
 * community. It should be used only for research and educational purposes. Any reproduction
 * or use for commercial purpose, public redistribution, in source or binary forms, with or 
 * without modifications, is NOT ALLOWED without the previous authorization of the copyright 
 * holder. The origin of this software must not be misrepresented; you must not claim that you
 * wrote the original software. If you use this software for any purpose (e.g. publication),
 * a reference to the software package and the authors must be included.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDER AND CONTRIBUTORS "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 * THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * Copyright (c) 2007-2020, Trasgo Group, Universidad de Valladolid.
 * All rights reserved.
 * 
 * More information on http://trasgo.infor.uva.es/
 * 
 * </license>
*/
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <omp.h>
#include <assert.h>
#include "Ctrl.h"

#define SEED 6834723
#define EPSILON 0.0001

#define HCOL (1024+2)

double main_clock;
double exec_clock;

#include "median-blur.h"
#include "gaussian-blur.h"

#include <sys/time.h>
#include <signal.h>

#include <time.h>

int H, W;
int H_NROW, H_NCOL;
char * image_path;


CTRL_KERNEL_CHAR(MedianBlur, MANUAL, LOCAL_SIZE_0, LOCAL_SIZE_0);
CTRL_KERNEL_CHAR(GaussianBlur, MANUAL, 8, 8);

CTRL_HOST_TASK( Init_Tiles, HitTile_int in_matrix, HitTile_int out_matrix ) {
    FILE * image;
    image = fopen("/home/xilinx/controller-DPR/controllers/examples/Median-Gaussian/src/Saltpepper.pgm", "r");

    size_t len = 100;
    char line[len];

    fgets(line, len, image);
    printf("%s\n", line);

    
    int file_H, file_W;
    fscanf(image, "%d", &file_H);
    fscanf(image, "%d", &file_W);

    fgets(line, len, image);

    int * data = malloc(H_NROW*H_NCOL * sizeof(int));

    for(int i = 0; i < H_NROW; i++) {
        for(int j = 0; j < H_NCOL; j++) {
            hit(in_matrix, i, j) = 0;
            hit(out_matrix, i, j) = 0;
        }
    }

    for(int i = 1; i < H_NROW-1; i++) {
        for(int j = 1; j < H_NCOL-1; j++) {
            fscanf(image, "%d", &data[i*file_W+j]);
        }
    }   

    for(int i = 1; i < H_NROW-1; i++) {
        for(int j = 1; j < H_NCOL-1; j++) {
            hit(in_matrix, i, j) = data[i*file_W+j]; 
            hit(out_matrix, i, j) = 0;
        }
    }   
    printf("Finishing Init_Tiles\n");
}

CTRL_HOST_TASK( Write_Image, HitTile_int image_matrix, int H, int W, int number ) {
    FILE * output;

    char filename[100];
    sprintf(filename, "output_image_%d.ppm", number);
    //output = fopen("output_image.ppm", "w");
    output = fopen(filename, "w");

    int i, j;
    
    int H_NROW = H+2;
    int H_NCOL = W+2;

    fwrite("P2\n", sizeof(char), 3, output);
    fprintf(output, "%d ", H);
    fprintf(output, "%d\n", W);
    fprintf(output, "%d\n", 255);

    for(i = 1; i < H_NROW-1; i++) {
        for(j = 1; j < H_NCOL-1; j++) {
            fprintf(output, "%d ", hit(image_matrix, i, j));
        }
    }

    fclose(output);
}

CTRL_HOST_TASK(Init_Images, HitTile_int input_image, HitTile_int output_image, int H, int W) {
//void initialise_image(HitTile_int image, int H, int W, int init_zeros) {
    for(int i = 0; i < H; i++) {
        for(int j = 0; j < W; j++) {
                hit(input_image, i, j) = rand();
                hit(output_image, i, j) = 0;
        }
    }
}


CTRL_HOST_TASK_PROTO( Init_Tiles, 2,
	OUT, HitTile_int, in_matrix,
	OUT, HitTile_int, out_matrix
);

CTRL_HOST_TASK_PROTO( Init_Images, 4,
        OUT, HitTile_int, input_image,
        OUT, HitTile_int, output_image,
        INVAL, int, H,
        INVAL, int, W
);

CTRL_HOST_TASK_PROTO( Write_Image, 4,
        IN, HitTile_int, image_matrix,
        INVAL, int, H,
        INVAL, int, W,
        INVAL, int, number
);

CTRL_KERNEL_PROTO( GaussianBlur,
        1, PYNQ, DEFAULT, 4,
        IN, HitTile_int, in_matrix,
        OUT, HitTile_int, out_matrix,
        INVAL, int, H,
        INVAL, int, W);

CTRL_KERNEL_PROTO( MedianBlur,
        1, PYNQ, DEFAULT, 5,
        IN, HitTile_int, in_matrix,
        OUT, HitTile_int, out_matrix,
        INVAL, int, H,
        INVAL, int, W,
        INVAL, int, iters);


#define N_PRIORITIES 5
#define MAX_N_TASK 50 

#define n_priorities(enabled_priorities) (enabled_priorities ? N_PRIORITIES : 1)

// Scheduler task types
typedef enum schedulerTaskType {
   SCHEDULER_MEDIAN_1,
   SCHEDULER_MEDIAN_2,
   SCHEDULER_MEDIAN_3,
   SCHEDULER_GAUSSIAN,
   number_of_sch_task_types
} schedulerTaskType;

typedef struct kernel_data {
    int iters;
    schedulerTaskType task_type;
    double arrival_time;
} kernel_data;

typedef struct schedulerTask {
    int id;
    kernel_data k_data;
    struct context context;
    HitTile_int * input_image;
    HitTile_int * output_image;
    int rr;
    int priority;
    int enqueued;
    int preempted;
} schedulerTask;

schedulerTask all_tasks[N_PRIORITIES * MAX_N_TASK];
schedulerTask * task_queue[N_PRIORITIES][MAX_N_TASK];
schedulerTask * running_tasks[N_RR+1];

// points to current task
int task_queue_head[N_PRIORITIES];

// points to insertion point (for next task)
int task_queue_tail[N_PRIORITIES];

// Indicates how many tasks are pending in the priority queues plus the tasks that are pending but were
// dispateched without being enqueued first

double * arrival_times;


HitTile_int * input_images;
HitTile_int * output_images;
kernel_data * k_data;

void task_generator(PCtrl ctrl, int n_tasks, double total_time, int enabled_priorities, int H, int W) {
    H_NROW = H+2;
    H_NCOL = W+2;

    //struct context contexts[n_tasks];
    input_images = (HitTile_int*)malloc(n_tasks * sizeof(HitTile_int));
    output_images = (HitTile_int*)malloc(n_tasks * sizeof(HitTile_int));
    k_data = (kernel_data*)malloc(n_tasks * sizeof(kernel_data));

    int priority;

    arrival_times = (double *)malloc(n_tasks * sizeof(double));

    for(int task = 0; task < n_tasks; task++) {
        k_data[task].task_type = rand() % 4; 
        k_data[task].arrival_time = (double)rand() / (double)RAND_MAX * (double)total_time * 60.0;

        if(k_data[task].task_type <= 2) // MEDIAN
            k_data[task].iters = k_data[task].task_type+1;

        input_images[task] = Ctrl_DomainAlloc(ctrl, int, hitShapeSize(H_NROW, H_NCOL));
        output_images[task] = Ctrl_DomainAlloc(ctrl, int, hitShapeSize(H_NROW, H_NCOL));

        //initialise_image(input_images[task], H_NROW, H_NCOL, 0);
        //initialise_image(output_images[task], H_NROW, H_NCOL, 0);

        if(enabled_priorities)
            priority = rand() % 5; 
        else
            priority = 0;
        
        all_tasks[task].id = task;
        all_tasks[task].k_data = k_data[task];
        all_tasks[task].input_image = &input_images[task];
        all_tasks[task].output_image = &output_images[task];
        all_tasks[task].priority = priority;
        all_tasks[task].enqueued = 0;
        all_tasks[task].preempted = 0;
    }    

    for(int task = 0; task < n_tasks; task++) {
        Ctrl_HostTask(ctrl, Init_Images, input_images[task], output_images[task], H_NROW, H_NCOL);
        printf("Generated image %d\n", task);
        //Ctrl_HostTask(ctrl, Init_Tiles, input_images[task], output_images[task]);
    }
    Ctrl_GlobalSync(ctrl);
   
    int j_min;
    // Sort tasks by arrival time
    for(int i = 0; i < n_tasks; i++) {
        j_min = i; 
        for(int j = i+1; j < n_tasks; j++) {
            if(all_tasks[j].k_data.arrival_time < all_tasks[j_min].k_data.arrival_time)
                j_min = j; 
        }

        HitTile_int * aux_input_image = all_tasks[i].input_image;
        HitTile_int * aux_output_image = all_tasks[i].output_image;
        kernel_data aux_k_data = all_tasks[i].k_data;

        all_tasks[i].input_image = all_tasks[j_min].input_image;
        all_tasks[i].output_image = all_tasks[j_min].output_image;
        all_tasks[i].k_data = all_tasks[j_min].k_data;

        all_tasks[j_min].input_image = aux_input_image;
        all_tasks[j_min].output_image = aux_output_image;
        all_tasks[j_min].k_data = aux_k_data;
    }

    double previous_time = 0.0;
    for(int task = 0; task < n_tasks; task++) {
        double absolute_time = all_tasks[task].k_data.arrival_time;
        all_tasks[task].k_data.arrival_time -= previous_time;
        previous_time = absolute_time;
    }

    for(int task = 0; task < n_tasks; task++)
        arrival_times[task] = all_tasks[task].k_data.arrival_time;

    for(int priority = 0; priority < N_PRIORITIES; priority++) {
        task_queue_head[priority] = 0;
        task_queue_tail[priority] = 0;
    }


    // Test sorted
    printf("TASKS TIMES\n");
    for(int task = 0; task < n_tasks; task++) {
        printf("%lf\n", all_tasks[task].k_data.arrival_time);
    }

    //all_tasks[0].k_data.arrival_time = 1.5;
    //arrival_times[0] = 1.5;


}

typedef enum kernels {
    MEDIAN,
    GAUSSIAN,
    number_of_kernel_types
} kernels;

typedef enum kernel_state {
    RUNNING,
    FINISHED,
    UNAVAILABLE
} kernel_state;



char pblock_names[N_RR][50];


char kernel_name[number_of_kernel_types][50];


#define MAX(a, b) (a > b) ? a : b

int ready_for_new_task = 1;
int n_arriving_task = 0;

int process_finished(int use_CPU, kernel_state * loaded_kernel_state, int n_rr, int * available_tasks, int n_tasks, int * n_total_finished) {
    int n_finished = 0;

    for(int finished_rr = 0; finished_rr < ((use_CPU) ? N_RR+1 : n_rr); finished_rr++) {
        if(generated_interrupts[finished_rr]) {
            #ifdef _DEBUG_OUTPUTS
            printf("---> FINISHED %d\n", finished_rr);
            #endif // _DEBUG_OUTPUTS
            loaded_kernel_state[finished_rr] = FINISHED;
            generated_interrupts[finished_rr] = 0;

            // The lock does not apply to CPU
            if(finished_rr < N_RR)
                omp_unset_lock(&lock_RR[finished_rr]);
            
            (*n_total_finished) ++;
        }
        if(loaded_kernel_state[finished_rr] == FINISHED)
            n_finished++;
    }


    if(*n_total_finished == n_tasks)
        *available_tasks = 0;

    return n_finished;
}

void swapping(PCtrl ctrl, int rr, int n_rr, schedulerTask * p_task, kernels * loaded_kernels) {
    if(rr < n_rr) {
        int swap = 0;

        // rr (as every other argument) has to be available to deeper functions. Therefore, cannot be in the stack
        p_task->rr = rr;

        if(p_task->k_data.task_type <= 2 && loaded_kernels[rr] == GAUSSIAN) {
            loaded_kernels[rr] = MEDIAN;
            swap = 1;
        }
        else if (p_task->k_data.task_type == 3 && loaded_kernels[rr] == MEDIAN) {
            loaded_kernels[rr] = GAUSSIAN;
            swap = 1;
        }

        printf("loaded_kernels[0] = %d, loaded_kernels[1] = %d\n", loaded_kernels[0], loaded_kernels[1]);
        if(swap) {
            char bitname[512];
            sprintf(bitname, "/home/xilinx/controller-DPR/controllers/Bitstreams/%s-%s_pblock_%s_partial.bit", kernel_name[loaded_kernels[0]], kernel_name[loaded_kernels[1]], pblock_names[rr]);
            printf("(rr: %d) %s\n", p_task->rr, bitname);
            Ctrl_Task ctrl_task = Ctrl_KernelTaskCreate_Swap(p_task->rr, bitname);
            Ctrl_LaunchKernel(ctrl, ctrl_task);
        }
    }
}

// Returns the arriving task or the next one in the queue based on priority
schedulerTask * get_task(int * n_pending_tasks, int use_CPU, int enabled_priorities, int enabled_preemption, int * n_arriving_task, int n_tasks, int arrived_task, int * rr, int n_rr, kernel_state * loaded_kernel_state) {
    schedulerTask * arriving_task;
    int get_from_queue = 0;


    //if(n_arriving_task < n_tasks) {
    if(arrived_task) {
        arriving_task = &all_tasks[*n_arriving_task];

        int preempt = 0;
        if(enabled_preemption) {
           for(*rr = 0; *rr < ((use_CPU) ? N_RR+1 : n_rr); (*rr)++) {
               if(loaded_kernel_state[*rr] == RUNNING && running_tasks[*rr]->priority < arriving_task->priority) {
                   preempt = 1;
                   break;
               }
           }
        }

        if(!preempt) {
            printf("loaded_kernel_state: ");
            for(*rr = 0; loaded_kernel_state[*rr] != FINISHED; (*rr)++) { printf("%d ", loaded_kernel_state[*rr]); }
            printf("\n");
        

            for(int priority = 0; priority < arriving_task->priority; priority++)
                if(n_pending_tasks[priority] > 0)
                    get_from_queue = 1;

            // Enqueue arriving task if another task from a queue has a higher priority
            if(get_from_queue)
                task_queue[arriving_task->priority][task_queue_tail[arriving_task->priority]++] = arriving_task;
        }
        else {
            reset(*rr);
            running_tasks[*rr]->context = RR[*rr].context;
            running_tasks[*rr]->preempted = 1;
            task_queue[running_tasks[*rr]->priority][task_queue_tail[running_tasks[*rr]->priority]++] = running_tasks[*rr];
            n_pending_tasks[running_tasks[*rr]->priority]++;
            context_retrieval[*rr].context = arriving_task->context;
            context_retrieval[*rr].retrieve = 1;
        }

        (*n_arriving_task)++;
    }
    else {
        printf("loaded_kernel_state: ");
        for(*rr = 0; loaded_kernel_state[*rr] != FINISHED; (*rr)++) { printf("%d ", loaded_kernel_state[*rr]); }
        printf("\n");
        get_from_queue = 1;
    }

    int priority;
    int task_in_queue = 0;

    schedulerTask * delivered_task;

    if(get_from_queue) {
        // Check if there are tasks in any queue
        if(enabled_priorities) {
            for(priority = 0; priority < 5; priority++) {
                if(task_queue_head[priority] < task_queue_tail[priority]) {
                    task_in_queue = 1;
                    break;
                }
            }
        }
        else {
            priority = 0;
            if(task_queue_head[0] < task_queue_tail[0]) {
                task_in_queue = 1;
            }
        }

        if(task_in_queue) {
            delivered_task = task_queue[priority][task_queue_head[priority]];
            printf("$$$$ Delivered from queue %d\n", priority);
            task_queue_head[priority]++;
            n_pending_tasks[priority]--;
        }
        else
            delivered_task = NULL;
    }
    else {
        n_pending_tasks[N_PRIORITIES]--;
        delivered_task = arriving_task;
    }

    return delivered_task;
}

void schedule_launch(PCtrl ctrl, Ctrl_Thread threads, Ctrl_Thread group, int rr, int n_rr, int * reset_host, schedulerTask * p_task, kernel_state * loaded_kernel_state) {
    if(p_task->k_data.task_type <= 2) {
        if(rr < n_rr) {
            printf("~~~~ Kernel type %d to RR %d\n", p_task->k_data.task_type, rr);
            Ctrl_Launch(ctrl, MedianBlur, threads, group, p_task->rr, *p_task->input_image, *p_task->output_image, H, W, p_task->k_data.iters);
        }
        else {
            printf("Kernel type %d to CPU\n", p_task->k_data.task_type);
            Ctrl_HostTask(ctrl, MedianBlur, p_task->rr, *reset_host, *p_task->input_image, *p_task->output_image, H, W, p_task->k_data.iters);
        }
    }
    else if(p_task->k_data.task_type == 3) {
        if(rr < n_rr) {
            printf("~~~~ Kernel type %d to RR %d\n", p_task->k_data.task_type, rr);
            Ctrl_Launch(ctrl, GaussianBlur, threads, group, p_task->rr, *p_task->input_image, *p_task->output_image, H, W);
        }
        else {
            printf("~~~~ Kernel type %d to CPU\n", p_task->k_data.task_type);
            Ctrl_HostTask(ctrl, GaussianBlur, p_task->rr, *reset_host, *p_task->input_image, *p_task->output_image, H, W);
        }
    }
    loaded_kernel_state[rr] = RUNNING;
    running_tasks[rr] = p_task;
}

void launch_task(PCtrl ctrl, Ctrl_Thread threads, Ctrl_Thread group, int n_rr, int rr, int * reset_host, schedulerTask * current_task, kernel_state * loaded_kernel_state, kernels * loaded_kernels) {
    //int rr;
    //printf("loaded_kernel_state: ");
    //for(rr = 0; loaded_kernel_state[rr] != FINISHED; rr++) { printf("%d ", loaded_kernel_state[rr]); }
    //printf("\n");

    swapping(ctrl, rr, n_rr, current_task, loaded_kernels);

    schedule_launch(ctrl, threads, group, rr, n_rr, reset_host, current_task, loaded_kernel_state);
}

void enqueue_task(int * n_arriving_task, int enabled_priorities, int * n_pending_tasks) {
    printf("#### Enqueing task %d\n", *n_arriving_task);
    schedulerTask * arriving_task = &all_tasks[*n_arriving_task];

    int priority = arriving_task->priority;
    task_queue[priority][task_queue_tail[priority]++] = arriving_task;

    n_pending_tasks[N_PRIORITIES]--;
    if(enabled_priorities)
        n_pending_tasks[priority]++;
    else
        n_pending_tasks[0]++;

    (*n_arriving_task)++;
}

int update_timeout(struct timeval * timeout, int n_current_task, int n_tasks) {
    if(n_current_task < n_tasks) {
        int sec = floor(arrival_times[n_current_task]);
        int usec = (arrival_times[n_current_task] - floor(arrival_times[n_current_task])) * 100000;

        timeout->tv_sec = sec;
        timeout->tv_usec = usec;

        return 1;
    }
    return 0;
}

void get_task_and_launch(PCtrl ctrl, Ctrl_Thread threads, Ctrl_Thread group, int * n_current_task, int n_tasks, int n_rr, int * reset_host, int enabled_priorities, int enabled_preemption, int arrived_task, int * n_pending_tasks, kernel_state * loaded_kernel_state, kernels * loaded_kernels, int use_CPU) {
        int rr;
        schedulerTask * current_task = get_task(n_pending_tasks, use_CPU, enabled_priorities, enabled_preemption, n_current_task, n_tasks, arrived_task, &rr, n_rr, loaded_kernel_state);
        if(current_task != NULL)
            launch_task(ctrl, threads, group, n_rr, rr, reset_host, current_task, loaded_kernel_state, loaded_kernels);
        else
            printf("NULL TASK\n");
}


void scheduler(PCtrl ctrl, Ctrl_Thread threads, Ctrl_Thread group, int n_tasks, int H, int W, int use_CPU, int n_rr, int enabled_priorities, int enabled_preemption) {
    reset_interrupt_controller();

    struct timeval timeout;
    int n_current_task = 0;
    int n_finished = 0; // number of tasks that finished since last time we waited
    int n_pending_tasks[N_PRIORITIES+1]; // pending tasks in queues and not arrived (last element)
    int available_tasks = 1;
    int reset_host = 0;
    kernels loaded_kernels[N_RR + 1];
    kernel_state loaded_kernel_state[N_RR + 1];

    sprintf(pblock_names[0], "medianblur_0");
    sprintf(pblock_names[1], "gaussianblur_0");

    sprintf(kernel_name[0], "MedianBlur");
    sprintf(kernel_name[1], "GaussianBlur");

    loaded_kernels[0] = GAUSSIAN;
    loaded_kernels[1] = GAUSSIAN;

    loaded_kernel_state[0] = FINISHED;
    loaded_kernel_state[1] = FINISHED;

    // Render unavailable regions that will not be used during scheduling
    if(use_CPU) {
        loaded_kernel_state[N_RR] = FINISHED;
    }
    else {
        loaded_kernel_state[N_RR] = UNAVAILABLE;
    }

    for(int i = ((use_CPU) ? N_RR+1 : n_rr); i < N_RR+1; i++) loaded_kernel_state[i] = UNAVAILABLE;


    for(int i = 0; i < N_PRIORITIES; i++) n_pending_tasks[i] = 0;
    n_pending_tasks[N_PRIORITIES] = n_tasks;

    update_timeout(&timeout, n_current_task, n_tasks);

    // Fill all the used regions with tasks before starting scheduler
    for(int rr = 0; rr < ((use_CPU) ? N_RR+1 : n_rr); rr++) {
        get_task_and_launch(ctrl, threads, group, &n_current_task, n_tasks, n_rr, &reset_host, enabled_priorities, enabled_preemption, 1, n_pending_tasks, loaded_kernel_state, loaded_kernels, use_CPU);
    }

    int remaining_tasks_to_arrive = 1;
    int finished_tasks = 0;

    printf("n_finished: %d\n", n_finished);
    for(int i = 0; i < N_PRIORITIES+1; i++) printf("n_pending_tasks[%d] = %d\n", i, n_pending_tasks[i]);

    // The source of tasks can be both arriving tasks and the queue at this stage
    while(remaining_tasks_to_arrive) {
        wait_finish(&timeout);

            printf("generated_interrupts: ");
            for(int i = 0; i < N_RR+1; i++)
                printf("%d ", generated_interrupts[i]);
            printf("\n");
        n_finished = process_finished(use_CPU, loaded_kernel_state, n_rr, &available_tasks, n_tasks, &finished_tasks);
        //finished_tasks += n_finished;
        printf("n_finished: %d, finished_tasks: %d\n", n_finished, finished_tasks);
        

        if(timeout.tv_sec == 0 && timeout.tv_usec == 0) {
            printf("..... TIMEOUT!\n");
            // new task just arrived. However, there will not be an available region unless
            // by the end of the scheduling.
            if(n_finished > 0) {
                for(int task = 0; task < n_finished; task++) {
                    get_task_and_launch(ctrl, threads, group, &n_current_task, n_tasks, n_rr, &reset_host, enabled_priorities, enabled_preemption, 1, n_pending_tasks, loaded_kernel_state, loaded_kernels, use_CPU);
                }
            }
            else {
                enqueue_task(&n_current_task, enabled_priorities, n_pending_tasks);
            }

            // set time of next task
            remaining_tasks_to_arrive = update_timeout(&timeout, n_current_task, n_tasks);
        }
        else { // launch a pending task, if there is any
            for(int task = 0; task < n_finished; task++) {
                get_task_and_launch(ctrl, threads, group, &n_current_task, n_tasks, n_rr, &reset_host, enabled_priorities, enabled_preemption, 0, n_pending_tasks, loaded_kernel_state, loaded_kernels, use_CPU);
            }
        }
        printf("n_finished: %d, finished_tasks: %d\n", n_finished, finished_tasks);
        for(int i = 0; i < N_PRIORITIES+1; i++) printf("n_pending_tasks[%d] = %d\n", i, n_pending_tasks[i]);
    }

    
    printf("\n\n///// SECOND PHASE\n");
    n_finished = 0;
    for(int rr = 0; rr < ((use_CPU) ? N_RR+1 : n_rr); rr++)
        if(loaded_kernel_state[rr] == FINISHED)
            n_finished++;

    // Now the only source of tasks is the queue
    while(available_tasks) {
        for(int task = 0; task < n_finished; task++) {
            printf("Before get task and launch\n");
            get_task_and_launch(ctrl, threads, group, &n_current_task, n_tasks, n_rr, &reset_host, enabled_priorities, enabled_preemption, 0, n_pending_tasks, loaded_kernel_state, loaded_kernels, use_CPU);
        }
        wait_finish(&timeout);
            printf("generated_interrupts: ");
            for(int i = 0; i < N_RR+1; i++)
                printf("%d ", generated_interrupts[i]);
            printf("\n");
        n_finished = process_finished(use_CPU, loaded_kernel_state, n_rr, &available_tasks, n_tasks, &finished_tasks);
        //finished_tasks += n_finished;

        printf("n_finished: %d, finished_tasks: %d\n", n_finished, finished_tasks);
        for(int i = 0; i < N_PRIORITIES+1; i++) printf("n_pending_tasks[%d] = %d\n", i, n_pending_tasks[i]);
    }

    printf("Finished tasks: %d\n", finished_tasks);
}


int main(int argc, char *argv[]) {
	main_clock = omp_get_wtime();

    int SIZE = 1024;

    if(argc < 10) {
        printf("Median-Gaussian_Pynq_Ctrl <H> <W> <image_path> <use_CPU> <n_rr> <enabled_priorities> <enabled_preemption> <n_tasks> <minutes>\n");
        return -1;
    }

    H = atoi(argv[1]);
    W = atoi(argv[2]);
    H_NROW = H + 2;
    H_NCOL = W + 2;

    image_path = argv[3];

    int use_CPU = atoi(argv[4]);
    int n_rr = atoi(argv[5]);
    int enabled_priorities = atoi(argv[6]);
    int enabled_preemption = atoi(argv[7]);
    int n_tasks = atoi(argv[8]);
    double minutes = atof(argv[9]);


	Ctrl_Thread threads;
	Ctrl_ThreadInit(threads, SIZE, SIZE);

	Ctrl_Thread group;
	Ctrl_ThreadInit(group, LOCAL_SIZE_0, LOCAL_SIZE_1);


	__ctrl_block__(1,1)
	{
        Ctrl_Policy policy = 1;
        int device = 0;
		PCtrl ctrl = Ctrl_Create(CTRL_TYPE_PYNQ, policy, device);

        Ctrl_GlobalSync(ctrl);
        exec_clock = omp_get_wtime();


        srand(0);

        task_generator(ctrl, n_tasks, minutes, enabled_priorities, H, W);
        scheduler(ctrl, threads, group, n_tasks, H, W, use_CPU, n_rr, enabled_priorities, enabled_preemption);
        //Ctrl_HostTask(ctrl, Init_Tiles, input_images[0], output_images[0]);

        //Ctrl_GlobalSync(ctrl);
       
        // 
        //int rr = 1;
        //Ctrl_Launch(ctrl, GaussianBlur, threads, group, rr, input_images[0], output_images[0], H, W);

        //struct timespec sleep_time = {
        //    .tv_sec = 0,
        //    .tv_nsec = 100000000
        //};

        ////nanosleep(&sleep_time, NULL);
        ////reset(1);

        ////_print_output(1);

        //////all_tasks[0].context = RR[rr].context;
        //////
        ////Ctrl_Launch(ctrl, GaussianBlur, threads, group, rr, input_images[0], output_images[0], H, W);

        //struct timeval timeout = {
        //    .tv_sec = 0,
        //    .tv_usec = 0
        //};
        //
        //wait_finish(&timeout);
        //
        ////for(int task = 0; task < 1; task++) 
        ////    Ctrl_HostTask(ctrl, Write_Image, input_images[task], H, W, task);

        //    //printf("generated_interrupts: ");
        //    //for(int i = 0; i < N_RR+1; i++)
        //    //    printf("%d ", generated_interrupts[i]);
        //    //printf("\n");
        //#ifdef _DEBUG_OUTPUTS
        //for(int task = 0; task < n_tasks; task++) {
        //    Ctrl_HostTask(ctrl, Write_Image, output_images[task], H, W, task);
        //}
        //#endif // _DEBUG_OUTPUTS

        /*
        int rr = 0;
        int iters = 1;
        int reset_host = 0;

        for(int i = 0; i < 5; i++) {
            Ctrl_HostTask(ctrl, MedianBlur, rr, reset_host, input_images[i], output_images[i], H, W, iters);
            wait_finish(&timeout);
        }
        */

		Ctrl_GlobalSync(ctrl);
        exec_clock = omp_get_wtime() - exec_clock;


        for(int task = 0; task < n_tasks; task++) {
            Ctrl_Free(ctrl, input_images[task], output_images[task]);
        }

        Ctrl_Destroy(ctrl);
    }

	main_clock = omp_get_wtime() - main_clock;

	#ifdef _CTRL_EXAMPLES_EXP_MODE_
		printf("%lf, %lf\n", main_clock, exec_clock);
		fflush(stdout);
	#else
		printf("\n ----------------------- TIME ----------------------- \n\n");
		printf(" Clock main: %lf\n", main_clock);
		printf(" Clock exec: %lf\n", exec_clock);
		printf("\n ---------------------------------------------------- \n");
	#endif

	return 0;
}
