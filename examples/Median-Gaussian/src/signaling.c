#include <signal.h>
#include <stdio.h>
#include <sys/time.h>
#include <unistd.h>

int interrupted = 0;
int my_signal;

void handler(int signal) {
    printf("Hello from handler\n");
   interrupted = 1;
   my_signal = signal; 
}

void main() {
    struct timeval it_interval = {
        .tv_sec = 0,
        .tv_usec = 0
    };
    struct timeval it_value = {
        .tv_sec = 1,
        .tv_usec = 0
    };

    struct itimerval itimerinterval = {
        .it_interval = it_interval,
        .it_value = it_value
    };

    setitimer(ITIMER_REAL, &itimerinterval, NULL);

    struct sigaction s_sigaction = {
        .sa_handler = handler,
        //.sa_flags = 0,
        .sa_flags = SA_RESTART
    };

    sigaction(SIGALRM, &s_sigaction, NULL);

    while(!interrupted) {
        printf("Before sleep\n");
        sleep(20);
        printf("After sleep\n");
    }
        
    printf("signal: %d\n", my_signal);
}
