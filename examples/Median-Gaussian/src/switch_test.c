#include <stdio.h>
#include <stdlib.h>

#include "pynq_api.h"
#include <unistd.h>

#define N_SAVED_VARS 10

int H, W;
int H_NROW, H_NCOL;

int * data;
int * d1;
int * d2;

PYNQ_AXI_INTERRUPT_CONTROLLER interrupt_controller; 
PYNQ_UIO uio_state;
PYNQ_HLS state;

PYNQ_GPIO decouple;
PYNQ_GPIO reset_gpio;

int enable = 1;
int disable = 0;

struct context {
    //Pointers to the loop index variables and the integer variables. Used for the user to record
    //for which variables the context should be saved when they call the macro save_context().
    int var[N_SAVED_VARS];
    int init_var[N_SAVED_VARS]; // initial value of variable
    int incr_var[N_SAVED_VARS]; // increment of the loop variable
    int saved[N_SAVED_VARS];
    int valid; // set to 0 if the kernel was interrupted in the middle of a saving context operation. 1 otherwise
};

void read_file(char * image_file) {
    FILE * image;
    //image = fopen("/home/xilinx/controller-DPR/controllers/examples/Median-Gaussian/src/Saltpepper.pgm", "r");
    image = fopen(image_file, "r");
    size_t len = 100;
    char line[len];

    fgets(line, len, image);
    printf("%s\n", line);

    
    int file_H, file_W;
    fscanf(image, "%d", &H);
    fscanf(image, "%d", &W);

    // colour
    fgets(line, len, image);

    data = malloc(H*W * sizeof(int));

    for(int i = 1; i < H_NROW-1; i++) {
        for(int j = 1; j < H_NCOL-1; j++) {
            fscanf(image, "%d", &d1[i * W + j]);
        }
    }   
}

void write_file(char * image_file) {
    FILE * image;
    //image = fopen("/home/xilinx/controller-DPR/controllers/examples/Median-Gaussian/src/Saltpepper.pgm", "r");
    image = fopen(image_file, "w");
    size_t len = 100;
    char line[len];

    fprintf(image, "P2\n");
    fprintf(image, "%d %d\n", H, W);

    fprintf(image, "255\n");

    for(int i = 1; i < H_NROW-1; i++) {
        for(int j = 1; j < H_NCOL-1; j++) {
            fprintf(image, "%d ", d2[i * W + j]);
        }
    }
}

void wait_finish() {                                                                                                  
    int n_interrupts;                                                                                                 
    printf("Before waitForUIO\n");                                                                                  
    PYNQ_waitForUIO(&uio_state, &n_interrupts);                                                      
    printf("After waitForUIO\n");                                                                                   
                                                                                                                      
                                                                                                                      
    //printf("Before getting interrupt_controller\n");                                                                
    //PYNQ_AXI_INTERRUPT_CONTROLLER * interrupt_controller = &interrupt_controller;                    
                                                                                                                      
    unsigned int raised_irqs, work, current_irq=0;                                                                    
    int IER_val;                                                                                                      
                                                                                                                      
    //printf("Before reading interrupts\n");                                                                          
    // Read the interrupts                                                                                            
    PYNQ_readMMIO(&(interrupt_controller.mmio_window), &raised_irqs, 0x04, sizeof(unsigned int));                    
                                                                                                                      
    ////printf("raised interrupts: 0x%x\n", raised_irqs);                                                             
                                                                                                                      
    int enable = 1;                                                                                                   
    int disable = 0;                                                                                                  
                                                                                                                      
                                                                                                                      
    //printf("Before acknowledging interrupts\n");                                                                    
    // ACKNOWLEDGE INTERRUPTS                                                                                         
    // Acknowledge in HLS block                                                                                       
    int ISR_HLS = 1;                                                                                                  
                                                                                                                      
    // Which RR triggered the interrupt?                                                                              
    work = raised_irqs;                                                                                               
    for(int _rr = 0; _rr < 32; _rr++) {                                                                               
        //printf("work: 0x%x\n", work);                                                                               
        if(work & 0x1) {                                                                                              
            PYNQ_writeToHLS(&state, &ISR_HLS, 0xC, sizeof(int));                                              
        }                                                                                                             
        work = work >> 1;                                                                                             
    }                                                                                                                 
                                                                                                                      
    //printf("After ack on HLS\n");                                                                                   
                                                                                                                      
    // Acknowledge in controller                                                                                      
    PYNQ_writeMMIO(&(interrupt_controller.mmio_window), &raised_irqs, 0x0C, sizeof(unsigned int));                   
    //printf("After ack on controller\n");                                                                            
} 

void reset() {
    PYNQ_writeGPIO(&reset_gpio, &enable);
    PYNQ_writeGPIO(&reset_gpio, &disable);
}


int main(int argc, char * argv[]) {
    if(argc < 4) {
        printf("switch_test <context_switch> <output_file> <kernel>\n");
        printf("<kernel>: 0 for median-blur, 1 for gaussian-blur\n");
        return 1;
    }

    int context_switch = atoi(argv[1]);
    char * output_file = argv[2];
    int kernel = atoi(argv[3]);

    printf("kernel: %d\n", kernel);

    //PYNQ_loadBitstream("/home/xilinx/controller-DPR/controllers/Bitstreams/GaussianBlur-GaussianBlur.bit", 0);
    if(kernel == 0)
        PYNQ_loadBitstream("/home/xilinx/controller-DPR/controllers/Bitstreams/MedianBlur-GaussianBlur.bit", 0);
    else if(kernel == 1)
        PYNQ_loadBitstream("/home/xilinx/controller-DPR/controllers/Bitstreams/GaussianBlur-GaussianBlur.bit", 0);

    PYNQ_MMIO_WINDOW bram;
    PYNQ_createMMIOWindow(&bram, 0x40020000, 8192);

    PYNQ_openGPIO(&decouple, 960, GPIO_OUT);
    PYNQ_openGPIO(&reset_gpio, 962, GPIO_OUT);


    PYNQ_openHLS(&state, 0x40000000, 65536);

    H = 600;
    W = 600;

    H_NROW = H+2;
    H_NCOL = W+2;

	// enable interrupts
	PYNQ_writeToHLS(&state, &enable, 0x4, sizeof(int));
	PYNQ_writeToHLS(&state, &enable, 0x8, sizeof(int));


    PYNQ_openInterruptController(&interrupt_controller, 0x41200000);
    PYNQ_openUIO(&uio_state, 0);
	PYNQ_registerInterrupt(&interrupt_controller, 0, 1);


    PYNQ_SHARED_MEMORY in_matrix;
    PYNQ_SHARED_MEMORY out_matrix;


    PYNQ_allocatedSharedMemory(&in_matrix, H_NROW*H_NCOL * sizeof(int), 1);
    PYNQ_allocatedSharedMemory(&out_matrix, H_NROW*H_NCOL * sizeof(int), 1);

    d1 = (int*)in_matrix.pointer;
    d2 = (int*)out_matrix.pointer;

    read_file("/home/xilinx/controller-DPR/controllers/examples/Median-Gaussian/src/Saltpepper.pgm");

    typedef struct {
        int origAcumCard[4];
        int card[3];
        int offset;
    } fpga_wrapper_KHitTile_int;

    typedef volatile int * data_KHitTile_int;

    fpga_wrapper_KHitTile_int in_matrix_wrapper;
    fpga_wrapper_KHitTile_int out_matrix_wrapper;

    data_KHitTile_int in_matrix_data = (volatile int*)in_matrix.physical_address;
    data_KHitTile_int out_matrix_data = (volatile int*)out_matrix.physical_address;

    for(int i = 0; i < 4; i++) {
        in_matrix_wrapper.origAcumCard[i] = 0;
        out_matrix_wrapper.origAcumCard[i] = 0;
    }

    for(int i = 0; i < 3; i++) {
        in_matrix_wrapper.card[i] = 0;
        out_matrix_wrapper.card[i] = 0;
    }

    in_matrix_wrapper.offset = 0;
    out_matrix_wrapper.offset = 0;

    int iters = 3;

    // Launch first time
    PYNQ_writeToHLS(&state, &in_matrix_wrapper, 0x10, sizeof(fpga_wrapper_KHitTile_int));
    PYNQ_writeToHLS(&state, &in_matrix_data, 0x34, sizeof(data_KHitTile_int));
    PYNQ_writeToHLS(&state, &out_matrix_wrapper, 0x40, sizeof(fpga_wrapper_KHitTile_int));
    PYNQ_writeToHLS(&state, &out_matrix_data, 0x64, sizeof(data_KHitTile_int));
    PYNQ_writeToHLS(&state, &H, 0xA0, sizeof(int));
    PYNQ_writeToHLS(&state, &W, 0xA8, sizeof(int));
    PYNQ_writeToHLS(&state, &iters, 0xB0, sizeof(int));

    PYNQ_writeToHLS(&state, &enable, 0, sizeof(int));

    if(context_switch) {
        usleep(300 * 1000);

        reset();

        // enable interrupts
        PYNQ_writeToHLS(&state, &enable, 0x4, sizeof(int));
        PYNQ_writeToHLS(&state, &enable, 0x8, sizeof(int));

        struct context context;
        PYNQ_readMMIO(&bram, &context, 0, sizeof(struct context));

        printf("BRAM\n");
        for(int i = 0; i < 10; i++)
            printf("%d\n", context.var[i]);

        // Launch second time
        PYNQ_writeToHLS(&state, &in_matrix_wrapper, 0x10, sizeof(fpga_wrapper_KHitTile_int));
        PYNQ_writeToHLS(&state, &in_matrix_data, 0x34, sizeof(data_KHitTile_int));
        PYNQ_writeToHLS(&state, &out_matrix_wrapper, 0x40, sizeof(fpga_wrapper_KHitTile_int));
        PYNQ_writeToHLS(&state, &out_matrix_data, 0x64, sizeof(data_KHitTile_int));
        PYNQ_writeToHLS(&state, &H, 0xA0, sizeof(int));
        PYNQ_writeToHLS(&state, &W, 0xA8, sizeof(int));
        PYNQ_writeToHLS(&state, &iters, 0xB0, sizeof(int));

        PYNQ_writeToHLS(&state, &enable, 0, sizeof(int));
        printf("Before wait\n");
    }

	wait_finish();
	printf("After wait\n");

    write_file(output_file);

    free(data);
}
