#include "scheduler.h"

void scheduler_no_preemption(PCtrl ctrl, Ctrl_Thread threads, Ctrl_Thread group, int n_tasks, int H, int W, int use_CPU, int n_rr, int enabled_priorities) {
    loaded_kernels[0] = GAUSSIAN;
    loaded_kernels[1] = GAUSSIAN;

    loaded_kernel_state[0] = FINISHED;
    loaded_kernel_state[1] = FINISHED;

    // Render unavailable regions that will not be used during scheduling
    if(use_CPU) {
        loaded_kernel_state[N_RR] = FINISHED;
    }
    else {
        loaded_kernel_state[N_RR] = UNAVAILABLE;
    }

    for(int i = n_rr; i < N_RR; i++) loaded_kernel_state[i] = UNAVAILABLE;


    int H_NROW = H+2;
    int H_NCOL = W+2;

    schedulerTask * p_task;
    Ctrl_Task ctrl_task;
    int rr;

    char pblock_names[N_RR][50];
    sprintf(pblock_names[0], "medianblur_0");
    sprintf(pblock_names[1], "gaussianblur_0");

    char bitname[n_tasks][512];

    char kernel_name[number_of_kernel_types][512];
    sprintf(kernel_name[0], "MedianBlur");
    sprintf(kernel_name[1], "GaussianBlur");

    int n_enqueued_kernels = 0;
    int n_interrupts[n_tasks];

    int reset_host = 0;
    int finished_tasks = 0;

    int swap = 0;

    reset_interrupt_controller();

    for(int task = 0; task < n_tasks; task++) {
        printf("loaded_kernel_state: ");
        for(rr = 0; loaded_kernel_state[rr] != FINISHED; rr++) { printf("%d ", loaded_kernel_state[rr]); }
        printf("\n");

        //p_task = &task_queue[0][task];
        p_task = get_next_task(enabled_priorities, task);
        p_task->rr = rr;

        #ifdef _DEBUG_OUTPUTS
        printf("---> Enqueing kernel type %d in RR %d\n", p_task->k_data.task_type, rr);
        #endif // _DEBUG_OUTPUTS 

        
        if(p_task->k_data.task_type <= 2 && loaded_kernels[rr] == GAUSSIAN) {
            loaded_kernels[rr] = MEDIAN;
            swap = 1;
        }
        else if (p_task->k_data.task_type == 3 && loaded_kernels[rr] == MEDIAN) {
            loaded_kernels[rr] = GAUSSIAN;
            swap = 1;
        }

        printf("loaded_kernels[0] = %d, loaded_kernels[1] = %d\n", loaded_kernels[0], loaded_kernels[1]);
        if(swap) {
            if(rr < n_rr) {
                sprintf(bitname[task], "/home/xilinx/controller-DPR/controllers/Bitstreams/%s-%s_pblock_%s_partial.bit", kernel_name[loaded_kernels[0]], kernel_name[loaded_kernels[1]], pblock_names[rr]);
                printf("(task %d, rr: %d) %s\n", task, p_task->rr, bitname[task]);
                ctrl_task = Ctrl_KernelTaskCreate_Swap(p_task->rr, bitname[task]); 
                Ctrl_LaunchKernel(ctrl, ctrl_task);
            }

            swap = 0;
        }
    
        if(p_task->k_data.task_type <= 2) {
            if(rr < n_rr) {
                printf("~~~~ Kernel type %d to RR %d\n", p_task->k_data.task_type, rr);
                Ctrl_Launch(ctrl, MedianBlur, threads, group, p_task->rr, *p_task->input_image, *p_task->output_image, H, W, p_task->k_data.iters);
            }
            else {
                printf("Kernel type %d to CPU\n", p_task->k_data.task_type);
                Ctrl_HostTask(ctrl, MedianBlur, p_task->rr, reset_host, *p_task->input_image, *p_task->output_image, H, W, p_task->k_data.iters);
            }
        }
        else if(p_task->k_data.task_type == 3) {
            if(rr < n_rr) {
                printf("~~~~ Kernel type %d to RR %d\n", p_task->k_data.task_type, rr);
                Ctrl_Launch(ctrl, GaussianBlur, threads, group, p_task->rr, *p_task->input_image, *p_task->output_image, H, W);
            }
            else {
                printf("~~~~ Kernel type %d to CPU\n", p_task->k_data.task_type);
                Ctrl_HostTask(ctrl, GaussianBlur, p_task->rr, reset_host, *p_task->input_image, *p_task->output_image, H, W);
            }
        }
        loaded_kernel_state[rr] = RUNNING;
        n_enqueued_kernels++;

        int wait_for_tasks = 0;

        if(task < n_tasks - ((use_CPU) ? N_RR+1 : n_rr))
            wait_for_tasks = (n_enqueued_kernels >= ((use_CPU) ? N_RR+1 : n_rr) && finished_tasks < n_tasks);
        else
            wait_for_tasks = finished_tasks < n_tasks;
        
        if(wait_for_tasks) {
            n_interrupts[task] = wait_finish();
            
            process_finished(use_CPU, loaded_kernel_state, &finished_tasks, n_rr, &n_enqueued_kernels);
        }
    }
    int remaining_tasks = n_tasks - finished_tasks;
    printf("remaining_tasks: %d\n", remaining_tasks);

    int old_finished_tasks = 0;
    finished_tasks = 0;
    while(remaining_tasks > 0) {
        wait_finish();

        process_finished(use_CPU, loaded_kernel_state, &finished_tasks, n_rr, &n_enqueued_kernels);

        printf("finished_tasks: %d\n", finished_tasks);
        remaining_tasks -= (finished_tasks - old_finished_tasks);
        old_finished_tasks = finished_tasks;
    }

    free(arrival_times);
}
