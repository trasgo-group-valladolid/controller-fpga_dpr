#include "scheduler.h"

void setup_PYNQ_scheduler(PCtrl ctrl, Ctrl_Thread threads, Ctrl_Thread group, int n_tasks, int use_CPU, int n_rr) {
    // Setup of FPGA descriptor
    sprintf(FPGA.pblock_names[0], "medianblur_0");
    sprintf(FPGA.pblock_names[1], "gaussianblur_0");

    FPGA.loaded_kernels[0] = GAUSSIAN;
    FPGA.loaded_kernels[1] = GAUSSIAN;
    FPGA.loaded_kernels[2] = GAUSSIAN;

    FPGA.loaded_kernel_state[0] = FINISHED;
    FPGA.loaded_kernel_state[1] = FINISHED;

    if(use_CPU) {
        FPGA.loaded_kernel_state[N_RR] = FINISHED;
    }
    else {
        FPGA.loaded_kernel_state[N_RR] = UNAVAILABLE;
    }

    for(int i = ((use_CPU) ? N_RR+1 : n_rr); i < N_RR+1; i++) 
        FPGA.loaded_kernel_state[i] = UNAVAILABLE;

    for(int rr = 0; rr < ((use_CPU) ? N_RR+1 : n_rr); rr++) {
        FPGA.running_tasks[rr] = NULL;
    }

    // Setup of available kernels
    sprintf(kernel_name[0], "MedianBlur");
    sprintf(kernel_name[1], "GaussianBlur");

    CtrlConstructs.ctrl = ctrl;
    CtrlConstructs.threads = threads;
    CtrlConstructs.group = group;

    current_launch_task_id = -1;
    for(int rr = 0; rr < N_RR; rr++) {
        evicted_launch_task_id[rr] = -2;
    }

    evicted_host_task_id = -2;
    current_host_task_id = -1;

#ifdef _EXP_MODE
    exp_file = fopen("/tmp/ramdisk/exp_file.txt", "w");
    init_time = omp_get_wtime();
#endif
}

void update_timeout(struct timeval * timeout, int last_arrived) {
    static int current_task = -1;
    int new_task = 0;

    if(last_arrived > current_task) {
        current_task = last_arrived;
        new_task = 1;
    }

    if(new_task) {
        int sec = floor(pregeneratedTasks.arrival_times[last_arrived]);
        int usec = (pregeneratedTasks.arrival_times[last_arrived] - sec) * 100000; 

        timeout->tv_sec = sec;
        timeout->tv_usec = usec;
    }
}

int has_finished(int n_tasks, int use_CPU, int n_rr, int * tasks_to_arrive) {
    static int finished_tasks = 0;
    static int n_current_task = 0;

    if(timeout.tv_sec == 0 && timeout.tv_usec == 0)
        n_current_task++;

    if(n_current_task <= n_tasks)
       *tasks_to_arrive = 1;
    else
       *tasks_to_arrive = 0; 

    for(int rr = 0; rr < ((use_CPU) ? N_RR+1 : n_rr); rr++) {
        if(generated_interrupts[rr]) {
#ifdef _DEBUG
            printf("FINISHED RR %d\n", rr);
#elif _EXP_MODE
            fprintf(exp_file, "F %d %lf\n", rr, omp_get_wtime() - init_time);
#endif
            generated_interrupts[rr] = 0;
            if(rr < N_RR) {
#ifdef _DEBUG
                printf("Unsetting lock_RR[%d]\n", rr);
#endif
                omp_unset_lock(&lock_RR[rr]);

                #pragma omp atomic write
                launched_kernel[rr] = 0;
            }

            FPGA.loaded_kernel_state[rr] = FINISHED;

            finished_tasks++;

            //// For testing purposes
            //if (FPGA.running_tasks[rr]->preempted) {
            //    printf("Storing output of preempted task %d\n", FPGA.running_tasks[rr]->id);
            //    for(int i = 0; i < H_NROW; i++) {
            //        for(int j = 0; j < H_NCOL; j++) {
            //            hit(partial_output, i, j) = hit(pregeneratedTasks.output_images[FPGA.running_tasks[rr]->id], i, j);
            //            //hit(partial_output, i, j) = 0;
            //        }
            //    }
            //}
        }
    }

    if(finished_tasks == n_tasks)
        return 1;
    else
        return 0;
}

void context_switch_thread() {
    struct timespec eviction_time = {
        .tv_sec = 0,
        .tv_nsec = 1
    };

    nanosleep(&eviction_time, NULL);
}


