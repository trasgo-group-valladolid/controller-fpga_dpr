#ifndef _SCHEDULER_
#define _SCHEDULER_

#include "Ctrl.h"
#include "median-blur.h"
#include "gaussian-blur.h"

#define N_PRIORITIES 5
#define MAX_N_TASK 50


int H, W;
int H_NROW, H_NCOL;

// Scheduler task types
typedef enum schedulerTaskType {
   SCHEDULER_MEDIAN_1,
   SCHEDULER_MEDIAN_2,
   SCHEDULER_MEDIAN_3,
   SCHEDULER_GAUSSIAN,
   number_of_sch_task_types
} schedulerTaskType;

typedef struct kernel_data {
    int iters;
    schedulerTaskType task_type;
    double arrival_time;
} kernel_data;

typedef struct schedulerTask {
    int id;
    kernel_data k_data;
    struct context context;
    HitTile_int * input_image;
    HitTile_int * output_image;
    int rr;
    int priority;
    int enqueued;
    int preempted;
} schedulerTask;

typedef struct schedulerQueue {
    schedulerTask * queue[MAX_N_TASK];
    int head;
    int tail;
} schedulerQueue;

schedulerQueue task_queue[N_PRIORITIES];


typedef enum kernels {
    MEDIAN,
    GAUSSIAN,
    number_of_kernel_types
} kernels;

typedef enum kernel_state {
    RUNNING,
    FINISHED,
    UNAVAILABLE
} kernel_state;

char kernel_name[number_of_kernel_types][50];

typedef struct t_FPGA {
    schedulerTask * running_tasks[N_RR+1];
    kernels loaded_kernels[N_RR+1];
    kernel_state loaded_kernel_state[N_RR+1];
    char pblock_names[N_RR][50];
} t_FPGA;

t_FPGA FPGA;

typedef struct t_pregeneratedTasks {
    schedulerTask all_tasks[N_PRIORITIES * MAX_N_TASK];
    double * arrival_times;
    HitTile_int * input_images;
    HitTile_int * output_images;
    //kernel_data * k_data;
} t_pregeneratedTasks;

t_pregeneratedTasks pregeneratedTasks;


struct timeval timeout;

typedef struct t_CtrlConstructs {
    PCtrl ctrl;
    Ctrl_Thread threads;
    Ctrl_Thread group;
} t_CtrlConstructs;

t_CtrlConstructs CtrlConstructs;

extern int reset_host;

// For testing purposes
HitTile_int partial_output;


// Scheduler functions
void task_generator(PCtrl ctrl, int n_tasks, double total_time, int enabled_priorities, int H, int W);
void insert_task(schedulerTask * task, schedulerQueue * queue);
schedulerTask * pop_task(schedulerQueue * queue);
void PYNQ_scheduler(PCtrl ctrl, Ctrl_Thread threads, Ctrl_Thread group, int n_tasks, int use_CPU, int n_rr, int enabled_priorities, int enabled_preemption);
void setup_PYNQ_scheduler(PCtrl ctrl, Ctrl_Thread threads, Ctrl_Thread group, int n_tasks, int use_CPU, int n_rr);
void update_timeout(struct timeval *, int last_arrived);
int has_finished(int n_tasks, int use_CPU, int n_rr, int * tasks_to_arrive);
schedulerTask * get_arrived_task();
schedulerTask * get_task_from_queue(int enabled_priorities, int enabled_preemption, int use_CPU);
void serve_task(schedulerTask * task, int arrived, int use_CPU, int n_rr, int enabled_preemption);
void enqueue(schedulerTask * task);
void swap(schedulerTask * task, int rr);
void launch(schedulerTask * task, int rr, int n_rr);
void run(schedulerTask * task, int rr, int n_rr);
void context_switch_thread();
// For testing purposes
void run_host_task(int n_task, int kernel);
void run_kernel_task(int n_task);
#endif // _SCHEDULER_
