#include "scheduler.h"

void process_finished(int use_CPU, kernel_state * loaded_kernel_state, int * finished_tasks, int n_rr, int * n_enqueued_kernels) {
    for(int finished_rr = 0; finished_rr < ((use_CPU) ? N_RR+1 : n_rr); finished_rr++) {
        if(generated_interrupts[finished_rr]) {
            #ifdef _DEBUG_OUTPUTS
            printf("---> FINISHED %d\n", finished_rr);
            #endif // _DEBUG_OUTPUTS
            loaded_kernel_state[finished_rr] = FINISHED;
            generated_interrupts[finished_rr] = 0;
            (*finished_tasks)++;
            (*n_enqueued_kernels)--;
        }
    }
}


schedulerTask * get_next_task(int enabled_priorities, int n_current_task) {
    if(enabled_priorities) {
        printf("//// QUEUE TAILS: ");
        for(int i = 0; i < 5; i++) {
            printf("%d ", task_queue_tail[i]);
        }

        printf("//// QUEUE HEADS: ");
        for(int i = 0; i < 5; i++) {
            printf("%d ", task_queue_head[i]);
        }

        int priority;
        for(priority = 0; task_queue_head[priority] == task_queue_tail[priority]; priority++);
        printf("Queue: %d\n", priority);

        return &task_queue[priority][task_queue_head[priority]++];
    }
    else {
        return &task_queue[0][n_current_task];
    }
}
