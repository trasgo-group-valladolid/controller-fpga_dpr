ARR 2 0 1.066357
CrS GaussianBlur GaussianBlur medianblur_0 1.066434
CrL M 2 0 0 1.066458
S /home/xilinx/Bitstreams/MedianBlur-GaussianBlur_pblock_medianblur_0_partial.bit 1.167237
L MedianBlur 0 1.590332
ARR 0 1 1.666134
CrS MedianBlur GaussianBlur gaussianblur_0 1.666168
CrL M 0 1 1 1.666202
S /home/xilinx/Bitstreams/MedianBlur-MedianBlur_pblock_gaussianblur_0_partial.bit 1.767417
L MedianBlur 1 2.106887
ARR 4 2 2.196536
R 0 2.196894
EQ 2 0
EV 0 2 0 2.197131
CrS MedianBlur MedianBlur medianblur_0 2.197151
CrL G 4 2 0 2.197170
S /home/xilinx/Bitstreams/GaussianBlur-MedianBlur_pblock_medianblur_0_partial.bit 2.297014
L GaussianBlur 0 2.637529
ARR 2 3 2.661265
R 1 2.661561
EQ 0 1
EV 1 3 1 2.661755
CrS MedianBlur MedianBlur gaussianblur_0 2.661771
CrL G 2 3 1 2.661788
S /home/xilinx/Bitstreams/GaussianBlur-GaussianBlur_pblock_gaussianblur_0_partial.bit 2.767011
L GaussianBlur 1 3.203825
ARR 3 4 3.259352
R 1 3.259663
EQ 2 3
EV 3 4 1 3.259907
CrS MedianBlur MedianBlur gaussianblur_0 3.259925
CrL M 3 4 1 3.259942
S /home/xilinx/Bitstreams/GaussianBlur-MedianBlur_pblock_gaussianblur_0_partial.bit 3.357030
L MedianBlur 1 3.697584
ARR 1 5 3.720064
EQ 1 5
ARR 1 6 3.722212
EQ 1 6
ARR 4 7 3.727003
R 1 3.727311
EQ 3 4
EV 4 7 1 3.727496
CrS MedianBlur MedianBlur gaussianblur_0 3.727512
CrL G 4 7 1 3.727529
S /home/xilinx/Bitstreams/GaussianBlur-GaussianBlur_pblock_gaussianblur_0_partial.bit 3.836883
L GaussianBlur 1 4.172267
F 0 4.172513
DQ 0 1
CrS GaussianBlur MedianBlur medianblur_0 4.172543
CrL M 0 1 0 4.172561
S /home/xilinx/Bitstreams/MedianBlur-GaussianBlur_pblock_medianblur_0_partial.bit 4.287156
L MedianBlur 0 4.650157
ARR 2 8 4.708437
R 0 4.708748
EQ 0 1
EV 1 8 0 4.708944
CrS MedianBlur MedianBlur medianblur_0 4.708960
CrL G 2 8 0 4.708977
S /home/xilinx/Bitstreams/GaussianBlur-GaussianBlur_pblock_medianblur_0_partial.bit 4.817036
L GaussianBlur 0 5.167847
ARR 0 9 5.177988
EQ 0 9
ARR 2 10 5.225361
EQ 2 10
ARR 2 11 5.246845
EQ 2 11
ARR 1 12 5.270837
EQ 1 12
ARR 3 13 5.314395
R 0 5.314708
EQ 2 8
EV 8 13 0 5.314893
CrS MedianBlur MedianBlur medianblur_0 5.314910
CrL M 3 13 0 5.314927
S /home/xilinx/Bitstreams/MedianBlur-GaussianBlur_pblock_medianblur_0_partial.bit 5.407388
L MedianBlur 0 5.744500
F 1 5.759986
DQ 0 1
CrS MedianBlur GaussianBlur gaussianblur_0 5.760017
CrL M 0 1 1 5.760035
S /home/xilinx/Bitstreams/MedianBlur-MedianBlur_pblock_gaussianblur_0_partial.bit 5.847785
L MedianBlur 1 6.176988
ARR 4 14 6.193215
R 0 6.193526
EQ 3 13
EV 13 14 0 6.193718
CrL M 4 14 0 6.193730
ARR 4 15 6.199948
R 1 6.200234
EQ 0 1
EV 1 15 1 6.200419
CrL M 4 15 1 6.200430
L MedianBlur 1 6.239837
L MedianBlur 0 6.249939
ARR 3 16 6.291272
EQ 3 16
ARR 0 17 6.326127
EQ 0 17
ARR 2 18 6.328694
EQ 2 18
ARR 1 19 7.377306
EQ 1 19
ARR 4 20 7.387288
EQ 4 20
ARR 0 21 7.435050
EQ 0 21
ARR 3 22 7.475817
EQ 3 22
F 1 7.529500
DQ 0 9
CrL M 0 9 1 7.529530
ARR 0 23 7.539561
EQ 0 23
L MedianBlur 1 7.539912
ARR 4 24 7.564067
R 1 7.564384
EQ 0 9
EV 9 24 1 7.564570
CrS MedianBlur MedianBlur gaussianblur_0 7.564586
CrL G 4 24 1 7.564602
S /home/xilinx/Bitstreams/MedianBlur-GaussianBlur_pblock_gaussianblur_0_partial.bit 7.657026
L GaussianBlur 1 8.005078
ARR 4 25 8.029532
EQ 4 25
ARR 2 26 8.050910
EQ 2 26
ARR 3 27 8.076851
EQ 3 27
ARR 3 28 8.079781
EQ 3 28
ARR 4 29 8.111545
EQ 4 29
F 0 8.173417
DQ 0 1
CrL M 0 1 0 8.173446
L MedianBlur 0 8.199822
F 1 9.151244
DQ 0 17
CrS MedianBlur GaussianBlur gaussianblur_0 9.151289
CrL M 0 17 1 9.151311
S /home/xilinx/Bitstreams/MedianBlur-MedianBlur_pblock_gaussianblur_0_partial.bit 9.247094
L MedianBlur 1 9.582419
F 0 10.094915
DQ 0 21
CrL M 0 21 0 10.094955
L MedianBlur 0 10.129819
F 0 11.420433
DQ 0 23
CrL M 0 23 0 11.420475
L MedianBlur 0 11.449926
F 1 11.515053
DQ 0 9
CrL M 0 9 1 11.515092
L MedianBlur 1 11.515136
F 0 12.737488
DQ 1 5
CrL M 1 5 0 12.737532
L MedianBlur 0 12.799819
F 1 13.423351
DQ 1 6
CrS MedianBlur GaussianBlur gaussianblur_0 13.423399
CrL G 1 6 1 13.423424
S /home/xilinx/Bitstreams/MedianBlur-GaussianBlur_pblock_gaussianblur_0_partial.bit 13.527045
L GaussianBlur 1 13.837468
F 0 13.837676
DQ 1 12
CrL M 1 12 0 13.837706
L MedianBlur 0 13.879821
F 1 14.983527
DQ 1 19
CrS MedianBlur GaussianBlur gaussianblur_0 14.983570
CrL M 1 19 1 14.983593
S /home/xilinx/Bitstreams/MedianBlur-MedianBlur_pblock_gaussianblur_0_partial.bit 15.101452
L MedianBlur 1 15.411106
F 0 15.411261
DQ 2 0
CrL M 2 0 0 15.411290
L MedianBlur 0 15.439835
F 0 15.469954
DQ 2 3
CrS GaussianBlur MedianBlur medianblur_0 15.469987
CrL G 2 3 0 15.470008
S /home/xilinx/Bitstreams/GaussianBlur-MedianBlur_pblock_medianblur_0_partial.bit 15.567477
L GaussianBlur 0 15.910968
F 0 17.057287
DQ 2 10
CrS GaussianBlur MedianBlur medianblur_0 17.057341
CrL M 2 10 0 17.057366
S /home/xilinx/Bitstreams/MedianBlur-MedianBlur_pblock_medianblur_0_partial.bit 17.187728
L MedianBlur 0 17.552469
F 1 17.552700
DQ 2 11
CrL M 2 11 1 17.552731
L MedianBlur 1 17.559843
F 0 18.198749
DQ 2 8
CrS GaussianBlur MedianBlur medianblur_0 18.198806
CrL G 2 8 0 18.198830
S /home/xilinx/Bitstreams/GaussianBlur-MedianBlur_pblock_medianblur_0_partial.bit 18.307050
L GaussianBlur 0 18.659959
F 1 19.480044
DQ 2 18
CrL M 2 18 1 19.480101
L MedianBlur 1 19.509820
F 0 19.806088
DQ 2 26
CrL G 2 26 0 19.806118
L GaussianBlur 0 19.859820
F 0 21.006019
DQ 3 4
CrS GaussianBlur MedianBlur medianblur_0 21.006060
CrL M 3 4 0 21.006081
S /home/xilinx/Bitstreams/MedianBlur-MedianBlur_pblock_medianblur_0_partial.bit 21.123406
L MedianBlur 0 21.451250
F 1 21.451477
DQ 3 13
CrL M 3 13 1 21.451507
L MedianBlur 1 21.469824
F 0 22.095391
DQ 3 16
CrL M 3 16 0 22.095434
L MedianBlur 0 22.129822
F 0 22.775644
DQ 3 22
CrL M 3 22 0 22.775687
L MedianBlur 0 22.779836
F 1 22.942652
DQ 3 27
CrL M 3 27 1 22.942681
L MedianBlur 1 22.979823
F 0 24.062885
DQ 3 28
CrL M 3 28 0 24.062924
L MedianBlur 0 24.109819
F 1 24.258184
DQ 4 20
CrL M 4 20 1 24.258214
L MedianBlur 1 24.299817
F 0 25.399118
DQ 4 25
CrL M 4 25 0 25.399158
L MedianBlur 0 25.449821
F 1 26.233304
DQ 4 29
CrS MedianBlur GaussianBlur gaussianblur_0 26.233340
CrL G 4 29 1 26.233361
S /home/xilinx/Bitstreams/MedianBlur-GaussianBlur_pblock_gaussianblur_0_partial.bit 26.347213
L GaussianBlur 1 26.667690
F 0 27.369144
F 1 27.811862
END 27.811892
