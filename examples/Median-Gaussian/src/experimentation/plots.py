import matplotlib.pyplot as plt
import sys

N_TASKS = 30
times = [0.1,0.5,0.8]

N_PRIORITIES = 5


colours = ['b', 'r', 'g', 'y', 'c']

N_RR = int(sys.argv[2])



if sys.argv[1] == "stacked":
    #size = 600
    for mode in ["no-preemption", "preemption"]:
        for size in [200, 300, 400, 500, 600]:
        #for size in [600]:
            plt.xlabel("Rate of task arrival")
            plt.ylabel("Service time (s)")

            previous_serving_times = [0,0,0]

            serving_times = [[] for i in range(N_PRIORITIES)]
            times_std = [[] for i in range(N_PRIORITIES)]

            #For seed 15 and 2 RR
            if N_RR == 2:
                plt.ylim([0,60])
            #For seed 15 and 1 RR
            if N_RR == 1:
                plt.ylim([0,120])

            for p in range(N_PRIORITIES):
                for i in range(len(times)):
                    #f = open(f"serving_times/serving_times-{mode}-0_{int(times[i]*10)}.txt")
                    f = open(f"serving_times/avg_service_times-{N_RR}rr-{mode}-{times[i]}-{size}-priority-15.txt")

                    # Discard previous priorities lines
                    for j in range(p):
                        f.readline()
                    line = f.readline().strip()
                    mean_time = float(line.split()[0])
                    std_time = float(line.split()[1])
                    serving_times[p].append(mean_time)
                    times_std[p].append(std_time)

                current_colour = list(colours[p])

                plt.bar(["Busy", "Medium", "Idle"], serving_times[p], yerr=times_std[p], bottom=previous_serving_times, color=current_colour, label=f"Priority {p}")
                for i in range(len(times)):
                    previous_serving_times[i] += serving_times[p][i]

            plt.legend()

            #plt.show()
            plt.savefig(f"serving_times/service_times-{mode}-{N_RR}rr-{size}-15.pdf")
            plt.clf()

elif sys.argv[1] == "points":
    sizes = [200,300,400,500,600]
    arrival_rates = ["Busy", "Medium", "Idle"]


    for mode in ["no-preemption", "preemption"]:
        #For seed 15 and 2 RR
        if N_RR == 2:
            plt.ylim([0,6])
        #For seed 15 and 1 RR
        if N_RR == 1:
            plt.ylim([0,3.8])

        plt.xlabel("Size (NxN)")
        plt.ylabel("Throughput (tasks/s)")
        time_counter = 0

        for time in [0.1, 0.5, 0.8]:
            mean_throughput = []
            std_throughput = []

            f = open(f"throughput/throughput-{N_RR}-{mode}-{time}.txt")

            for size in range(len([200,300,400,500,600])):
                line = f.readline()
                mean_throughput.append(float(line.split()[0]))
                std_throughput.append(float(line.split()[1]))

            plt.errorbar([200,300,400,500,600], mean_throughput, yerr=std_throughput, marker='o', label=f"{arrival_rates[time_counter]}")
            time_counter += 1
        plt.legend()

        f = open(f"throughput/full_{N_RR}rr-{mode}-0.1.txt")
        mean_throughput = []
        for size in sizes:
            mean_throughput.append(float(f.readline()))
        plt.errorbar([200,300,400,500,600], mean_throughput, yerr=std_throughput, marker='o', linestyle="dashed", label=f"Full reconfiguration")
        plt.legend()

        plt.savefig(f"throughput/throughput-{N_RR}rr-{mode}.pdf")
        plt.clf()

