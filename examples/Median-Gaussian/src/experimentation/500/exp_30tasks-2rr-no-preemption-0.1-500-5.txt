ARR 2 0 0.082810
CrS GaussianBlur GaussianBlur medianblur_0 0.082852
CrL M 2 0 0 0.082930
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-GaussianBlur_pblock_medianblur_0_partial.bit 0.110460
L MedianBlur 0 0.432368
ARR 0 1 0.472925
CrS MedianBlur GaussianBlur gaussianblur_0 0.472948
CrL M 0 1 1 0.473014
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-MedianBlur_pblock_gaussianblur_0_partial.bit 0.480683
L MedianBlur 1 0.811656
ARR 4 2 0.864606
EQ 4 2
ARR 2 3 0.875423
EQ 2 3
F 0 0.899018
DQ 2 3
CrS GaussianBlur MedianBlur medianblur_0 0.899046
CrL G 2 3 0 0.899106
S /home/xilinx/Bitstreams-axi_firewall/GaussianBlur-MedianBlur_pblock_medianblur_0_partial.bit 0.910360
L GaussianBlur 0 1.225454
ARR 3 4 1.239772
EQ 3 4
ARR 1 5 1.251692
EQ 1 5
ARR 1 6 1.252800
EQ 1 6
ARR 4 7 1.255235
EQ 4 7
ARR 2 8 1.279650
EQ 2 8
ARR 0 9 1.283813
EQ 0 9
ARR 2 10 1.307525
EQ 2 10
ARR 2 11 1.318300
EQ 2 11
ARR 1 12 1.330329
EQ 1 12
ARR 3 13 1.352143
EQ 3 13
ARR 4 14 1.360363
EQ 4 14
ARR 4 15 1.363499
EQ 4 15
ARR 3 16 1.408928
EQ 3 16
ARR 0 17 1.426387
EQ 0 17
ARR 2 18 1.427711
EQ 2 18
ARR 1 19 1.501569
EQ 1 19
ARR 4 20 1.506587
EQ 4 20
ARR 0 21 1.530501
EQ 0 21
ARR 3 22 1.549550
EQ 3 22
ARR 0 23 1.581214
EQ 0 23
ARR 4 24 1.593500
EQ 4 24
ARR 4 25 1.605826
EQ 4 25
ARR 2 26 1.616534
EQ 2 26
ARR 3 27 1.629529
EQ 3 27
ARR 3 28 1.631031
EQ 3 28
ARR 4 29 1.646932
EQ 4 29
F 0 2.043850
DQ 0 9
CrS GaussianBlur MedianBlur medianblur_0 2.043886
CrL M 0 9 0 2.043955
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-MedianBlur_pblock_medianblur_0_partial.bit 2.070357
L MedianBlur 0 2.382298
F 1 2.382374
DQ 0 17
CrL M 0 17 1 2.382980
L MedianBlur 1 2.409790
F 0 3.794170
DQ 0 21
CrL M 0 21 0 3.794249
F 1 3.824426
DQ 0 23
CrL M 0 23 1 3.824487
L MedianBlur 0 3.829805
L MedianBlur 1 3.849780
F 0 4.770446
DQ 1 5
CrL M 1 5 0 4.770505
L MedianBlur 0 4.789811
F 1 4.793131
DQ 1 6
CrS MedianBlur GaussianBlur gaussianblur_0 4.793153
CrL G 1 6 1 4.793206
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-GaussianBlur_pblock_gaussianblur_0_partial.bit 4.810363
L GaussianBlur 1 5.132859
F 0 5.255471
DQ 1 12
CrL M 1 12 0 5.255548
L MedianBlur 0 5.279782
F 1 5.951082
DQ 1 19
CrS MedianBlur GaussianBlur gaussianblur_0 5.951120
CrL M 1 19 1 5.951202
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-MedianBlur_pblock_gaussianblur_0_partial.bit 5.951745
L MedianBlur 1 6.258022
F 0 6.259868
DQ 2 8
CrS GaussianBlur MedianBlur medianblur_0 6.259894
CrL G 2 8 0 6.259962
S /home/xilinx/Bitstreams-axi_firewall/GaussianBlur-MedianBlur_pblock_medianblur_0_partial.bit 6.260449
L GaussianBlur 0 6.584230
F 0 7.402500
DQ 2 10
CrS GaussianBlur MedianBlur medianblur_0 7.402528
CrL M 2 10 0 7.402594
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-MedianBlur_pblock_medianblur_0_partial.bit 7.420325
L MedianBlur 0 7.752730
F 1 7.759901
DQ 2 11
CrL M 2 11 1 7.760516
L MedianBlur 1 7.769805
F 0 8.225091
DQ 2 18
CrL M 2 18 0 8.225159
L MedianBlur 0 8.259780
F 1 9.170356
DQ 2 26
CrS MedianBlur GaussianBlur gaussianblur_0 9.170385
CrL G 2 26 1 9.170445
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-GaussianBlur_pblock_gaussianblur_0_partial.bit 9.190358
L GaussianBlur 1 9.488663
F 0 9.655536
DQ 3 4
CrL M 3 4 0 9.655608
L MedianBlur 0 9.659798
F 0 10.126046
DQ 3 13
CrL M 3 13 0 10.126133
L MedianBlur 0 10.169780
F 1 10.306749
DQ 3 16
CrS MedianBlur GaussianBlur gaussianblur_0 10.306779
CrL M 3 16 1 10.306848
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-MedianBlur_pblock_gaussianblur_0_partial.bit 10.307387
L MedianBlur 1 10.626875
F 1 11.100049
DQ 3 22
CrL M 3 22 1 11.100127
L MedianBlur 1 11.159812
F 0 11.579067
DQ 3 27
CrL M 3 27 0 11.579131
L MedianBlur 0 11.579788
F 1 12.104953
DQ 3 28
CrL M 3 28 1 12.105036
L MedianBlur 1 12.159780
F 0 12.521013
DQ 4 2
CrS GaussianBlur MedianBlur medianblur_0 12.521051
CrL G 4 2 0 12.521120
S /home/xilinx/Bitstreams-axi_firewall/GaussianBlur-MedianBlur_pblock_medianblur_0_partial.bit 12.550359
L GaussianBlur 0 12.873041
F 1 13.093292
DQ 4 7
CrS MedianBlur GaussianBlur gaussianblur_0 13.093326
CrL G 4 7 1 13.093396
S /home/xilinx/Bitstreams-axi_firewall/GaussianBlur-GaussianBlur_pblock_gaussianblur_0_partial.bit 13.100392
L GaussianBlur 1 13.403698
F 0 13.691036
DQ 4 14
CrS GaussianBlur MedianBlur medianblur_0 13.691079
CrL M 4 14 0 13.691156
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-GaussianBlur_pblock_medianblur_0_partial.bit 13.720341
L MedianBlur 0 14.039152
F 1 14.221652
DQ 4 15
CrS MedianBlur GaussianBlur gaussianblur_0 14.221684
CrL M 4 15 1 14.221760
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-MedianBlur_pblock_gaussianblur_0_partial.bit 14.222238
L MedianBlur 1 14.517565
F 0 15.447273
DQ 4 20
CrL M 4 20 0 15.447352
F 1 15.462373
DQ 4 24
CrS MedianBlur GaussianBlur gaussianblur_0 15.462408
CrL G 4 24 1 15.462471
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-GaussianBlur_pblock_gaussianblur_0_partial.bit 15.480338
L MedianBlur 0 15.499804
L GaussianBlur 1 15.785317
F 1 16.603610
DQ 4 25
CrS MedianBlur GaussianBlur gaussianblur_0 16.603647
CrL M 4 25 1 16.603719
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-MedianBlur_pblock_gaussianblur_0_partial.bit 16.630346
L MedianBlur 1 16.952091
F 0 16.952164
DQ 4 29
CrS GaussianBlur MedianBlur medianblur_0 16.952195
CrL G 4 29 0 16.952266
S /home/xilinx/Bitstreams-axi_firewall/GaussianBlur-MedianBlur_pblock_medianblur_0_partial.bit 16.980329
L GaussianBlur 0 17.330894
F 0 18.149177
F 1 18.348354
END 18.348378
