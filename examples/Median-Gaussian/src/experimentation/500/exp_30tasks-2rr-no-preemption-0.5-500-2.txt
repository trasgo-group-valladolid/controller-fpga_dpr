ARR 2 0 4.016139
CrS GaussianBlur GaussianBlur medianblur_0 4.016191
CrL M 2 0 0 4.016272
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-GaussianBlur_pblock_medianblur_0_partial.bit 4.020456
L MedianBlur 0 4.352252
F 0 4.815524
ARR 0 1 5.418072
CrL M 0 1 0 5.418134
L MedianBlur 0 5.429790
F 0 6.819371
ARR 4 2 7.442474
CrS GaussianBlur GaussianBlur medianblur_0 7.442501
CrL G 4 2 0 7.442562
S /home/xilinx/Bitstreams-axi_firewall/GaussianBlur-GaussianBlur_pblock_medianblur_0_partial.bit 7.470357
L GaussianBlur 0 7.796109
ARR 2 3 7.849145
CrL G 2 3 1 7.849210
L GaussianBlur 1 7.889791
F 0 8.624897
F 1 8.718562
ARR 3 4 8.888022
CrS GaussianBlur GaussianBlur medianblur_0 8.888046
CrL M 3 4 0 8.888103
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-GaussianBlur_pblock_medianblur_0_partial.bit 8.910349
L MedianBlur 0 9.228149
ARR 1 5 9.283972
CrS MedianBlur GaussianBlur gaussianblur_0 9.283996
CrL M 1 5 1 9.284061
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-MedianBlur_pblock_gaussianblur_0_partial.bit 9.310590
L MedianBlur 1 9.615317
ARR 1 6 9.619920
EQ 1 6
ARR 4 7 9.631783
EQ 4 7
F 0 9.694594
DQ 1 6
CrS GaussianBlur MedianBlur medianblur_0 9.694629
CrL G 1 6 0 9.694695
S /home/xilinx/Bitstreams-axi_firewall/GaussianBlur-MedianBlur_pblock_medianblur_0_partial.bit 9.700457
L GaussianBlur 0 10.015034
F 1 10.082270
DQ 4 7
CrS MedianBlur GaussianBlur gaussianblur_0 10.082303
CrL G 4 7 1 10.082374
S /home/xilinx/Bitstreams-axi_firewall/GaussianBlur-GaussianBlur_pblock_gaussianblur_0_partial.bit 10.100332
L GaussianBlur 1 10.412589
F 0 10.834480
F 1 11.230884
ARR 2 8 11.309087
CrL G 2 8 0 11.309144
ARR 0 9 11.329054
CrS MedianBlur GaussianBlur gaussianblur_0 11.329082
CrL M 0 9 1 11.329139
S /home/xilinx/Bitstreams-axi_firewall/GaussianBlur-MedianBlur_pblock_gaussianblur_0_partial.bit 11.330330
L GaussianBlur 0 11.339945
L MedianBlur 1 11.651425
F 0 12.157920
ARR 2 10 12.678094
CrS GaussianBlur MedianBlur medianblur_0 12.678127
CrL M 2 10 0 12.678193
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-MedianBlur_pblock_medianblur_0_partial.bit 12.700371
L MedianBlur 0 13.004766
F 1 13.046141
ARR 2 11 13.062909
CrL M 2 11 1 13.062973
L MedianBlur 1 13.089792
ARR 1 12 13.122298
EQ 1 12
F 0 13.476814
DQ 1 12
CrL M 1 12 0 13.476881
L MedianBlur 0 13.479799
ARR 3 13 14.131379
EQ 3 13
ARR 4 14 14.171670
EQ 4 14
ARR 4 15 14.186490
EQ 4 15
F 0 14.412024
DQ 3 13
CrL M 3 13 0 14.412085
L MedianBlur 0 14.449787
F 1 14.491215
DQ 4 14
CrL M 4 14 1 14.491271
L MedianBlur 1 14.519791
F 0 15.866667
DQ 4 15
CrL M 4 15 0 15.866755
L MedianBlur 0 15.879792
F 1 15.935880
ARR 3 16 16.213932
CrL M 3 16 1 16.213996
L MedianBlur 1 16.249788
ARR 0 17 16.300486
EQ 0 17
ARR 2 18 16.306239
EQ 2 18
F 1 16.716433
DQ 0 17
CrL M 0 17 1 16.716492
L MedianBlur 1 16.769785
F 0 16.810715
DQ 2 18
CrL M 2 18 0 16.810775
L MedianBlur 0 16.839797
F 1 18.186685
F 0 18.254739
ARR 1 19 19.376457
CrL M 1 19 0 19.376517
L MedianBlur 0 19.399787
ARR 4 20 19.400685
CrL M 4 20 1 19.400740
L MedianBlur 1 19.449817
ARR 0 21 20.419879
EQ 0 21
ARR 3 22 20.514346
EQ 3 22
F 0 20.795540
DQ 0 21
CrL M 0 21 0 20.795603
L MedianBlur 0 20.839795
F 1 20.845110
DQ 3 22
CrL M 3 22 1 20.845166
L MedianBlur 1 20.889813
ARR 0 23 21.572937
EQ 0 23
ARR 4 24 21.633541
EQ 4 24
ARR 4 25 21.694378
EQ 4 25
ARR 2 26 21.747107
EQ 2 26
F 0 21.770141
DQ 0 23
CrL M 0 23 0 21.770201
L MedianBlur 0 21.799791
ARR 3 27 21.811478
EQ 3 27
ARR 3 28 21.818127
EQ 3 28
F 1 21.819993
DQ 2 26
CrS MedianBlur GaussianBlur gaussianblur_0 21.820021
CrL G 2 26 1 21.820079
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-GaussianBlur_pblock_gaussianblur_0_partial.bit 21.850441
L GaussianBlur 1 22.156166
ARR 4 29 22.236744
EQ 4 29
F 0 22.731710
DQ 3 27
CrL M 3 27 0 22.731785
L MedianBlur 0 22.759790
F 1 22.974388
DQ 3 28
CrS MedianBlur GaussianBlur gaussianblur_0 22.974418
CrL M 3 28 1 22.974479
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-MedianBlur_pblock_gaussianblur_0_partial.bit 22.990352
L MedianBlur 1 23.320270
F 0 23.690741
DQ 4 24
CrS GaussianBlur MedianBlur medianblur_0 23.690792
CrL G 4 24 0 23.690881
S /home/xilinx/Bitstreams-axi_firewall/GaussianBlur-MedianBlur_pblock_medianblur_0_partial.bit 23.700478
L GaussianBlur 0 24.040643
F 1 24.251355
DQ 4 25
CrL M 4 25 1 24.251432
L MedianBlur 1 24.269795
F 0 24.858861
DQ 4 29
CrL G 4 29 0 24.858927
L GaussianBlur 0 24.879788
F 1 25.668116
F 0 25.697974
END 25.698001
