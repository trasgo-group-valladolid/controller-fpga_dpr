ARR 2 0 4.016141
CrS GaussianBlur GaussianBlur medianblur_0 4.016198
CrL M 2 0 0 4.016281
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-GaussianBlur_pblock_medianblur_0_partial.bit 4.040533
L MedianBlur 0 4.375980
F 0 4.839246
ARR 0 1 5.445672
CrL M 0 1 0 5.445740
L MedianBlur 0 5.489842
F 0 6.879446
ARR 4 2 7.469914
CrS GaussianBlur GaussianBlur medianblur_0 7.469945
CrL G 4 2 0 7.470006
S /home/xilinx/Bitstreams-axi_firewall/GaussianBlur-GaussianBlur_pblock_medianblur_0_partial.bit 7.490416
L GaussianBlur 0 7.861429
ARR 2 3 7.914471
CrL G 2 3 1 7.914542
L GaussianBlur 1 7.949840
F 0 8.677441
F 1 8.765836
ARR 3 4 8.953361
CrS GaussianBlur GaussianBlur medianblur_0 8.953391
CrL M 3 4 0 8.953454
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-GaussianBlur_pblock_medianblur_0_partial.bit 8.953983
L MedianBlur 0 9.289442
ARR 1 5 9.345658
CrS MedianBlur GaussianBlur gaussianblur_0 9.345689
CrL M 1 5 1 9.345758
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-MedianBlur_pblock_gaussianblur_0_partial.bit 9.370618
L MedianBlur 1 9.679451
ARR 1 6 9.699855
EQ 1 6
ARR 4 7 9.711720
R 0 9.712069
EQ 3 4
EV 4 7 0 9.712266
CrS MedianBlur MedianBlur medianblur_0 9.712283
CrL G 4 7 0 9.712318
S /home/xilinx/Bitstreams-axi_firewall/GaussianBlur-MedianBlur_pblock_medianblur_0_partial.bit 9.730397
L GaussianBlur 0 10.071870
F 1 10.144941
DQ 1 6
CrS MedianBlur GaussianBlur gaussianblur_0 10.144978
CrL G 1 6 1 10.145047
S /home/xilinx/Bitstreams-axi_firewall/GaussianBlur-GaussianBlur_pblock_gaussianblur_0_partial.bit 10.160373
L GaussianBlur 1 10.492433
F 0 10.888615
DQ 3 4
CrS GaussianBlur MedianBlur medianblur_0 10.888658
CrL M 3 4 0 10.888736
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-GaussianBlur_pblock_medianblur_0_partial.bit 10.889229
L MedianBlur 0 11.260737
F 1 11.309141
F 0 11.724340
ARR 2 8 11.822331
CrS GaussianBlur GaussianBlur medianblur_0 11.822369
CrL G 2 8 0 11.822436
S /home/xilinx/Bitstreams-axi_firewall/GaussianBlur-GaussianBlur_pblock_medianblur_0_partial.bit 11.850405
L GaussianBlur 0 12.192216
ARR 0 9 12.219685
CrS MedianBlur GaussianBlur gaussianblur_0 12.219719
CrL M 0 9 1 12.219788
S /home/xilinx/Bitstreams-axi_firewall/GaussianBlur-MedianBlur_pblock_gaussianblur_0_partial.bit 12.240384
L MedianBlur 1 12.546706
F 0 13.010070
ARR 2 10 13.565057
CrS GaussianBlur MedianBlur medianblur_0 13.565102
CrL M 2 10 0 13.565176
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-MedianBlur_pblock_medianblur_0_partial.bit 13.580416
L MedianBlur 0 13.925353
F 1 13.940807
ARR 2 11 13.982943
CrL M 2 11 1 13.983013
L MedianBlur 1 14.036782
ARR 1 12 14.042337
EQ 1 12
F 0 14.393785
DQ 1 12
CrL M 1 12 0 14.393855
L MedianBlur 0 14.429829
ARR 3 13 15.051414
R 0 15.051734
EQ 1 12
EV 12 13 0 15.051924
CrL M 3 13 0 15.051954
L MedianBlur 0 15.069838
ARR 4 14 15.092209
R 0 15.092484
EQ 3 13
EV 13 14 0 15.092667
CrL M 4 14 0 15.092696
ARR 4 15 15.107539
R 1 15.107818
EQ 2 11
EV 11 15 1 15.108005
CrL M 4 15 1 15.108034
L MedianBlur 1 15.129833
L MedianBlur 0 15.149844
F 1 16.070307
DQ 1 12
CrL M 1 12 1 16.070394
L MedianBlur 1 16.089837
F 1 16.411658
DQ 2 11
CrL M 2 11 1 16.411734
L MedianBlur 1 16.411807
F 0 16.563404
DQ 3 13
CrL M 3 13 0 16.563475
L MedianBlur 0 16.589829
F 1 16.748988
ARR 3 16 17.135607
CrL M 3 16 1 17.135669
L MedianBlur 1 17.189826
ARR 0 17 17.222157
EQ 0 17
ARR 2 18 17.227906
EQ 2 18
F 1 17.662610
DQ 0 17
CrL M 0 17 1 17.662697
L MedianBlur 1 17.679836
F 0 17.966761
DQ 2 18
CrL M 2 18 0 17.966828
L MedianBlur 0 17.989830
F 1 19.086662
F 0 19.397192
ARR 1 19 20.297886
CrL M 1 19 0 20.297974
L MedianBlur 0 20.319837
ARR 4 20 20.322183
CrL M 4 20 1 20.322247
L MedianBlur 1 20.349842
ARR 0 21 21.341960
EQ 0 21
ARR 3 22 21.436400
R 0 21.436751
EQ 1 19
EV 19 22 0 21.436939
CrL M 3 22 0 21.436970
L MedianBlur 0 21.469832
F 1 21.763771
DQ 0 21
CrL M 0 21 1 21.763857
L MedianBlur 1 21.819861
F 0 22.407086
DQ 1 19
CrL M 1 19 0 22.407156
L MedianBlur 0 22.429832
ARR 0 23 22.494968
EQ 0 23
ARR 4 24 22.555557
R 0 22.555881
EQ 1 19
EV 19 24 0 22.556067
CrS MedianBlur MedianBlur medianblur_0 22.556087
CrL G 4 24 0 22.556124
S /home/xilinx/Bitstreams-axi_firewall/GaussianBlur-MedianBlur_pblock_medianblur_0_partial.bit 22.580397
L GaussianBlur 0 22.941901
F 1 22.959823
DQ 0 23
CrL M 0 23 1 22.960446
L MedianBlur 1 22.999837
ARR 4 25 23.021680
R 1 23.021982
EQ 0 23
EV 23 25 1 23.022171
CrL M 4 25 1 23.022200
ARR 2 26 23.074904
EQ 2 26
L MedianBlur 1 23.079847
ARR 3 27 23.139075
EQ 3 27
ARR 3 28 23.145724
EQ 3 28
ARR 4 29 23.224424
EQ 4 29
F 0 23.759827
DQ 0 23
CrS GaussianBlur MedianBlur medianblur_0 23.759876
CrL M 0 23 0 23.759948
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-MedianBlur_pblock_medianblur_0_partial.bit 23.780417
L MedianBlur 0 24.098757
F 1 24.476822
DQ 1 19
CrL M 1 19 1 24.476900
L MedianBlur 1 24.489834
F 1 24.665021
DQ 2 26
CrS MedianBlur GaussianBlur gaussianblur_0 24.665058
CrL G 2 26 1 24.665121
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-GaussianBlur_pblock_gaussianblur_0_partial.bit 24.690388
L GaussianBlur 1 24.998473
F 0 25.030664
DQ 3 27
CrL M 3 27 0 25.030741
L MedianBlur 0 25.079834
F 1 25.816621
DQ 3 28
CrS MedianBlur GaussianBlur gaussianblur_0 25.816669
CrL M 3 28 1 25.816743
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-MedianBlur_pblock_gaussianblur_0_partial.bit 25.840408
L MedianBlur 1 26.159020
F 0 26.169841
DQ 4 29
CrS GaussianBlur MedianBlur medianblur_0 26.169879
S /home/xilinx/Bitstreams-axi_firewall/GaussianBlur-MedianBlur_pblock_medianblur_0_partial.bit 26.170529
CrL G 4 29 0 26.170548
L GaussianBlur 0 26.499412
F 1 27.090977
F 0 27.316843
END 27.316865
