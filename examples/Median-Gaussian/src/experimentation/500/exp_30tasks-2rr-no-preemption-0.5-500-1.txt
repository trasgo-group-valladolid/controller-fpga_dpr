ARR 2 0 4.016136
CrS GaussianBlur GaussianBlur medianblur_0 4.016186
CrL M 2 0 0 4.016267
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-GaussianBlur_pblock_medianblur_0_partial.bit 4.020439
L MedianBlur 0 4.360513
F 0 4.823764
ARR 0 1 5.426330
CrL M 0 1 0 5.426391
L MedianBlur 0 5.439780
F 0 6.829336
ARR 4 2 7.450730
CrS GaussianBlur GaussianBlur medianblur_0 7.450756
CrL G 4 2 0 7.450818
S /home/xilinx/Bitstreams-axi_firewall/GaussianBlur-GaussianBlur_pblock_medianblur_0_partial.bit 7.480348
L GaussianBlur 0 7.759448
ARR 2 3 7.812486
CrL G 2 3 1 7.812552
L GaussianBlur 1 7.819796
F 0 8.575031
F 1 8.635355
ARR 3 4 8.851407
CrS GaussianBlur GaussianBlur medianblur_0 8.851431
CrL M 3 4 0 8.851487
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-GaussianBlur_pblock_medianblur_0_partial.bit 8.870355
L MedianBlur 0 9.181738
ARR 1 5 9.237561
CrS MedianBlur GaussianBlur gaussianblur_0 9.237586
CrL M 1 5 1 9.237662
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-MedianBlur_pblock_gaussianblur_0_partial.bit 9.238378
L MedianBlur 1 9.563615
ARR 1 6 9.574368
EQ 1 6
ARR 4 7 9.585673
EQ 4 7
F 0 9.647463
DQ 1 6
CrS GaussianBlur MedianBlur medianblur_0 9.647497
CrL G 1 6 0 9.647563
S /home/xilinx/Bitstreams-axi_firewall/GaussianBlur-MedianBlur_pblock_medianblur_0_partial.bit 9.670400
L GaussianBlur 0 10.011672
F 1 10.029097
DQ 4 7
CrS MedianBlur GaussianBlur gaussianblur_0 10.029130
CrL G 4 7 1 10.029203
S /home/xilinx/Bitstreams-axi_firewall/GaussianBlur-GaussianBlur_pblock_gaussianblur_0_partial.bit 10.029672
L GaussianBlur 1 10.373170
F 0 10.835497
F 1 11.196049
ARR 2 8 11.322397
CrL G 2 8 0 11.322457
ARR 0 9 11.342366
CrS MedianBlur GaussianBlur gaussianblur_0 11.342395
CrL M 0 9 1 11.342452
S /home/xilinx/Bitstreams-axi_firewall/GaussianBlur-MedianBlur_pblock_gaussianblur_0_partial.bit 11.350374
L GaussianBlur 0 11.359934
L MedianBlur 1 11.649221
F 0 12.177865
ARR 2 10 12.667473
CrS GaussianBlur MedianBlur medianblur_0 12.667505
CrL M 2 10 0 12.667570
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-MedianBlur_pblock_medianblur_0_partial.bit 12.680435
L MedianBlur 0 12.993801
F 1 13.043733
ARR 2 11 13.052902
CrL M 2 11 1 13.052967
L MedianBlur 1 13.089778
ARR 1 12 13.112294
EQ 1 12
F 0 13.459438
DQ 1 12
CrL M 1 12 0 13.459508
L MedianBlur 0 13.459788
ARR 3 13 14.121387
EQ 3 13
ARR 4 14 14.161676
EQ 4 14
ARR 4 15 14.176493
EQ 4 15
F 0 14.397875
DQ 3 13
CrL M 3 13 0 14.397938
L MedianBlur 0 14.419778
F 1 14.490054
DQ 4 14
CrL M 4 14 1 14.490115
L MedianBlur 1 14.519778
F 0 15.834966
DQ 4 15
CrL M 4 15 0 15.835056
L MedianBlur 0 15.869789
F 1 15.934228
ARR 3 16 16.203934
CrL M 3 16 1 16.203993
L MedianBlur 1 16.219780
ARR 0 17 16.290478
EQ 0 17
ARR 2 18 16.296225
EQ 2 18
F 1 16.691681
DQ 0 17
CrL M 0 17 1 16.691743
L MedianBlur 1 16.699791
F 0 16.805820
DQ 2 18
CrL M 2 18 0 16.805878
L MedianBlur 0 16.839778
F 1 18.107408
F 0 18.243562
ARR 1 19 19.366462
CrL M 1 19 0 19.366542
L MedianBlur 0 19.369807
ARR 4 20 19.390721
CrL M 4 20 1 19.390779
L MedianBlur 1 19.419809
ARR 0 21 20.409871
EQ 0 21
ARR 3 22 20.504345
EQ 3 22
F 0 20.765619
DQ 0 21
CrL M 0 21 0 20.765685
L MedianBlur 0 20.809777
F 1 20.815133
DQ 3 22
CrL M 3 22 1 20.815192
L MedianBlur 1 20.829779
ARR 0 23 21.562990
EQ 0 23
ARR 4 24 21.623600
EQ 4 24
ARR 4 25 21.684416
EQ 4 25
ARR 2 26 21.737138
EQ 2 26
F 0 21.750042
DQ 0 23
CrL M 0 23 0 21.750104
F 1 21.772718
DQ 2 26
CrS MedianBlur GaussianBlur gaussianblur_0 21.772749
CrL G 2 26 1 21.772805
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-GaussianBlur_pblock_gaussianblur_0_partial.bit 21.800359
L MedianBlur 0 21.809897
L GaussianBlur 1 22.102542
ARR 3 27 22.138662
EQ 3 27
ARR 3 28 22.145330
EQ 3 28
ARR 4 29 22.224039
EQ 4 29
F 0 22.741542
DQ 3 27
CrL M 3 27 0 22.741614
L MedianBlur 0 22.769790
F 1 22.920691
DQ 3 28
CrS MedianBlur GaussianBlur gaussianblur_0 22.920720
CrL M 3 28 1 22.920780
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-MedianBlur_pblock_gaussianblur_0_partial.bit 22.940343
L MedianBlur 1 23.228677
F 0 23.708703
DQ 4 24
CrS GaussianBlur MedianBlur medianblur_0 23.708748
CrL G 4 24 0 23.708825
S /home/xilinx/Bitstreams-axi_firewall/GaussianBlur-MedianBlur_pblock_medianblur_0_partial.bit 23.710373
L GaussianBlur 0 24.017432
F 1 24.166915
DQ 4 25
CrL M 4 25 1 24.166990
L MedianBlur 1 24.199779
F 0 24.835621
DQ 4 29
CrL G 4 29 0 24.835694
L GaussianBlur 0 24.849782
F 1 25.598083
F 0 25.667830
END 25.667858
