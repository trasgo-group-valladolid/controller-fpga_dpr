ARR 2 0 4.016157
CrS GaussianBlur GaussianBlur medianblur_0 4.016207
CrL M 2 0 0 4.016305
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-GaussianBlur_pblock_medianblur_0_partial.bit 4.016920
L MedianBlur 0 4.360995
F 0 4.824252
ARR 0 1 5.435672
CrL M 0 1 0 5.435731
L MedianBlur 0 5.479824
F 0 6.869386
ARR 4 2 7.459919
CrS GaussianBlur GaussianBlur medianblur_0 7.459950
CrL G 4 2 0 7.460034
S /home/xilinx/Bitstreams-axi_firewall/GaussianBlur-GaussianBlur_pblock_medianblur_0_partial.bit 7.490398
L GaussianBlur 0 7.812539
ARR 2 3 7.872868
CrL G 2 3 1 7.872936
L GaussianBlur 1 7.909827
F 0 8.628488
F 1 8.725762
ARR 3 4 8.911761
CrS GaussianBlur GaussianBlur medianblur_0 8.911785
CrL M 3 4 0 8.911842
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-GaussianBlur_pblock_medianblur_0_partial.bit 8.930385
L MedianBlur 0 9.276260
ARR 1 5 9.332084
CrS MedianBlur GaussianBlur gaussianblur_0 9.332109
CrL M 1 5 1 9.332186
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-MedianBlur_pblock_gaussianblur_0_partial.bit 9.332920
L MedianBlur 1 9.660511
ARR 1 6 9.674380
EQ 1 6
ARR 4 7 9.685687
EQ 4 7
F 0 9.741461
DQ 1 6
CrS GaussianBlur MedianBlur medianblur_0 9.741490
CrL G 1 6 0 9.741548
S /home/xilinx/Bitstreams-axi_firewall/GaussianBlur-MedianBlur_pblock_medianblur_0_partial.bit 9.750407
L GaussianBlur 0 10.071286
F 1 10.126387
DQ 4 7
CrS MedianBlur GaussianBlur gaussianblur_0 10.126419
CrL G 4 7 1 10.126494
S /home/xilinx/Bitstreams-axi_firewall/GaussianBlur-GaussianBlur_pblock_gaussianblur_0_partial.bit 10.126970
L GaussianBlur 1 10.446836
F 0 10.893384
F 1 11.267850
ARR 2 8 11.357748
CrL G 2 8 0 11.357806
ARR 0 9 11.377719
CrS MedianBlur GaussianBlur gaussianblur_0 11.377746
CrL M 0 9 1 11.377818
L GaussianBlur 0 11.379825
S /home/xilinx/Bitstreams-axi_firewall/GaussianBlur-MedianBlur_pblock_gaussianblur_0_partial.bit 11.400379
L MedianBlur 1 11.699745
F 0 12.197780
ARR 2 10 12.718034
CrS GaussianBlur MedianBlur medianblur_0 12.718067
CrL M 2 10 0 12.718133
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-MedianBlur_pblock_medianblur_0_partial.bit 12.740390
L MedianBlur 0 13.072277
F 1 13.093870
ARR 2 11 13.125392
CrL M 2 11 1 13.125458
L MedianBlur 1 13.159819
ARR 1 12 13.184784
EQ 1 12
F 0 13.537270
DQ 1 12
CrL M 1 12 0 13.537352
L MedianBlur 0 13.559826
ARR 3 13 14.193888
EQ 3 13
ARR 4 14 14.234159
EQ 4 14
ARR 4 15 14.248971
EQ 4 15
F 0 14.491696
DQ 3 13
CrL M 3 13 0 14.491754
L MedianBlur 0 14.519820
F 1 14.555348
DQ 4 14
CrL M 4 14 1 14.555404
L MedianBlur 1 14.569822
F 0 15.916073
DQ 4 15
CrL M 4 15 0 15.916163
L MedianBlur 0 15.919843
F 1 15.965642
ARR 3 16 16.276443
CrL M 3 16 1 16.276505
L MedianBlur 1 16.289834
ARR 0 17 16.362995
EQ 0 17
ARR 2 18 16.368747
EQ 2 18
F 1 16.761875
DQ 0 17
CrL M 0 17 1 16.761934
L MedianBlur 1 16.809819
F 0 16.852927
DQ 2 18
CrL M 2 18 0 16.852984
L MedianBlur 0 16.879825
F 1 18.225717
F 0 18.293745
ARR 1 19 19.438981
CrL M 1 19 0 19.439041
L MedianBlur 0 19.449831
ARR 4 20 19.463235
CrL M 4 20 1 19.463304
L MedianBlur 1 19.499825
ARR 0 21 20.483024
EQ 0 21
ARR 3 22 20.577484
EQ 3 22
F 0 20.845866
DQ 0 21
CrL M 0 21 0 20.845928
L MedianBlur 0 20.849834
F 1 20.895597
DQ 3 22
CrL M 3 22 1 20.895655
L MedianBlur 1 20.939818
ARR 0 23 21.636109
EQ 0 23
ARR 4 24 21.696730
EQ 4 24
ARR 4 25 21.757536
EQ 4 25
F 0 21.792151
DQ 0 23
CrL M 0 23 0 21.792209
L MedianBlur 0 21.809852
ARR 2 26 21.810452
EQ 2 26
ARR 3 27 21.874607
EQ 3 27
ARR 3 28 21.881249
EQ 3 28
F 1 21.883210
DQ 2 26
CrS MedianBlur GaussianBlur gaussianblur_0 21.883238
CrL G 2 26 1 21.883293
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-GaussianBlur_pblock_gaussianblur_0_partial.bit 21.900412
L GaussianBlur 1 22.231726
ARR 4 29 22.308547
EQ 4 29
F 0 22.741323
DQ 3 27
CrL M 3 27 0 22.741397
L MedianBlur 0 22.769823
F 1 23.049964
DQ 3 28
CrS MedianBlur GaussianBlur gaussianblur_0 23.049994
CrL M 3 28 1 23.050051
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-MedianBlur_pblock_gaussianblur_0_partial.bit 23.060418
L MedianBlur 1 23.375120
F 0 23.705141
DQ 4 24
CrS GaussianBlur MedianBlur medianblur_0 23.705186
CrL G 4 24 0 23.705263
S /home/xilinx/Bitstreams-axi_firewall/GaussianBlur-MedianBlur_pblock_medianblur_0_partial.bit 23.730392
L GaussianBlur 0 24.038601
F 1 24.308127
DQ 4 25
CrL M 4 25 1 24.308203
L MedianBlur 1 24.329859
F 0 24.856774
DQ 4 29
CrL G 4 29 0 24.856838
L GaussianBlur 0 24.899821
F 0 25.718132
F 1 25.727919
END 25.727941
