ARR 2 0 4.016137
CrS GaussianBlur GaussianBlur medianblur_0 4.016186
CrL M 2 0 0 4.016267
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-GaussianBlur_pblock_medianblur_0_partial.bit 4.030593
L MedianBlur 0 4.334214
F 0 4.797482
ARR 0 1 5.405661
CrL M 0 1 0 5.405732
L MedianBlur 0 5.449816
F 0 6.839361
ARR 4 2 7.429888
CrS GaussianBlur GaussianBlur medianblur_0 7.429923
CrL G 4 2 0 7.429998
S /home/xilinx/Bitstreams-axi_firewall/GaussianBlur-GaussianBlur_pblock_medianblur_0_partial.bit 7.450418
L GaussianBlur 0 7.753199
ARR 2 3 7.812834
CrL G 2 3 1 7.812898
L GaussianBlur 1 7.849807
F 0 8.569382
F 1 8.665976
ARR 3 4 8.851717
CrS GaussianBlur GaussianBlur medianblur_0 8.851740
CrL M 3 4 0 8.851798
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-GaussianBlur_pblock_medianblur_0_partial.bit 8.870385
L MedianBlur 0 9.202251
ARR 1 5 9.258076
CrS MedianBlur GaussianBlur gaussianblur_0 9.258103
CrL M 1 5 1 9.258170
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-MedianBlur_pblock_gaussianblur_0_partial.bit 9.260534
L MedianBlur 1 9.570000
ARR 1 6 9.574557
EQ 1 6
ARR 4 7 9.585861
EQ 4 7
F 0 9.667451
DQ 1 6
CrS GaussianBlur MedianBlur medianblur_0 9.667480
CrL G 1 6 0 9.667539
S /home/xilinx/Bitstreams-axi_firewall/GaussianBlur-MedianBlur_pblock_medianblur_0_partial.bit 9.680344
L GaussianBlur 0 9.984438
F 1 10.035673
DQ 4 7
CrS MedianBlur GaussianBlur gaussianblur_0 10.035707
CrL G 4 7 1 10.035775
S /home/xilinx/Bitstreams-axi_firewall/GaussianBlur-GaussianBlur_pblock_gaussianblur_0_partial.bit 10.050357
L GaussianBlur 1 10.337069
F 0 10.800992
F 1 11.152571
ARR 2 8 11.228747
CrL G 2 8 0 11.228807
L GaussianBlur 0 11.229798
ARR 0 9 11.248718
CrS MedianBlur GaussianBlur gaussianblur_0 11.248744
CrL M 0 9 1 11.248802
S /home/xilinx/Bitstreams-axi_firewall/GaussianBlur-MedianBlur_pblock_gaussianblur_0_partial.bit 11.250352
L MedianBlur 1 11.536362
F 0 12.047765
ARR 2 10 12.558078
CrS GaussianBlur MedianBlur medianblur_0 12.558111
CrL M 2 10 0 12.558182
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-MedianBlur_pblock_medianblur_0_partial.bit 12.558714
L MedianBlur 0 12.902423
F 1 12.930546
ARR 2 11 12.962921
CrL M 2 11 1 12.962984
L MedianBlur 1 12.999796
ARR 1 12 13.022308
EQ 1 12
F 0 13.372645
DQ 1 12
CrL M 1 12 0 13.372724
L MedianBlur 0 13.419792
ARR 3 13 14.031413
EQ 3 13
ARR 4 14 14.071698
EQ 4 14
ARR 4 15 14.086515
EQ 4 15
F 0 14.362123
DQ 3 13
CrL M 3 13 0 14.362184
F 1 14.415047
DQ 4 14
CrL M 4 14 1 14.415121
L MedianBlur 0 14.429799
L MedianBlur 1 14.459796
F 0 15.848124
DQ 4 15
CrL M 4 15 0 15.848209
L MedianBlur 0 15.869799
F 1 15.878491
ARR 3 16 16.113933
CrL M 3 16 1 16.113992
L MedianBlur 1 16.139793
ARR 0 17 16.200481
EQ 0 17
ARR 2 18 16.206231
EQ 2 18
F 1 16.609666
DQ 0 17
CrL M 0 17 1 16.609725
L MedianBlur 1 16.619831
F 0 16.800525
DQ 2 18
CrL M 2 18 0 16.800582
L MedianBlur 0 16.829793
F 1 18.026365
F 0 18.238202
ARR 1 19 19.276382
CrL M 1 19 0 19.276441
L MedianBlur 0 19.299795
ARR 4 20 19.300613
CrL M 4 20 1 19.300668
L MedianBlur 1 19.339792
ARR 0 21 20.319885
EQ 0 21
ARR 3 22 20.414353
EQ 3 22
F 0 20.716936
DQ 0 21
CrL M 0 21 0 20.716999
L MedianBlur 0 20.749803
F 1 20.754706
DQ 3 22
CrL M 3 22 1 20.754763
L MedianBlur 1 20.769798
ARR 0 23 21.472959
EQ 0 23
ARR 4 24 21.533593
EQ 4 24
ARR 4 25 21.594403
EQ 4 25
ARR 2 26 21.647106
EQ 2 26
F 0 21.689789
DQ 0 23
CrL M 0 23 0 21.689850
L MedianBlur 0 21.699805
ARR 3 27 21.711482
EQ 3 27
F 1 21.712492
DQ 2 26
CrS MedianBlur GaussianBlur gaussianblur_0 21.712519
CrL G 2 26 1 21.712574
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-GaussianBlur_pblock_gaussianblur_0_partial.bit 21.720443
L GaussianBlur 1 22.026264
ARR 3 28 22.035522
EQ 3 28
ARR 4 29 22.114242
EQ 4 29
F 0 22.631568
DQ 3 27
CrL M 3 27 0 22.631641
L MedianBlur 0 22.679792
F 1 22.844386
DQ 3 28
CrS MedianBlur GaussianBlur gaussianblur_0 22.844415
CrL M 3 28 1 22.844476
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-MedianBlur_pblock_gaussianblur_0_partial.bit 22.860375
L MedianBlur 1 23.180411
F 0 23.610694
DQ 4 24
CrS GaussianBlur MedianBlur medianblur_0 23.610740
CrL G 4 24 0 23.610816
S /home/xilinx/Bitstreams-axi_firewall/GaussianBlur-MedianBlur_pblock_medianblur_0_partial.bit 23.640397
L GaussianBlur 0 23.964325
F 1 24.111296
DQ 4 25
CrL M 4 25 1 24.111369
L MedianBlur 1 24.149836
F 0 24.782425
DQ 4 29
CrL G 4 29 0 24.782495
L GaussianBlur 0 24.839792
F 1 25.547867
F 0 25.657703
END 25.657726
