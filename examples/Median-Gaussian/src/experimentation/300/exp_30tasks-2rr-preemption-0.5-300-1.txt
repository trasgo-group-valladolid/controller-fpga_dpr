ARR 2 0 4.016129
CrS GaussianBlur GaussianBlur medianblur_0 4.016182
CrL M 2 0 0 4.016275
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-GaussianBlur_pblock_medianblur_0_partial.bit 4.016876
L MedianBlur 0 4.371063
F 0 4.538870
ARR 0 1 5.445947
CrL M 0 1 0 5.446016
L MedianBlur 0 5.449791
F 0 5.952534
ARR 4 2 7.469860
CrS GaussianBlur GaussianBlur medianblur_0 7.469895
CrL G 4 2 0 7.469962
S /home/xilinx/Bitstreams-axi_firewall/GaussianBlur-GaussianBlur_pblock_medianblur_0_partial.bit 7.490357
L GaussianBlur 0 7.810270
ARR 2 3 7.872817
CrL G 2 3 1 7.872887
L GaussianBlur 1 7.919773
F 0 8.104020
F 1 8.213502
ARR 3 4 8.912246
CrS GaussianBlur GaussianBlur medianblur_0 8.912287
CrL M 3 4 0 8.912373
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-GaussianBlur_pblock_medianblur_0_partial.bit 8.912914
L MedianBlur 0 9.233441
ARR 1 5 9.295618
CrS MedianBlur GaussianBlur gaussianblur_0 9.295649
CrL M 1 5 1 9.295716
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-MedianBlur_pblock_gaussianblur_0_partial.bit 9.320554
L MedianBlur 1 9.645098
F 0 9.645190
ARR 1 6 9.650365
CrS GaussianBlur MedianBlur medianblur_0 9.650399
CrL G 1 6 0 9.650457
S /home/xilinx/Bitstreams-axi_firewall/GaussianBlur-MedianBlur_pblock_medianblur_0_partial.bit 9.670335
L GaussianBlur 0 9.989363
F 1 9.989900
ARR 4 7 10.001170
CrS MedianBlur GaussianBlur gaussianblur_0 10.001206
CrL G 4 7 1 10.001272
S /home/xilinx/Bitstreams-axi_firewall/GaussianBlur-GaussianBlur_pblock_gaussianblur_0_partial.bit 10.010357
L GaussianBlur 1 10.332601
F 0 10.339936
F 1 10.626219
ARR 2 8 11.362002
CrL G 2 8 0 11.362074
ARR 0 9 11.381989
CrS MedianBlur GaussianBlur gaussianblur_0 11.382023
CrL M 0 9 1 11.382080
L GaussianBlur 0 11.389780
S /home/xilinx/Bitstreams-axi_firewall/GaussianBlur-MedianBlur_pblock_gaussianblur_0_partial.bit 11.410342
L MedianBlur 1 11.742870
F 0 11.749902
F 1 12.245712
ARR 2 10 12.768275
CrS GaussianBlur GaussianBlur medianblur_0 12.768321
CrL M 2 10 0 12.768397
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-MedianBlur_pblock_medianblur_0_partial.bit 12.790347
L MedianBlur 0 13.116496
ARR 2 11 13.172723
CrL M 2 11 1 13.172800
L MedianBlur 1 13.209774
ARR 1 12 13.232124
EQ 1 12
F 0 13.284585
DQ 1 12
CrL M 1 12 0 13.284660
L MedianBlur 0 13.309776
F 0 13.650365
F 1 13.718705
ARR 3 13 14.241413
CrL M 3 13 0 14.241478
L MedianBlur 0 14.269774
ARR 4 14 14.281737
CrL M 4 14 1 14.281798
L MedianBlur 1 14.289788
ARR 4 15 14.296607
R 0 14.296953
EQ 3 13
EV 13 15 0 14.297154
CrL M 4 15 0 14.297184
L MedianBlur 0 14.349778
F 0 14.690878
DQ 3 13
CrL M 3 13 0 14.690969
L MedianBlur 0 14.719783
F 1 14.797995
F 0 15.195944
ARR 3 16 16.325213
CrL M 3 16 0 16.325280
L MedianBlur 0 16.359805
ARR 0 17 16.411768
CrL M 0 17 1 16.411829
ARR 2 18 16.417578
R 1 16.417898
EQ 0 17
EV 17 18 1 16.418085
CrL M 2 18 1 16.418115
L MedianBlur 1 16.449846
F 0 16.528490
DQ 0 17
CrL M 0 17 0 16.528578
L MedianBlur 0 16.528625
F 1 16.960694
F 0 17.036889
ARR 1 19 19.489456
CrL M 1 19 0 19.489548
ARR 4 20 19.513728
CrL M 4 20 1 19.513798
L MedianBlur 0 19.519818
L MedianBlur 1 19.549774
F 0 20.031256
F 1 20.061649
ARR 0 21 20.533295
CrL M 0 21 0 20.533382
L MedianBlur 0 20.559811
ARR 3 22 20.627852
CrL M 3 22 1 20.627919
L MedianBlur 1 20.629787
F 0 20.898714
F 1 20.967604
ARR 0 23 21.686376
CrL M 0 23 0 21.686441
L MedianBlur 0 21.709778
ARR 4 24 21.747031
CrS MedianBlur GaussianBlur gaussianblur_0 21.747062
CrL G 4 24 1 21.747121
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-GaussianBlur_pblock_gaussianblur_0_partial.bit 21.760419
L GaussianBlur 1 22.068753
F 0 22.069901
ARR 4 25 22.130703
CrL M 4 25 0 22.130775
ARR 2 26 22.183484
EQ 2 26
L MedianBlur 0 22.189786
ARR 3 27 22.247660
EQ 3 27
ARR 3 28 22.254316
EQ 3 28
ARR 4 29 22.333017
EQ 4 29
F 1 22.362872
DQ 2 26
CrL G 2 26 1 22.362942
L GaussianBlur 1 22.379810
F 1 22.674314
DQ 3 27
CrS MedianBlur GaussianBlur gaussianblur_0 22.674361
CrL M 3 27 1 22.674433
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-MedianBlur_pblock_gaussianblur_0_partial.bit 22.700341
L MedianBlur 1 23.013777
F 0 23.013855
DQ 3 28
CrL M 3 28 0 23.013934
L MedianBlur 0 23.069778
F 1 23.350143
DQ 4 29
CrS MedianBlur GaussianBlur gaussianblur_0 23.350182
CrL G 4 29 1 23.350246
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-GaussianBlur_pblock_gaussianblur_0_partial.bit 23.370337
L GaussianBlur 1 23.689402
F 0 23.689893
F 1 23.982959
END 23.982983
