ARR 2 0 6.059906
CrS GaussianBlur GaussianBlur medianblur_0 6.059958
CrL M 2 0 0 6.060038
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-GaussianBlur_pblock_medianblur_0_partial.bit 6.080466
L MedianBlur 0 6.379103
F 0 6.546822
ARR 0 1 8.445932
CrL M 0 1 0 8.446012
L MedianBlur 0 8.469788
F 0 8.972620
ARR 4 2 11.506293
CrS GaussianBlur GaussianBlur medianblur_0 11.506332
CrL G 4 2 0 11.506405
S /home/xilinx/Bitstreams-axi_firewall/GaussianBlur-GaussianBlur_pblock_medianblur_0_partial.bit 11.530389
L GaussianBlur 0 11.843691
ARR 2 3 11.934601
CrL G 2 3 1 11.934670
L GaussianBlur 1 11.949798
F 0 12.141730
F 1 12.247810
ARR 3 4 13.957912
CrS GaussianBlur GaussianBlur medianblur_0 13.957952
CrL M 3 4 0 13.958027
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-GaussianBlur_pblock_medianblur_0_partial.bit 13.960351
L MedianBlur 0 14.273351
ARR 1 5 14.362612
CrS MedianBlur GaussianBlur gaussianblur_0 14.362639
CrL M 1 5 1 14.362718
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-MedianBlur_pblock_gaussianblur_0_partial.bit 14.363440
L MedianBlur 1 14.670410
F 0 14.679843
ARR 1 6 14.687665
CrS GaussianBlur MedianBlur medianblur_0 14.687696
CrL G 1 6 0 14.687760
S /home/xilinx/Bitstreams-axi_firewall/GaussianBlur-MedianBlur_pblock_medianblur_0_partial.bit 14.700388
L GaussianBlur 0 15.011157
F 1 15.019787
ARR 4 7 15.038293
CrS MedianBlur GaussianBlur gaussianblur_0 15.038326
CrL G 4 7 1 15.038389
S /home/xilinx/Bitstreams-axi_firewall/GaussianBlur-GaussianBlur_pblock_gaussianblur_0_partial.bit 15.040336
L GaussianBlur 1 15.308678
F 0 15.319785
F 1 15.602245
ARR 2 8 16.415110
CrL G 2 8 0 16.415173
L GaussianBlur 0 16.429787
ARR 0 9 16.446900
CrS MedianBlur GaussianBlur gaussianblur_0 16.446925
CrL M 0 9 1 16.446984
S /home/xilinx/Bitstreams-axi_firewall/GaussianBlur-MedianBlur_pblock_gaussianblur_0_partial.bit 16.460379
L MedianBlur 1 16.800461
F 0 16.800883
F 1 17.303406
ARR 2 10 17.889879
CrS GaussianBlur GaussianBlur medianblur_0 17.889911
CrL M 2 10 0 17.889987
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-MedianBlur_pblock_medianblur_0_partial.bit 17.890534
L MedianBlur 0 18.194368
ARR 2 11 18.279024
CrL M 2 11 1 18.279092
L MedianBlur 1 18.299789
F 0 18.362919
ARR 1 12 18.374021
CrL M 1 12 0 18.374080
L MedianBlur 0 18.429781
F 0 18.770937
F 1 18.808993
ARR 3 13 19.448073
CrL M 3 13 0 19.448144
L MedianBlur 0 19.479785
ARR 4 14 19.512444
CrL M 4 14 1 19.512502
L MedianBlur 1 19.529786
ARR 4 15 19.536060
EQ 4 15
F 0 19.984393
DQ 4 15
CrL M 4 15 0 19.984460
L MedianBlur 0 19.999786
F 1 20.034736
F 0 20.335187
ARR 3 16 22.599883
CrL M 3 16 0 22.599967
L MedianBlur 0 22.649797
F 0 22.817579
ARR 0 17 23.639162
CrL M 0 17 0 23.639237
ARR 2 18 23.648307
CrL M 2 18 1 23.648369
L MedianBlur 0 23.649866
L MedianBlur 1 23.669814
F 0 24.154585
F 1 24.174622
ARR 1 19 28.739882
CrL M 1 19 0 28.739963
L MedianBlur 0 28.769797
ARR 4 20 28.778514
CrL M 4 20 1 28.778585
L MedianBlur 1 28.829785
F 0 29.279750
F 1 29.340528
ARR 0 21 29.869300
CrL M 0 21 0 29.869359
L MedianBlur 0 29.889787
F 0 30.225070
ARR 3 22 30.921064
CrL M 3 22 0 30.921143
L MedianBlur 0 30.969786
F 0 31.305102
ARR 0 23 32.974818
CrL M 0 23 0 32.974904
L MedianBlur 0 33.019784
ARR 4 24 33.071755
CrS MedianBlur GaussianBlur gaussianblur_0 33.071782
CrL G 4 24 1 33.071845
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-GaussianBlur_pblock_gaussianblur_0_partial.bit 33.080461
L GaussianBlur 1 33.382613
F 0 33.389915
ARR 4 25 33.487088
CrL M 4 25 0 33.487155
L MedianBlur 0 33.529783
ARR 2 26 33.571384
EQ 2 26
F 1 33.676668
DQ 2 26
CrL G 2 26 1 33.676732
L GaussianBlur 1 33.699788
F 1 33.994206
F 0 34.034666
ARR 3 27 34.574949
CrL M 3 27 0 34.575007
ARR 3 28 34.585510
CrS MedianBlur GaussianBlur gaussianblur_0 34.585538
CrL M 3 28 1 34.585601
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-MedianBlur_pblock_gaussianblur_0_partial.bit 34.586130
L MedianBlur 0 34.629884
L MedianBlur 1 34.920943
F 0 34.966906
F 1 35.256645
ARR 4 29 35.947604
CrS GaussianBlur GaussianBlur medianblur_0 35.947635
CrL G 4 29 0 35.947743
S /home/xilinx/Bitstreams-axi_firewall/GaussianBlur-MedianBlur_pblock_medianblur_0_partial.bit 35.948262
L GaussianBlur 0 36.282757
F 0 36.576313
END 36.576338
