ARR 2 0 4.016136
CrS GaussianBlur GaussianBlur medianblur_0 4.016193
CrL M 2 0 0 4.016280
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-GaussianBlur_pblock_medianblur_0_partial.bit 4.050520
L MedianBlur 0 4.370885
F 0 4.538612
ARR 0 1 5.445967
CrL M 0 1 0 5.446040
L MedianBlur 0 5.449852
F 0 5.952631
ARR 4 2 7.469913
CrS GaussianBlur GaussianBlur medianblur_0 7.469946
CrL G 4 2 0 7.470009
S /home/xilinx/Bitstreams-axi_firewall/GaussianBlur-GaussianBlur_pblock_medianblur_0_partial.bit 7.490407
L GaussianBlur 0 7.812173
ARR 2 3 7.865228
CrL G 2 3 1 7.865309
L GaussianBlur 1 7.909845
F 0 8.105881
F 1 8.203516
ARR 3 4 8.904647
CrS GaussianBlur GaussianBlur medianblur_0 8.904679
CrL M 3 4 0 8.904738
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-GaussianBlur_pblock_medianblur_0_partial.bit 8.930395
L MedianBlur 0 9.228859
ARR 1 5 9.285699
CrS MedianBlur GaussianBlur gaussianblur_0 9.285730
CrL M 1 5 1 9.285797
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-MedianBlur_pblock_gaussianblur_0_partial.bit 9.310614
L MedianBlur 1 9.598243
F 0 9.609828
ARR 1 6 9.615038
CrS GaussianBlur MedianBlur medianblur_0 9.615071
CrL G 1 6 0 9.615131
S /home/xilinx/Bitstreams-axi_firewall/GaussianBlur-MedianBlur_pblock_medianblur_0_partial.bit 9.620441
L GaussianBlur 0 9.927544
F 1 9.929961
ARR 4 7 9.941260
CrS MedianBlur GaussianBlur gaussianblur_0 9.941292
CrL G 4 7 1 9.941354
S /home/xilinx/Bitstreams-axi_firewall/GaussianBlur-GaussianBlur_pblock_gaussianblur_0_partial.bit 9.970382
L GaussianBlur 1 10.271583
F 0 10.279957
F 1 10.565147
ARR 2 8 11.302040
CrL G 2 8 0 11.302109
L GaussianBlur 0 11.319838
ARR 0 9 11.322025
CrS MedianBlur GaussianBlur gaussianblur_0 11.322054
CrL M 0 9 1 11.322111
S /home/xilinx/Bitstreams-axi_firewall/GaussianBlur-MedianBlur_pblock_gaussianblur_0_partial.bit 11.330473
L MedianBlur 1 11.618698
F 0 11.619959
F 1 12.121635
ARR 2 10 12.638329
CrS GaussianBlur GaussianBlur medianblur_0 12.638367
CrL M 2 10 0 12.638441
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-MedianBlur_pblock_medianblur_0_partial.bit 12.638987
L MedianBlur 0 12.982836
ARR 2 11 13.035791
CrL M 2 11 1 13.035870
L MedianBlur 1 13.079835
ARR 1 12 13.095196
EQ 1 12
F 0 13.151102
DQ 1 12
CrL M 1 12 0 13.151170
L MedianBlur 0 13.169839
F 0 13.507945
F 1 13.585430
ARR 3 13 14.104473
CrL M 3 13 0 14.104555
L MedianBlur 0 14.119838
ARR 4 14 14.144819
CrL M 4 14 1 14.144883
ARR 4 15 14.159692
R 0 14.160088
EQ 3 13
EV 13 15 0 14.160290
CrL M 4 15 0 14.160320
L MedianBlur 1 14.179877
L MedianBlur 0 14.199852
F 0 14.536529
DQ 3 13
CrL M 3 13 0 14.536617
L MedianBlur 0 14.549844
F 1 14.686500
F 0 15.014365
ARR 3 16 16.188441
CrL M 3 16 0 16.188532
L MedianBlur 0 16.219835
ARR 0 17 16.275036
CrL M 0 17 1 16.275104
ARR 2 18 16.280850
R 1 16.281178
EQ 0 17
EV 17 18 1 16.281369
CrL M 2 18 1 16.281399
L MedianBlur 1 16.319880
F 0 16.388459
DQ 0 17
CrL M 0 17 0 16.388527
L MedianBlur 0 16.388568
F 1 16.830601
F 0 16.898996
ARR 1 19 19.352701
CrL M 1 19 0 19.352788
ARR 4 20 19.376967
CrL M 4 20 1 19.377034
L MedianBlur 0 19.379892
L MedianBlur 1 19.399838
F 0 19.884717
F 1 19.904828
ARR 0 21 20.396575
CrL M 0 21 0 20.396639
L MedianBlur 0 20.449836
ARR 3 22 20.491109
CrL M 3 22 1 20.491170
L MedianBlur 1 20.519834
F 0 20.788756
F 1 20.857617
ARR 0 23 21.549590
CrL M 0 23 0 21.549653
L MedianBlur 0 21.569836
ARR 4 24 21.610241
CrS MedianBlur GaussianBlur gaussianblur_0 21.610271
CrL G 4 24 1 21.610326
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-GaussianBlur_pblock_gaussianblur_0_partial.bit 21.640438
L GaussianBlur 1 21.957052
F 0 21.959864
ARR 4 25 22.021226
CrL M 4 25 0 22.021297
L MedianBlur 0 22.049838
ARR 2 26 22.074006
EQ 2 26
ARR 3 27 22.138177
EQ 3 27
ARR 3 28 22.144854
EQ 3 28
ARR 4 29 22.223564
EQ 4 29
F 1 22.251280
DQ 2 26
CrL G 2 26 1 22.251350
L GaussianBlur 1 22.309832
F 0 22.554746
DQ 3 27
CrL M 3 27 0 22.554812
L MedianBlur 0 22.569835
F 1 22.604196
DQ 3 28
CrS MedianBlur GaussianBlur gaussianblur_0 22.604229
CrL M 3 28 1 22.604288
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-MedianBlur_pblock_gaussianblur_0_partial.bit 22.630387
L MedianBlur 1 22.950326
F 0 22.959909
DQ 4 29
CrS GaussianBlur MedianBlur medianblur_0 22.959946
CrL G 4 29 0 22.960049
S /home/xilinx/Bitstreams-axi_firewall/GaussianBlur-MedianBlur_pblock_medianblur_0_partial.bit 22.970438
L GaussianBlur 0 23.300939
F 1 23.301018
F 0 23.594507
END 23.594534
