ARR 2 0 6.059964
CrS GaussianBlur GaussianBlur medianblur_0 6.060013
CrL M 2 0 0 6.060047
S /home/xilinx/Bitstreams/MedianBlur-GaussianBlur_pblock_medianblur_0_partial.bit 6.177294
L MedianBlur 0 6.527557
F 0 7.161016
ARR 0 1 8.606020
CrL M 0 1 0 8.606054
L MedianBlur 0 8.649853
F 0 10.550097
ARR 4 2 11.665776
CrS GaussianBlur GaussianBlur medianblur_0 11.665817
CrL G 4 2 0 11.665841
S /home/xilinx/Bitstreams/GaussianBlur-GaussianBlur_pblock_medianblur_0_partial.bit 11.757221
L GaussianBlur 0 12.151959
ARR 2 3 12.244789
CrL G 2 3 1 12.244817
L GaussianBlur 1 12.279838
F 0 13.301424
F 1 13.429291
ARR 3 4 14.268453
CrS GaussianBlur GaussianBlur medianblur_0 14.268497
CrL M 3 4 0 14.268522
S /home/xilinx/Bitstreams/MedianBlur-GaussianBlur_pblock_medianblur_0_partial.bit 14.363260
L MedianBlur 0 14.717588
ARR 1 5 14.809270
CrS MedianBlur GaussianBlur gaussianblur_0 14.809304
CrL M 1 5 1 14.809327
S /home/xilinx/Bitstreams/MedianBlur-MedianBlur_pblock_gaussianblur_0_partial.bit 14.899450
L MedianBlur 1 15.269481
ARR 1 6 15.276785
EQ 1 6
ARR 4 7 15.295184
R 0 15.295539
EQ 3 4
EV 4 7 0 15.295740
CrS MedianBlur MedianBlur medianblur_0 15.295761
CrL G 4 7 0 15.295782
S /home/xilinx/Bitstreams/GaussianBlur-MedianBlur_pblock_medianblur_0_partial.bit 15.413150
L GaussianBlur 0 15.790587
F 1 15.905755
DQ 1 6
CrS MedianBlur GaussianBlur gaussianblur_0 15.905797
CrL G 1 6 1 15.905819
S /home/xilinx/Bitstreams/GaussianBlur-GaussianBlur_pblock_gaussianblur_0_partial.bit 16.017163
L GaussianBlur 1 16.466717
F 0 16.937011
DQ 3 4
CrS GaussianBlur MedianBlur medianblur_0 16.937054
CrL M 3 4 0 16.937077
S /home/xilinx/Bitstreams/MedianBlur-GaussianBlur_pblock_medianblur_0_partial.bit 17.037269
L MedianBlur 0 17.366618
F 1 17.613615
ARR 2 8 17.908653
CrL G 2 8 1 17.908688
ARR 0 9 17.940866
EQ 0 9
L GaussianBlur 1 17.959845
F 0 18.002061
DQ 0 9
CrL M 0 9 0 18.002087
L MedianBlur 0 18.029848
ARR 2 10 19.029933
R 0 19.030283
EQ 0 9
EV 9 10 0 19.030479
CrL M 2 10 0 19.030494
L MedianBlur 0 19.059854
F 1 19.105832
DQ 0 9
CrS MedianBlur GaussianBlur gaussianblur_0 19.105865
CrL M 0 9 1 19.105886
S /home/xilinx/Bitstreams/MedianBlur-MedianBlur_pblock_gaussianblur_0_partial.bit 19.207045
L MedianBlur 1 19.555828
ARR 2 11 19.565829
R 1 19.566134
EQ 0 9
EV 9 11 1 19.566332
CrL M 2 11 1 19.566347
L MedianBlur 1 19.589855
ARR 1 12 19.661634
EQ 1 12
F 0 19.695893
DQ 0 9
CrL M 0 9 0 19.695924
L MedianBlur 0 19.719839
ARR 3 13 20.736589
R 0 20.736942
EQ 0 9
EV 9 13 0 20.737140
CrL M 3 13 0 20.737155
L MedianBlur 0 20.789843
ARR 4 14 20.801902
R 0 20.802186
EQ 3 13
EV 13 14 0 20.802374
CrL M 4 14 0 20.802389
ARR 4 15 20.826385
R 1 20.826667
EQ 2 11
EV 11 15 1 20.826854
CrL M 4 15 1 20.826868
L MedianBlur 0 20.829867
L MedianBlur 1 20.859854
F 1 22.149938
DQ 0 9
CrL M 0 9 1 22.149970
L MedianBlur 1 22.169841
F 0 22.759831
DQ 1 12
CrL M 1 12 0 22.759874
L MedianBlur 0 22.799841
F 1 23.072285
DQ 2 11
CrL M 2 11 1 23.072316
L MedianBlur 1 23.099841
F 1 23.786099
DQ 3 13
CrL M 3 13 1 23.786132
L MedianBlur 1 23.809880
ARR 3 16 23.891733
R 0 23.892076
EQ 1 12
EV 12 16 0 23.892271
CrL M 3 16 0 23.892285
L MedianBlur 0 23.916419
F 0 24.559711
DQ 1 12
CrL M 1 12 0 24.559752
L MedianBlur 0 24.579844
F 0 24.772231
ARR 0 17 24.932207
CrL M 0 17 0 24.932234
ARR 2 18 24.941687
R 0 24.942012
EQ 0 17
EV 17 18 0 24.942197
CrL M 2 18 0 24.942211
L MedianBlur 0 24.959845
F 1 25.728491
DQ 0 17
CrL M 0 17 1 25.728529
L MedianBlur 1 25.739856
F 0 26.890613
L MedianBlur 0 26.890685
F 1 27.674586
F 0 28.803891
ARR 1 19 30.035376
CrL M 1 19 0 30.035413
ARR 4 20 30.074382
CrL M 4 20 1 30.074412
L MedianBlur 0 30.079855
L MedianBlur 1 30.099841
ARR 0 21 31.165685
EQ 0 21
F 0 32.017095
DQ 0 21
CrL M 0 21 0 32.017132
F 1 32.038776
L MedianBlur 0 32.049855
ARR 3 22 32.218228
CrL M 3 22 1 32.218254
L MedianBlur 1 32.259872
F 0 33.335919
F 1 33.544416
ARR 0 23 34.272203
CrL M 0 23 0 34.272233
L MedianBlur 0 34.299852
ARR 4 24 34.369476
CrS MedianBlur GaussianBlur gaussianblur_0 34.369508
CrL G 4 24 1 34.369530
S /home/xilinx/Bitstreams/MedianBlur-GaussianBlur_pblock_gaussianblur_0_partial.bit 34.477169
L GaussianBlur 1 34.870652
ARR 4 25 34.967849
R 0 34.968184
EQ 0 23
EV 23 25 0 34.968382
CrL M 4 25 0 34.968397
L MedianBlur 0 35.009885
ARR 2 26 35.053105
EQ 2 26
F 1 36.016723
DQ 0 23
CrS MedianBlur GaussianBlur gaussianblur_0 36.016765
CrL M 0 23 1 36.016789
S /home/xilinx/Bitstreams/MedianBlur-MedianBlur_pblock_gaussianblur_0_partial.bit 36.143462
L MedianBlur 1 36.489139
ARR 3 27 36.528586
R 1 36.528906
EQ 0 23
EV 23 27 1 36.529105
CrL M 3 27 1 36.529120
ARR 3 28 36.540131
EQ 3 28
L MedianBlur 1 36.559879
F 0 36.925806
DQ 0 23
CrL M 0 23 0 36.925838
L MedianBlur 0 36.939860
ARR 4 29 37.567396
R 0 37.567786
EQ 0 23
EV 23 29 0 37.567979
CrS MedianBlur MedianBlur medianblur_0 37.568014
CrL G 4 29 0 37.568035
S /home/xilinx/Bitstreams/GaussianBlur-MedianBlur_pblock_medianblur_0_partial.bit 37.677229
F 1 38.051288
DQ 0 23
CrL M 0 23 1 38.051326
L GaussianBlur 0 38.059847
L MedianBlur 1 38.089845
F 1 38.704393
DQ 2 26
CrS MedianBlur GaussianBlur gaussianblur_0 38.704441
CrL G 2 26 1 38.704465
S /home/xilinx/Bitstreams/GaussianBlur-GaussianBlur_pblock_gaussianblur_0_partial.bit 38.807101
L GaussianBlur 1 39.149559
F 0 39.205376
DQ 3 28
CrS GaussianBlur MedianBlur medianblur_0 39.205413
CrL M 3 28 0 39.205436
S /home/xilinx/Bitstreams/MedianBlur-GaussianBlur_pblock_medianblur_0_partial.bit 39.306393
L MedianBlur 0 39.658330
F 1 40.295230
END 40.295261
