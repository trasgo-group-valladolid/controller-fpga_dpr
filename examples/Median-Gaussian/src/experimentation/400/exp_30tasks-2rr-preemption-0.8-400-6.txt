ARR 2 0 6.059947
CrS GaussianBlur GaussianBlur medianblur_0 6.060000
CrL M 2 0 0 6.060095
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-GaussianBlur_pblock_medianblur_0_partial.bit 6.061280
L MedianBlur 0 6.382114
F 0 6.679458
ARR 0 1 8.448076
CrL M 0 1 0 8.448162
L MedianBlur 0 8.469837
F 0 9.361506
ARR 4 2 11.508053
CrS GaussianBlur GaussianBlur medianblur_0 11.508101
CrL G 4 2 0 11.508179
S /home/xilinx/Bitstreams-axi_firewall/GaussianBlur-GaussianBlur_pblock_medianblur_0_partial.bit 11.510418
L GaussianBlur 0 11.831955
ARR 2 3 11.916756
CrL G 2 3 1 11.916833
L GaussianBlur 1 11.949864
F 0 12.359306
F 1 12.477198
ARR 3 4 13.939826
CrS GaussianBlur GaussianBlur medianblur_0 13.939868
CrL M 3 4 0 13.939940
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-GaussianBlur_pblock_medianblur_0_partial.bit 13.960419
L MedianBlur 0 14.265639
ARR 1 5 14.354895
CrS MedianBlur GaussianBlur gaussianblur_0 14.354929
CrL M 1 5 1 14.354997
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-MedianBlur_pblock_gaussianblur_0_partial.bit 14.360701
L MedianBlur 1 14.668084
F 0 14.679824
ARR 1 6 14.687638
CrS GaussianBlur MedianBlur medianblur_0 14.687673
CrL G 1 6 0 14.687736
S /home/xilinx/Bitstreams-axi_firewall/GaussianBlur-MedianBlur_pblock_medianblur_0_partial.bit 14.710380
L GaussianBlur 0 15.033179
F 1 15.039823
ARR 4 7 15.058327
CrS MedianBlur GaussianBlur gaussianblur_0 15.058363
CrL G 4 7 1 15.058425
S /home/xilinx/Bitstreams-axi_firewall/GaussianBlur-GaussianBlur_pblock_gaussianblur_0_partial.bit 15.080385
L GaussianBlur 1 15.399937
F 0 15.555944
F 1 15.921809
ARR 2 8 16.494571
CrL G 2 8 0 16.494641
L GaussianBlur 0 16.499878
ARR 0 9 16.526366
CrS MedianBlur GaussianBlur gaussianblur_0 16.526396
CrL M 0 9 1 16.526458
S /home/xilinx/Bitstreams-axi_firewall/GaussianBlur-MedianBlur_pblock_gaussianblur_0_partial.bit 16.527001
L MedianBlur 1 16.839293
F 0 17.023184
F 1 17.731947
ARR 2 10 17.927921
CrS GaussianBlur GaussianBlur medianblur_0 17.927958
CrL M 2 10 0 17.928026
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-MedianBlur_pblock_medianblur_0_partial.bit 17.950403
L MedianBlur 0 18.274545
ARR 2 11 18.359197
CrL M 2 11 1 18.359271
L MedianBlur 1 18.399831
ARR 1 12 18.454084
EQ 1 12
F 0 18.572496
DQ 1 12
CrL M 1 12 0 18.572562
L MedianBlur 0 18.589835
F 0 19.193936
F 1 19.300262
ARR 3 13 19.527877
CrL M 3 13 0 19.527963
L MedianBlur 0 19.529848
ARR 4 14 19.592261
CrL M 4 14 1 19.592327
ARR 4 15 19.615886
R 0 19.616247
EQ 3 13
EV 13 15 0 19.616448
CrL M 4 15 0 19.616478
L MedianBlur 1 19.619880
L MedianBlur 0 19.649834
F 0 20.256099
DQ 3 13
CrL M 3 13 0 20.256191
L MedianBlur 0 20.279837
F 1 20.525710
F 0 21.089173
ARR 3 16 22.679920
CrL M 3 16 0 22.680009
L MedianBlur 0 22.729832
F 0 23.027104
ARR 0 17 23.719024
CrL M 0 17 0 23.719111
ARR 2 18 23.728182
CrL M 2 18 1 23.728251
L MedianBlur 0 23.729924
L MedianBlur 1 23.779855
F 0 24.624946
F 1 24.674697
ARR 1 19 28.819923
CrL M 1 19 0 28.820013
L MedianBlur 0 28.839836
ARR 4 20 28.858558
CrL M 4 20 1 28.858623
L MedianBlur 1 28.889830
F 0 29.734818
F 1 29.784866
ARR 0 21 29.948928
CrL M 0 21 0 29.948991
L MedianBlur 0 29.959852
F 0 30.554241
ARR 3 22 31.000472
CrL M 3 22 0 31.000546
L MedianBlur 0 31.049830
F 0 31.644409
ARR 0 23 33.053934
CrL M 0 23 0 33.054024
L MedianBlur 0 33.089863
ARR 4 24 33.150871
CrS MedianBlur GaussianBlur gaussianblur_0 33.150902
CrL G 4 24 1 33.150964
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-GaussianBlur_pblock_gaussianblur_0_partial.bit 33.160499
L GaussianBlur 1 33.481025
ARR 4 25 33.578123
R 0 33.578399
EQ 0 23
EV 23 25 0 33.578589
CrL M 4 25 0 33.578621
L MedianBlur 0 33.609835
ARR 2 26 33.662846
EQ 2 26
F 1 34.004460
DQ 0 23
CrS MedianBlur GaussianBlur gaussianblur_0 34.004500
CrL M 0 23 1 34.004570
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-MedianBlur_pblock_gaussianblur_0_partial.bit 34.005086
L MedianBlur 1 34.309747
F 0 34.505803
DQ 2 26
CrS GaussianBlur MedianBlur medianblur_0 34.505842
CrL G 2 26 0 34.505955
S /home/xilinx/Bitstreams-axi_firewall/GaussianBlur-MedianBlur_pblock_medianblur_0_partial.bit 34.520389
L GaussianBlur 0 34.860424
F 1 34.906857
ARR 3 27 35.335530
CrL M 3 27 1 35.335616
ARR 3 28 35.346167
R 0 35.346487
EQ 2 26
EV 26 28 0 35.346675
CrS MedianBlur MedianBlur medianblur_0 35.346695
CrL M 3 28 0 35.346732
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-MedianBlur_pblock_medianblur_0_partial.bit 35.347218
L MedianBlur 1 35.359880
L MedianBlur 0 35.657790
F 1 35.961166
DQ 2 26
CrS MedianBlur GaussianBlur gaussianblur_0 35.961208
CrL G 2 26 1 35.961280
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-GaussianBlur_pblock_gaussianblur_0_partial.bit 35.990386
L GaussianBlur 1 36.301305
F 0 36.301395
F 1 36.823095
ARR 4 29 37.024172
CrS GaussianBlur GaussianBlur medianblur_0 37.024208
CrL G 4 29 0 37.024274
S /home/xilinx/Bitstreams-axi_firewall/GaussianBlur-GaussianBlur_pblock_medianblur_0_partial.bit 37.050385
L GaussianBlur 0 37.361795
F 0 37.883592
END 37.883622
