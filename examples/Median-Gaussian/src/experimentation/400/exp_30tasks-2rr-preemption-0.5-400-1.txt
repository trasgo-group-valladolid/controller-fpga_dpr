ARR 2 0 4.016124
CrS GaussianBlur GaussianBlur medianblur_0 4.016179
CrL M 2 0 0 4.016274
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-GaussianBlur_pblock_medianblur_0_partial.bit 4.016894
L MedianBlur 0 4.358614
F 0 4.655982
ARR 0 1 5.424599
CrL M 0 1 0 5.424666
L MedianBlur 0 5.478466
F 0 6.370155
ARR 4 2 7.448549
CrS GaussianBlur GaussianBlur medianblur_0 7.448581
CrL G 4 2 0 7.448654
S /home/xilinx/Bitstreams-axi_firewall/GaussianBlur-GaussianBlur_pblock_medianblur_0_partial.bit 7.449186
L GaussianBlur 0 7.736845
ARR 2 3 7.789888
CrL G 2 3 1 7.789961
L GaussianBlur 1 7.838467
F 0 8.258946
F 1 8.360540
ARR 3 4 8.829098
CrS GaussianBlur GaussianBlur medianblur_0 8.829140
CrL M 3 4 0 8.829215
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-GaussianBlur_pblock_medianblur_0_partial.bit 8.849061
L MedianBlur 0 9.156299
ARR 1 5 9.214297
CrS MedianBlur GaussianBlur gaussianblur_0 9.214329
CrL M 1 5 1 9.214396
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-MedianBlur_pblock_gaussianblur_0_partial.bit 9.239247
L MedianBlur 1 9.527400
F 0 9.528591
ARR 1 6 9.533233
CrS GaussianBlur MedianBlur medianblur_0 9.533265
CrL G 1 6 0 9.533328
S /home/xilinx/Bitstreams-axi_firewall/GaussianBlur-MedianBlur_pblock_medianblur_0_partial.bit 9.549021
L GaussianBlur 0 9.873764
F 1 9.873856
ARR 4 7 9.885150
CrS MedianBlur GaussianBlur gaussianblur_0 9.885182
CrL G 4 7 1 9.885245
S /home/xilinx/Bitstreams-axi_firewall/GaussianBlur-GaussianBlur_pblock_gaussianblur_0_partial.bit 9.899061
L GaussianBlur 1 10.219140
F 0 10.399677
F 1 10.744179
ARR 2 8 11.250425
CrL G 2 8 0 11.250491
ARR 0 9 11.270402
CrS MedianBlur GaussianBlur gaussianblur_0 11.270435
CrL M 0 9 1 11.270493
S /home/xilinx/Bitstreams-axi_firewall/GaussianBlur-MedianBlur_pblock_gaussianblur_0_partial.bit 11.279063
L GaussianBlur 0 11.288609
L MedianBlur 1 11.592998
F 0 11.811831
F 1 12.485802
ARR 2 10 12.611076
CrS GaussianBlur GaussianBlur medianblur_0 12.611114
CrL M 2 10 0 12.611189
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-MedianBlur_pblock_medianblur_0_partial.bit 12.611730
L MedianBlur 0 12.928104
ARR 2 11 12.981464
CrL M 2 11 1 12.981540
L MedianBlur 1 13.008471
ARR 1 12 13.040865
EQ 1 12
F 0 13.226230
DQ 1 12
CrL M 1 12 0 13.226304
L MedianBlur 0 13.238471
F 0 13.842868
F 1 13.911120
ARR 3 13 14.049804
CrL M 3 13 0 14.049882
L MedianBlur 0 14.088482
ARR 4 14 14.090148
CrL M 4 14 1 14.090210
ARR 4 15 14.105017
R 0 14.105376
EQ 3 13
EV 13 15 0 14.105575
CrL M 4 15 0 14.105605
L MedianBlur 0 14.128476
L MedianBlur 1 14.148472
F 0 14.725237
DQ 3 13
CrL M 3 13 0 14.725304
L MedianBlur 0 14.728483
F 1 15.043991
F 0 15.605309
ARR 3 16 16.133040
CrL M 3 16 0 16.133128
L MedianBlur 0 16.148473
ARR 0 17 16.219624
CrL M 0 17 1 16.219690
ARR 2 18 16.225444
R 1 16.225766
EQ 0 17
EV 17 18 1 16.225956
CrL M 2 18 1 16.225986
L MedianBlur 1 16.248520
F 0 16.449092
DQ 0 17
CrL M 0 17 0 16.449161
L MedianBlur 0 16.458508
F 1 17.151806
F 0 17.357647
ARR 1 19 19.296780
CrL M 1 19 0 19.296870
L MedianBlur 0 19.308473
ARR 4 20 19.321050
CrL M 4 20 1 19.321115
L MedianBlur 1 19.368466
F 0 20.214733
F 1 20.275311
ARR 0 21 20.340228
CrL M 0 21 0 20.340295
L MedianBlur 0 20.388477
ARR 3 22 20.434762
CrL M 3 22 1 20.434823
L MedianBlur 1 20.458467
F 0 20.991674
F 1 21.059940
ARR 0 23 21.492991
CrL M 0 23 0 21.493056
L MedianBlur 0 21.508470
ARR 4 24 21.553642
CrS MedianBlur GaussianBlur gaussianblur_0 21.553673
CrL G 4 24 1 21.553729
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-GaussianBlur_pblock_gaussianblur_0_partial.bit 21.579056
L GaussianBlur 1 21.877915
ARR 4 25 21.939208
R 0 21.939543
EQ 0 23
EV 23 25 0 21.939739
CrL M 4 25 0 21.939770
L MedianBlur 0 21.968479
ARR 2 26 21.992477
EQ 2 26
ARR 3 27 22.056644
EQ 3 27
ARR 3 28 22.063294
EQ 3 28
ARR 4 29 22.141993
EQ 4 29
F 1 22.401391
DQ 0 23
CrS MedianBlur GaussianBlur gaussianblur_0 22.401432
CrL M 0 23 1 22.401502
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-MedianBlur_pblock_gaussianblur_0_partial.bit 22.402018
L MedianBlur 1 22.698080
F 0 22.864577
DQ 2 26
CrS GaussianBlur MedianBlur medianblur_0 22.864618
CrL G 2 26 0 22.864690
S /home/xilinx/Bitstreams-axi_firewall/GaussianBlur-MedianBlur_pblock_medianblur_0_partial.bit 22.868997
L GaussianBlur 0 23.193176
F 1 23.295247
DQ 3 27
CrL M 3 27 1 23.295326
L MedianBlur 1 23.338468
F 0 23.716544
DQ 3 28
CrS GaussianBlur MedianBlur medianblur_0 23.716584
CrL M 3 28 0 23.716655
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-MedianBlur_pblock_medianblur_0_partial.bit 23.717165
L MedianBlur 0 24.038048
F 1 24.048461
DQ 4 29
CrS MedianBlur GaussianBlur gaussianblur_0 24.048499
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-GaussianBlur_pblock_gaussianblur_0_partial.bit 24.049151
CrL G 4 29 1 24.049174
L GaussianBlur 1 24.338301
F 0 24.635622
F 1 24.861078
END 24.861102
