ARR 2 0 6.059967
CrS GaussianBlur GaussianBlur medianblur_0 6.060022
CrL M 2 0 0 6.060106
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-GaussianBlur_pblock_medianblur_0_partial.bit 6.080516
L MedianBlur 0 6.379522
F 0 6.676882
ARR 0 1 8.445828
CrL M 0 1 0 8.445914
L MedianBlur 0 8.489848
F 0 9.381512
ARR 4 2 11.505809
CrS GaussianBlur GaussianBlur medianblur_0 11.505856
CrL G 4 2 0 11.505930
S /home/xilinx/Bitstreams-axi_firewall/GaussianBlur-GaussianBlur_pblock_medianblur_0_partial.bit 11.520449
L GaussianBlur 0 11.843121
ARR 2 3 11.927919
CrL G 2 3 1 11.928000
L GaussianBlur 1 11.939854
F 0 12.365250
F 1 12.461965
ARR 3 4 13.949937
CrS GaussianBlur GaussianBlur medianblur_0 13.949979
CrL M 3 4 0 13.950058
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-GaussianBlur_pblock_medianblur_0_partial.bit 13.950578
L MedianBlur 0 14.282498
ARR 1 5 14.379148
CrS MedianBlur GaussianBlur gaussianblur_0 14.379188
CrL M 1 5 1 14.379258
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-MedianBlur_pblock_gaussianblur_0_partial.bit 14.380617
L MedianBlur 1 14.694147
F 0 14.699950
ARR 1 6 14.707234
CrS GaussianBlur MedianBlur medianblur_0 14.707269
CrL G 1 6 0 14.707339
S /home/xilinx/Bitstreams-axi_firewall/GaussianBlur-MedianBlur_pblock_medianblur_0_partial.bit 14.707834
L GaussianBlur 0 15.052820
F 1 15.059839
ARR 4 7 15.078336
CrS MedianBlur GaussianBlur gaussianblur_0 15.078372
CrL G 4 7 1 15.078434
S /home/xilinx/Bitstreams-axi_firewall/GaussianBlur-GaussianBlur_pblock_gaussianblur_0_partial.bit 15.100402
L GaussianBlur 1 15.409707
F 0 15.575990
F 1 15.931990
ARR 2 8 16.504335
CrL G 2 8 0 16.504423
L GaussianBlur 0 16.529864
ARR 0 9 16.536162
CrS MedianBlur GaussianBlur gaussianblur_0 16.536193
CrL M 0 9 1 16.536254
S /home/xilinx/Bitstreams-axi_firewall/GaussianBlur-MedianBlur_pblock_gaussianblur_0_partial.bit 16.560427
L MedianBlur 1 16.900802
F 0 17.053006
F 1 17.793221
ARR 2 10 17.989433
CrS GaussianBlur GaussianBlur medianblur_0 17.989470
CrL M 2 10 0 17.989534
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-MedianBlur_pblock_medianblur_0_partial.bit 17.990403
L MedianBlur 0 18.288754
ARR 2 11 18.374503
CrL M 2 11 1 18.374576
L MedianBlur 1 18.389853
ARR 1 12 18.469395
EQ 1 12
F 0 18.586829
DQ 1 12
CrL M 1 12 0 18.586897
L MedianBlur 0 18.639853
F 0 19.242450
F 1 19.288165
ARR 3 13 19.543206
CrL M 3 13 0 19.543292
L MedianBlur 0 19.569850
ARR 4 14 19.607590
CrL M 4 14 1 19.607654
L MedianBlur 1 19.629858
ARR 4 15 19.631215
R 0 19.631576
EQ 3 13
EV 13 15 0 19.631776
CrL M 4 15 0 19.631806
L MedianBlur 0 19.649857
F 0 20.247005
DQ 3 13
CrL M 3 13 0 20.247104
L MedianBlur 0 20.249877
F 1 20.528901
F 0 21.085009
ARR 3 16 22.696028
CrL M 3 16 0 22.696115
L MedianBlur 0 22.729857
F 0 23.027143
ARR 0 17 23.735166
CrL M 0 17 0 23.735256
ARR 2 18 23.744329
CrL M 2 18 1 23.744396
L MedianBlur 0 23.759852
L MedianBlur 1 23.779846
F 0 24.655041
F 1 24.674768
ARR 1 19 28.838003
CrL M 1 19 0 28.838094
L MedianBlur 0 28.859851
ARR 4 20 28.876640
CrL M 4 20 1 28.876705
L MedianBlur 1 28.909847
F 0 29.754805
F 1 29.804838
ARR 0 21 29.967009
CrL M 0 21 0 29.967075
L MedianBlur 0 29.979851
F 0 30.574289
ARR 3 22 31.018559
CrL M 3 22 0 31.018626
L MedianBlur 0 31.079873
F 0 31.674470
ARR 0 23 33.071998
CrL M 0 23 0 33.072079
L MedianBlur 0 33.109850
ARR 4 24 33.168924
CrS MedianBlur GaussianBlur gaussianblur_0 33.168954
CrL G 4 24 1 33.169015
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-GaussianBlur_pblock_gaussianblur_0_partial.bit 33.180488
L GaussianBlur 1 33.486598
ARR 4 25 33.586951
R 0 33.587250
EQ 0 23
EV 23 25 0 33.587441
CrL M 4 25 0 33.587472
L MedianBlur 0 33.619852
ARR 2 26 33.671695
EQ 2 26
F 1 34.010007
DQ 0 23
CrS MedianBlur GaussianBlur gaussianblur_0 34.010044
CrL M 0 23 1 34.010106
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-MedianBlur_pblock_gaussianblur_0_partial.bit 34.030414
L MedianBlur 1 34.343985
F 0 34.515815
DQ 2 26
CrS GaussianBlur MedianBlur medianblur_0 34.515853
CrL G 2 26 0 34.515956
S /home/xilinx/Bitstreams-axi_firewall/GaussianBlur-MedianBlur_pblock_medianblur_0_partial.bit 34.520459
L GaussianBlur 0 34.834033
F 1 34.941904
ARR 3 27 35.338801
CrL M 3 27 1 35.338889
L MedianBlur 1 35.339866
ARR 3 28 35.349442
R 0 35.349759
EQ 2 26
EV 26 28 0 35.349992
CrS MedianBlur MedianBlur medianblur_0 35.350013
CrL M 3 28 0 35.350049
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-MedianBlur_pblock_medianblur_0_partial.bit 35.360413
L MedianBlur 0 35.682561
F 1 35.939266
DQ 2 26
CrS MedianBlur GaussianBlur gaussianblur_0 35.939306
CrL G 2 26 1 35.939375
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-GaussianBlur_pblock_gaussianblur_0_partial.bit 35.950465
L GaussianBlur 1 36.252707
F 0 36.283223
F 1 36.774590
ARR 4 29 37.036751
CrS GaussianBlur GaussianBlur medianblur_0 37.036785
CrL G 4 29 0 37.036849
S /home/xilinx/Bitstreams-axi_firewall/GaussianBlur-GaussianBlur_pblock_medianblur_0_partial.bit 37.060438
L GaussianBlur 0 37.384602
F 0 37.906379
END 37.906408
