ARR 2 0 4.016144
CrS GaussianBlur GaussianBlur medianblur_0 4.016199
CrL M 2 0 0 4.016286
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-GaussianBlur_pblock_medianblur_0_partial.bit 4.020433
L MedianBlur 0 4.309515
F 0 4.606873
ARR 0 1 5.375508
CrL M 0 1 0 5.375589
L MedianBlur 0 5.399783
F 0 6.291414
ARR 4 2 7.399866
CrS GaussianBlur GaussianBlur medianblur_0 7.399904
CrL G 4 2 0 7.399974
S /home/xilinx/Bitstreams-axi_firewall/GaussianBlur-GaussianBlur_pblock_medianblur_0_partial.bit 7.410485
L GaussianBlur 0 7.706777
ARR 2 3 7.759835
CrL G 2 3 1 7.759906
L GaussianBlur 1 7.809771
F 0 8.229019
F 1 8.332001
ARR 3 4 8.799016
CrS GaussianBlur GaussianBlur medianblur_0 8.799055
CrL M 3 4 0 8.799126
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-GaussianBlur_pblock_medianblur_0_partial.bit 8.810464
L MedianBlur 0 9.127027
ARR 1 5 9.182855
CrS MedianBlur GaussianBlur gaussianblur_0 9.182886
CrL M 1 5 1 9.182951
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-MedianBlur_pblock_gaussianblur_0_partial.bit 9.210615
L MedianBlur 1 9.539626
F 0 9.539905
ARR 1 6 9.544545
CrS GaussianBlur MedianBlur medianblur_0 9.544577
CrL G 1 6 0 9.544644
S /home/xilinx/Bitstreams-axi_firewall/GaussianBlur-MedianBlur_pblock_medianblur_0_partial.bit 9.545141
L GaussianBlur 0 9.906175
F 1 9.919770
ARR 4 7 9.931576
CrS MedianBlur GaussianBlur gaussianblur_0 9.931610
CrL G 4 7 1 9.931671
S /home/xilinx/Bitstreams-axi_firewall/GaussianBlur-GaussianBlur_pblock_gaussianblur_0_partial.bit 9.960311
L GaussianBlur 1 10.305167
F 0 10.431273
F 1 10.829451
ARR 2 8 11.331758
CrL G 2 8 0 11.331839
L GaussianBlur 0 11.349774
ARR 0 9 11.351760
CrS MedianBlur GaussianBlur gaussianblur_0 11.351790
CrL M 0 9 1 11.351853
S /home/xilinx/Bitstreams-axi_firewall/GaussianBlur-MedianBlur_pblock_gaussianblur_0_partial.bit 11.360401
L MedianBlur 1 11.688864
F 0 11.873004
F 1 12.581431
ARR 2 10 12.707859
CrS GaussianBlur GaussianBlur medianblur_0 12.707901
CrL M 2 10 0 12.707972
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-MedianBlur_pblock_medianblur_0_partial.bit 12.720461
L MedianBlur 0 13.080649
ARR 2 11 13.142727
CrL M 2 11 1 13.142801
L MedianBlur 1 13.169769
ARR 1 12 13.202127
EQ 1 12
F 0 13.380110
DQ 1 12
CrL M 1 12 0 13.380184
L MedianBlur 0 13.399781
F 0 14.002849
F 1 14.071269
ARR 3 13 14.211012
CrL M 3 13 0 14.211078
L MedianBlur 0 14.239770
ARR 4 14 14.251331
CrL M 4 14 1 14.251392
ARR 4 15 14.266197
R 0 14.266548
EQ 3 13
EV 13 15 0 14.266748
CrL M 4 15 0 14.266778
L MedianBlur 1 14.269786
L MedianBlur 0 14.279777
F 0 14.878495
DQ 3 13
CrL M 3 13 0 14.878588
L MedianBlur 0 14.879779
F 1 15.170674
F 0 15.749383
ARR 3 16 16.294247
CrL M 3 16 0 16.294330
L MedianBlur 0 16.319775
ARR 0 17 16.380832
CrL M 0 17 1 16.380900
ARR 2 18 16.386653
R 1 16.386986
EQ 0 17
EV 17 18 1 16.387178
CrL M 2 18 1 16.387208
L MedianBlur 1 16.429831
F 0 16.617932
DQ 0 17
CrL M 0 17 0 16.618015
L MedianBlur 0 16.639770
F 1 17.324964
F 0 17.534185
ARR 1 19 19.458045
CrL M 1 19 0 19.458133
ARR 4 20 19.482310
CrL M 4 20 1 19.482379
L MedianBlur 1 19.509771
L MedianBlur 0 19.519772
F 1 20.413266
F 0 20.420919
ARR 0 21 20.501467
CrL M 0 21 0 20.501529
L MedianBlur 0 20.559765
ARR 3 22 20.595986
CrL M 3 22 1 20.596047
L MedianBlur 1 20.629769
F 0 21.162999
F 1 21.231272
ARR 0 23 21.654243
CrL M 0 23 0 21.654310
ARR 4 24 21.714899
CrS MedianBlur GaussianBlur gaussianblur_0 21.714932
CrL G 4 24 1 21.714996
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-GaussianBlur_pblock_gaussianblur_0_partial.bit 21.715551
L MedianBlur 0 21.719889
L GaussianBlur 1 22.059381
ARR 4 25 22.120511
R 0 22.120799
EQ 0 23
EV 23 25 0 22.120993
CrL M 4 25 0 22.121024
L MedianBlur 0 22.139776
ARR 2 26 22.173727
EQ 2 26
ARR 3 27 22.237899
EQ 3 27
ARR 3 28 22.244559
EQ 3 28
ARR 4 29 22.323264
EQ 4 29
F 1 22.582842
DQ 0 23
CrS MedianBlur GaussianBlur gaussianblur_0 22.582878
CrL M 0 23 1 22.582940
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-MedianBlur_pblock_gaussianblur_0_partial.bit 22.590349
L MedianBlur 1 22.899350
F 0 23.035790
DQ 2 26
CrS GaussianBlur MedianBlur medianblur_0 23.035829
CrL G 2 26 0 23.035905
S /home/xilinx/Bitstreams-axi_firewall/GaussianBlur-MedianBlur_pblock_medianblur_0_partial.bit 23.036404
L GaussianBlur 0 23.401352
F 1 23.496529
DQ 3 27
CrL M 3 27 1 23.496605
L MedianBlur 1 23.529768
F 0 23.924817
DQ 3 28
CrS GaussianBlur MedianBlur medianblur_0 23.924858
CrL M 3 28 0 23.924931
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-MedianBlur_pblock_medianblur_0_partial.bit 23.925436
L MedianBlur 0 24.293708
F 1 24.299850
DQ 4 29
CrS MedianBlur GaussianBlur gaussianblur_0 24.299885
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-GaussianBlur_pblock_gaussianblur_0_partial.bit 24.300540
CrL G 4 29 1 24.300545
L GaussianBlur 1 24.629342
F 0 24.891144
F 1 25.152018
END 25.152044
