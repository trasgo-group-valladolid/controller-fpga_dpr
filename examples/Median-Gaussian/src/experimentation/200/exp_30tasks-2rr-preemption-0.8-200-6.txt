ARR 2 0 6.059925
CrS GaussianBlur GaussianBlur medianblur_0 6.059981
CrL M 2 0 0 6.060063
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-GaussianBlur_pblock_medianblur_0_partial.bit 6.080525
L MedianBlur 0 6.388738
F 0 6.463551
ARR 0 1 8.454921
CrL M 0 1 0 8.455014
L MedianBlur 0 8.479809
F 0 8.704264
ARR 4 2 11.515592
CrS GaussianBlur GaussianBlur medianblur_0 11.515639
CrL G 4 2 0 11.515717
S /home/xilinx/Bitstreams-axi_firewall/GaussianBlur-GaussianBlur_pblock_medianblur_0_partial.bit 11.520470
L GaussianBlur 0 11.851989
ARR 2 3 11.944610
CrL G 2 3 1 11.944686
L GaussianBlur 1 11.949820
F 0 11.982591
F 1 12.080407
ARR 3 4 13.968085
CrS GaussianBlur GaussianBlur medianblur_0 13.968130
CrL M 3 4 0 13.968203
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-GaussianBlur_pblock_medianblur_0_partial.bit 13.970363
L MedianBlur 0 14.275328
F 0 14.350175
ARR 1 5 14.364722
CrL M 1 5 0 14.364792
ARR 1 6 14.372099
CrL G 1 6 1 14.372163
L GaussianBlur 1 14.379814
L MedianBlur 0 14.389822
ARR 4 7 14.390089
R 0 14.390423
EQ 1 5
EV 5 7 0 14.390618
CrS MedianBlur MedianBlur medianblur_0 14.390637
CrL G 4 7 0 14.390673
S /home/xilinx/Bitstreams-axi_firewall/GaussianBlur-GaussianBlur_pblock_medianblur_0_partial.bit 14.391156
L GaussianBlur 0 14.703159
F 1 14.709911
DQ 1 5
CrS MedianBlur GaussianBlur gaussianblur_0 14.709948
CrL M 1 5 1 14.710018
S /home/xilinx/Bitstreams-axi_firewall/GaussianBlur-MedianBlur_pblock_gaussianblur_0_partial.bit 14.730607
L MedianBlur 1 15.039436
F 0 15.049813
F 1 15.114276
ARR 2 8 16.145311
CrL G 2 8 0 16.145383
L GaussianBlur 0 16.169805
ARR 0 9 16.177146
CrL M 0 9 1 16.177208
L MedianBlur 1 16.199805
F 0 16.300649
F 1 16.424311
ARR 2 10 17.266537
CrS GaussianBlur GaussianBlur medianblur_0 17.266580
CrL M 2 10 0 17.266653
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-MedianBlur_pblock_medianblur_0_partial.bit 17.290394
L MedianBlur 0 17.624855
F 0 17.699705
ARR 2 11 17.714623
CrL M 2 11 0 17.714694
L MedianBlur 0 17.739803
ARR 1 12 17.809508
CrL M 1 12 1 17.809568
L MedianBlur 1 17.829861
F 0 17.966393
F 1 17.981608
ARR 3 13 18.883795
CrL M 3 13 0 18.883881
L MedianBlur 0 18.929803
ARR 4 14 18.948183
CrL M 4 14 1 18.948248
ARR 4 15 18.971805
R 0 18.972147
EQ 3 13
EV 13 15 0 18.972333
CrL M 4 15 0 18.972362
L MedianBlur 1 18.979817
L MedianBlur 0 19.009810
F 0 19.161398
DQ 3 13
CrL M 3 13 0 19.161465
L MedianBlur 0 19.169814
F 1 19.206257
F 0 19.351942
ARR 3 16 22.037535
CrL M 3 16 0 22.037618
L MedianBlur 0 22.059807
F 0 22.134623
ARR 0 17 23.076884
CrL M 0 17 0 23.076974
ARR 2 18 23.086048
CrL M 2 18 1 23.086120
L MedianBlur 0 23.099808
L MedianBlur 1 23.129800
F 0 23.326789
F 1 23.356858
ARR 1 19 28.179893
CrL M 1 19 0 28.179983
L MedianBlur 0 28.209807
ARR 4 20 28.218569
CrL M 4 20 1 28.218633
L MedianBlur 1 28.239811
F 0 28.436902
F 1 28.467017
ARR 0 21 29.309618
CrL M 0 21 0 29.309700
L MedianBlur 0 29.329807
F 0 29.479331
ARR 3 22 30.361610
CrL M 3 22 0 30.361675
L MedianBlur 0 30.409801
F 0 30.559349
ARR 0 23 32.415502
CrL M 0 23 0 32.415586
L MedianBlur 0 32.469805
ARR 4 24 32.512429
CrS MedianBlur GaussianBlur gaussianblur_0 32.512460
CrL G 4 24 1 32.512523
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-GaussianBlur_pblock_gaussianblur_0_partial.bit 32.520506
L GaussianBlur 1 32.839617
F 0 32.849814
ARR 4 25 32.947534
CrL M 4 25 0 32.947603
L MedianBlur 0 32.969805
F 1 32.970226
ARR 2 26 33.031996
CrL G 2 26 1 33.032057
L GaussianBlur 1 33.069802
F 0 33.194489
F 1 33.200699
ARR 3 27 34.035677
CrL M 3 27 0 34.035743
ARR 3 28 34.046242
CrS MedianBlur GaussianBlur gaussianblur_0 34.046273
CrL M 3 28 1 34.046329
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-MedianBlur_pblock_gaussianblur_0_partial.bit 34.050360
L MedianBlur 0 34.059923
L MedianBlur 1 34.381687
F 0 34.389910
F 1 34.531205
ARR 4 29 35.417206
CrS GaussianBlur GaussianBlur medianblur_0 35.417251
CrL G 4 29 0 35.417324
S /home/xilinx/Bitstreams-axi_firewall/GaussianBlur-MedianBlur_pblock_medianblur_0_partial.bit 35.420368
L GaussianBlur 0 35.760352
F 0 35.890934
END 35.890955
