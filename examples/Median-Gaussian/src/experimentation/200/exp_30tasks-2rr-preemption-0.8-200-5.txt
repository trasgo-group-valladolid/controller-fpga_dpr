ARR 2 0 6.059897
CrS GaussianBlur GaussianBlur medianblur_0 6.059952
CrL M 2 0 0 6.060037
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-GaussianBlur_pblock_medianblur_0_partial.bit 6.070603
L MedianBlur 0 6.369463
F 0 6.444278
ARR 0 1 8.435642
CrL M 0 1 0 8.435728
L MedianBlur 0 8.479776
F 0 8.703897
ARR 4 2 11.496233
CrS GaussianBlur GaussianBlur medianblur_0 11.496277
CrL G 4 2 0 11.496350
S /home/xilinx/Bitstreams-axi_firewall/GaussianBlur-GaussianBlur_pblock_medianblur_0_partial.bit 11.510487
L GaussianBlur 0 11.809532
ARR 2 3 11.894340
CrL G 2 3 1 11.894430
L GaussianBlur 1 11.909782
F 0 11.940162
F 1 12.040376
ARR 3 4 13.917831
CrS GaussianBlur GaussianBlur medianblur_0 13.917877
CrL M 3 4 0 13.917952
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-GaussianBlur_pblock_medianblur_0_partial.bit 13.920338
L MedianBlur 0 14.251749
F 0 14.326596
ARR 1 5 14.349174
CrL M 1 5 0 14.349244
ARR 1 6 14.356553
CrL G 1 6 1 14.356616
L MedianBlur 0 14.369777
ARR 4 7 14.374543
R 0 14.374874
EQ 1 5
EV 5 7 0 14.375071
CrS MedianBlur MedianBlur medianblur_0 14.375090
CrL G 4 7 0 14.375126
S /home/xilinx/Bitstreams-axi_firewall/GaussianBlur-GaussianBlur_pblock_medianblur_0_partial.bit 14.375609
L GaussianBlur 1 14.379908
L GaussianBlur 0 14.721032
F 1 14.729766
DQ 1 5
CrS MedianBlur GaussianBlur gaussianblur_0 14.729831
CrL M 1 5 1 14.730545
S /home/xilinx/Bitstreams-axi_firewall/GaussianBlur-MedianBlur_pblock_gaussianblur_0_partial.bit 14.730792
L MedianBlur 1 15.029416
F 0 15.039836
F 1 15.104265
ARR 2 8 16.135369
CrL G 2 8 0 16.135458
L GaussianBlur 0 16.139810
ARR 0 9 16.167236
CrL M 0 9 1 16.167301
L MedianBlur 1 16.169791
F 0 16.270661
F 1 16.394333
ARR 2 10 17.256652
CrS GaussianBlur GaussianBlur medianblur_0 17.256695
CrL M 2 10 0 17.256764
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-MedianBlur_pblock_medianblur_0_partial.bit 17.270482
L MedianBlur 0 17.575857
F 0 17.650689
ARR 2 11 17.664578
CrL M 2 11 0 17.664647
L MedianBlur 0 17.689777
ARR 1 12 17.759482
CrL M 1 12 1 17.759563
L MedianBlur 1 17.759785
F 1 17.909919
F 0 17.914641
ARR 3 13 18.833825
CrL M 3 13 0 18.833893
L MedianBlur 0 18.834265
ARR 4 14 18.898191
CrL M 4 14 1 18.898253
L MedianBlur 1 18.919778
ARR 4 15 18.921807
R 0 18.922130
EQ 3 13
EV 13 15 0 18.922316
CrL M 4 15 0 18.922346
L MedianBlur 0 18.949815
F 0 19.101500
DQ 3 13
CrL M 3 13 0 19.101565
L MedianBlur 0 19.109789
F 1 19.146253
F 0 19.246421
ARR 3 16 21.987575
CrL M 3 16 0 21.987665
L MedianBlur 0 22.009808
F 0 22.084651
ARR 0 17 23.026916
CrL M 0 17 0 23.026982
ARR 2 18 23.036042
CrL M 2 18 1 23.036104
L MedianBlur 1 23.039797
L MedianBlur 0 23.059779
F 1 23.264744
F 0 23.284737
ARR 1 19 28.129867
CrL M 1 19 0 28.129959
L MedianBlur 0 28.159784
ARR 4 20 28.168553
CrL M 4 20 1 28.168618
L MedianBlur 1 28.189778
F 0 28.386895
F 1 28.416998
ARR 0 21 29.259568
CrL M 0 21 0 29.259634
L MedianBlur 0 29.279778
F 0 29.429291
ARR 3 22 30.311515
CrL M 3 22 0 30.311599
L MedianBlur 0 30.359776
F 0 30.509309
ARR 0 23 32.365403
CrL M 0 23 0 32.365486
L MedianBlur 0 32.369795
ARR 4 24 32.462333
CrS MedianBlur GaussianBlur gaussianblur_0 32.462364
CrL G 4 24 1 32.462425
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-GaussianBlur_pblock_gaussianblur_0_partial.bit 32.470469
L GaussianBlur 1 32.764854
F 0 32.769809
ARR 4 25 32.867535
CrL M 4 25 0 32.867609
L MedianBlur 0 32.889778
F 1 32.895478
ARR 2 26 32.952034
CrL G 2 26 1 32.952097
L GaussianBlur 1 32.959788
F 1 33.090670
F 0 33.114475
ARR 3 27 33.955738
CrL M 3 27 0 33.955825
ARR 3 28 33.966340
CrS MedianBlur GaussianBlur gaussianblur_0 33.966375
CrL M 3 28 1 33.966437
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-MedianBlur_pblock_gaussianblur_0_partial.bit 33.970367
L MedianBlur 0 33.979925
L MedianBlur 1 34.268327
F 0 34.279770
F 1 34.417858
ARR 4 29 35.307044
CrS GaussianBlur GaussianBlur medianblur_0 35.307083
CrL G 4 29 0 35.307149
S /home/xilinx/Bitstreams-axi_firewall/GaussianBlur-MedianBlur_pblock_medianblur_0_partial.bit 35.310324
L GaussianBlur 0 35.639995
F 0 35.770597
END 35.770623
