ARR 2 0 4.016142
CrS GaussianBlur GaussianBlur medianblur_0 4.016192
CrL M 2 0 0 4.016285
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-GaussianBlur_pblock_medianblur_0_partial.bit 4.016897
L MedianBlur 0 4.353373
F 0 4.428202
ARR 0 1 5.419592
CrL M 0 1 0 5.419674
L MedianBlur 0 5.429808
F 0 5.653942
ARR 4 2 7.445168
CrS GaussianBlur GaussianBlur medianblur_0 7.445201
CrL G 4 2 0 7.445270
S /home/xilinx/Bitstreams-axi_firewall/GaussianBlur-GaussianBlur_pblock_medianblur_0_partial.bit 7.470363
L GaussianBlur 0 7.780938
ARR 2 3 7.842831
CrL G 2 3 1 7.842897
L GaussianBlur 1 7.869791
F 0 7.911533
F 1 8.000369
ARR 3 4 8.882398
CrS GaussianBlur GaussianBlur medianblur_0 8.882426
CrL M 3 4 0 8.882489
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-GaussianBlur_pblock_medianblur_0_partial.bit 8.900363
L MedianBlur 0 9.212912
ARR 1 5 9.268734
CrS MedianBlur GaussianBlur gaussianblur_0 9.268759
CrL M 1 5 1 9.268839
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-MedianBlur_pblock_gaussianblur_0_partial.bit 9.269570
L MedianBlur 1 9.611354
F 0 9.611446
ARR 1 6 9.616629
CrS GaussianBlur MedianBlur medianblur_0 9.616656
CrL G 1 6 0 9.616716
S /home/xilinx/Bitstreams-axi_firewall/GaussianBlur-MedianBlur_pblock_medianblur_0_partial.bit 9.640341
L GaussianBlur 0 9.951075
F 1 9.959915
ARR 4 7 9.971187
CrS MedianBlur GaussianBlur gaussianblur_0 9.971213
CrL G 4 7 1 9.971275
S /home/xilinx/Bitstreams-axi_firewall/GaussianBlur-GaussianBlur_pblock_gaussianblur_0_partial.bit 9.980394
L GaussianBlur 1 10.267189
F 0 10.270006
F 1 10.397792
ARR 2 8 11.292247
CrL G 2 8 0 11.292316
ARR 0 9 11.312245
CrS MedianBlur GaussianBlur gaussianblur_0 11.312280
CrL M 0 9 1 11.312352
S /home/xilinx/Bitstreams-axi_firewall/GaussianBlur-MedianBlur_pblock_gaussianblur_0_partial.bit 11.330361
L GaussianBlur 0 11.339904
L MedianBlur 1 11.618746
F 0 11.629783
F 1 11.842941
ARR 2 10 12.648945
CrS GaussianBlur GaussianBlur medianblur_0 12.648977
CrL M 2 10 0 12.649043
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-MedianBlur_pblock_medianblur_0_partial.bit 12.660454
L MedianBlur 0 12.973710
ARR 2 11 13.026655
CrL M 2 11 1 13.026726
F 0 13.048562
L MedianBlur 1 13.059798
ARR 1 12 13.086209
CrL M 1 12 0 13.086265
L MedianBlur 0 13.129789
F 0 13.279871
F 1 13.284599
ARR 3 13 14.095611
CrL M 3 13 0 14.095701
ARR 4 14 14.135978
CrL M 4 14 1 14.136161
L MedianBlur 0 14.144983
ARR 4 15 14.150982
EQ 4 15
L MedianBlur 1 14.199799
F 0 14.369894
DQ 4 15
CrL M 4 15 0 14.369956
L MedianBlur 0 14.409789
F 1 14.424782
F 0 14.559530
ARR 3 16 16.179457
CrL M 3 16 0 16.179542
L MedianBlur 0 16.189807
F 0 16.264630
ARR 0 17 16.266197
CrL M 0 17 0 16.266253
ARR 2 18 16.271998
CrL M 2 18 1 16.272052
L MedianBlur 1 16.299799
L MedianBlur 0 16.309792
F 1 16.524718
F 0 16.534729
ARR 1 19 19.343444
CrL M 1 19 0 19.343526
ARR 4 20 19.367705
CrL M 4 20 1 19.367767
L MedianBlur 1 19.390399
L MedianBlur 0 19.399822
F 1 19.615461
F 0 19.624826
ARR 0 21 20.387562
CrL M 0 21 0 20.387621
L MedianBlur 0 20.399800
ARR 3 22 20.482057
CrL M 3 22 1 20.482111
L MedianBlur 1 20.519787
F 0 20.549853
F 1 20.669879
ARR 0 23 21.539883
CrL M 0 23 0 21.539965
L MedianBlur 0 21.589822
ARR 4 24 21.600599
CrS MedianBlur GaussianBlur gaussianblur_0 21.600625
CrL G 4 24 1 21.600696
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-GaussianBlur_pblock_gaussianblur_0_partial.bit 21.601284
L GaussianBlur 1 21.905857
F 0 21.909820
ARR 4 25 21.971168
CrL M 4 25 0 21.971230
L MedianBlur 0 21.999801
ARR 2 26 22.023938
EQ 2 26
F 1 22.036566
DQ 2 26
CrL G 2 26 1 22.036626
L GaussianBlur 1 22.059790
ARR 3 27 22.088357
EQ 3 27
ARR 3 28 22.095005
EQ 3 28
ARR 4 29 22.173710
EQ 4 29
F 1 22.190726
DQ 3 27
CrS MedianBlur GaussianBlur gaussianblur_0 22.190760
CrL M 3 27 1 22.190819
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-MedianBlur_pblock_gaussianblur_0_partial.bit 22.220333
L MedianBlur 1 22.509495
F 0 22.509909
DQ 3 28
CrL M 3 28 0 22.509983
L MedianBlur 0 22.519813
F 1 22.659540
DQ 4 29
CrS MedianBlur GaussianBlur gaussianblur_0 22.659569
CrL G 4 29 1 22.659632
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-GaussianBlur_pblock_gaussianblur_0_partial.bit 22.660319
L GaussianBlur 1 22.991158
F 0 22.999881
F 1 23.121742
END 23.121766
