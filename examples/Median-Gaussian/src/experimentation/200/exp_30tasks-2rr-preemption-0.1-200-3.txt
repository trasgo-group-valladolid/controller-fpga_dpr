ARR 2 0 0.082830
CrS GaussianBlur GaussianBlur medianblur_0 0.082879
CrL M 2 0 0 0.082959
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-GaussianBlur_pblock_medianblur_0_partial.bit 0.100573
L MedianBlur 0 0.413633
ARR 0 1 0.452998
CrS MedianBlur GaussianBlur gaussianblur_0 0.453028
CrL M 0 1 1 0.453099
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-MedianBlur_pblock_gaussianblur_0_partial.bit 0.470770
L MedianBlur 1 0.778982
F 0 0.779870
ARR 4 2 0.824764
CrS GaussianBlur MedianBlur medianblur_0 0.824794
CrL G 4 2 0 0.824860
S /home/xilinx/Bitstreams-axi_firewall/GaussianBlur-MedianBlur_pblock_medianblur_0_partial.bit 0.850362
L GaussianBlur 0 1.149412
F 1 1.159858
ARR 2 3 1.170658
CrS MedianBlur GaussianBlur gaussianblur_0 1.170687
CrL G 2 3 1 1.170751
S /home/xilinx/Bitstreams-axi_firewall/GaussianBlur-GaussianBlur_pblock_gaussianblur_0_partial.bit 1.200420
L GaussianBlur 1 1.510617
F 0 1.510711
ARR 3 4 1.538562
CrS GaussianBlur MedianBlur medianblur_0 1.538594
CrL M 3 4 0 1.538655
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-GaussianBlur_pblock_medianblur_0_partial.bit 1.540382
L MedianBlur 0 1.843949
F 1 1.849881
ARR 1 5 1.861786
CrS MedianBlur GaussianBlur gaussianblur_0 1.861818
CrL M 1 5 1 1.861881
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-MedianBlur_pblock_gaussianblur_0_partial.bit 1.870461
L MedianBlur 1 2.167712
F 0 2.169882
ARR 1 6 2.171544
CrS GaussianBlur MedianBlur medianblur_0 2.171574
CrL G 1 6 0 2.172108
S /home/xilinx/Bitstreams-axi_firewall/GaussianBlur-MedianBlur_pblock_medianblur_0_partial.bit 2.172220
L GaussianBlur 0 2.501737
F 1 2.509970
ARR 4 7 2.512420
CrS MedianBlur GaussianBlur gaussianblur_0 2.512450
CrL G 4 7 1 2.512510
S /home/xilinx/Bitstreams-axi_firewall/GaussianBlur-GaussianBlur_pblock_gaussianblur_0_partial.bit 2.530417
L GaussianBlur 1 2.828387
F 0 2.839955
ARR 2 8 2.864933
CrL G 2 8 0 2.864997
ARR 0 9 2.869166
EQ 0 9
L GaussianBlur 0 2.889874
ARR 2 10 2.892882
EQ 2 10
ARR 2 11 2.903659
EQ 2 11
ARR 1 12 2.915694
EQ 1 12
ARR 3 13 2.937514
R 0 2.937837
EQ 2 8
EV 8 13 0 2.938030
CrS MedianBlur MedianBlur medianblur_0 2.938046
CrL M 3 13 0 2.938079
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-GaussianBlur_pblock_medianblur_0_partial.bit 2.938569
L MedianBlur 0 3.269434
F 1 3.279843
DQ 0 9
CrS MedianBlur GaussianBlur gaussianblur_0 3.279877
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-MedianBlur_pblock_gaussianblur_0_partial.bit 3.280530
CrL M 0 9 1 3.280531
L MedianBlur 1 3.601596
F 0 3.601686
DQ 1 12
CrL M 1 12 0 3.602304
ARR 4 14 3.611008
R 0 3.611305
EQ 1 12
EV 12 14 0 3.611486
CrL M 4 14 0 3.611512
ARR 4 15 3.614653
R 1 3.614921
EQ 0 9
EV 9 15 1 3.615100
CrL M 4 15 1 3.615125
L MedianBlur 0 3.629913
L MedianBlur 1 3.639864
ARR 3 16 3.660557
EQ 3 16
ARR 0 17 3.678020
EQ 0 17
ARR 2 18 3.679349
EQ 2 18
ARR 1 19 3.753209
EQ 1 19
ARR 4 20 3.758219
EQ 4 20
ARR 0 21 3.782129
EQ 0 21
F 1 3.789957
DQ 0 9
CrL M 0 9 1 3.790015
L MedianBlur 1 3.799902
ARR 3 22 3.801408
R 1 3.801694
EQ 0 9
EV 9 22 1 3.801874
CrL M 3 22 1 3.801898
ARR 0 23 3.833563
EQ 0 23
L MedianBlur 1 3.839854
ARR 4 24 3.845859
R 1 3.846131
EQ 3 22
EV 22 24 1 3.846311
CrS MedianBlur MedianBlur gaussianblur_0 3.846326
CrL G 4 24 1 3.846358
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-GaussianBlur_pblock_gaussianblur_0_partial.bit 3.846847
L GaussianBlur 1 4.167594
F 0 4.169978
DQ 0 17
CrL M 0 17 0 4.170588
ARR 4 25 4.183357
R 0 4.183652
EQ 0 17
EV 17 25 0 4.183834
CrL M 4 25 0 4.183860
ARR 2 26 4.194573
EQ 2 26
ARR 3 27 4.207582
EQ 3 27
ARR 3 28 4.209089
EQ 3 28
L MedianBlur 0 4.209908
ARR 4 29 4.224997
EQ 4 29
F 1 4.298412
DQ 0 21
CrS MedianBlur GaussianBlur gaussianblur_0 4.298443
CrL M 0 21 1 4.298500
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-MedianBlur_pblock_gaussianblur_0_partial.bit 4.320407
L MedianBlur 1 4.609314
F 0 4.619920
DQ 0 9
CrL M 0 9 0 4.619994
L MedianBlur 0 4.649857
F 1 4.759214
DQ 0 23
CrL M 0 23 1 4.759280
L MedianBlur 1 4.799838
F 0 4.860711
DQ 0 17
CrL M 0 17 0 4.860773
L MedianBlur 0 4.889841
F 1 4.950954
DQ 1 12
CrL M 1 12 1 4.951015
L MedianBlur 1 4.959848
F 1 5.109925
DQ 1 19
CrL M 1 19 1 5.110051
F 0 5.115346
DQ 2 10
CrL M 2 10 0 5.115407
L MedianBlur 1 5.159869
L MedianBlur 0 5.169853
F 0 5.244923
DQ 2 11
CrL M 2 11 0 5.244983
L MedianBlur 0 5.289837
F 1 5.384654
DQ 2 8
CrS MedianBlur GaussianBlur gaussianblur_0 5.384682
CrL G 2 8 1 5.384734
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-GaussianBlur_pblock_gaussianblur_0_partial.bit 5.400406
L GaussianBlur 1 5.706104
F 0 5.719842
DQ 2 18
CrL M 2 18 0 5.720457
L MedianBlur 0 5.749877
F 1 5.836884
DQ 2 26
CrL G 2 26 1 5.836946
L GaussianBlur 1 5.859841
F 0 5.974753
DQ 3 16
CrL M 3 16 0 5.974816
F 1 5.990704
DQ 3 22
CrS MedianBlur GaussianBlur gaussianblur_0 5.990733
CrL M 3 22 1 5.990788
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-MedianBlur_pblock_gaussianblur_0_partial.bit 6.020387
L MedianBlur 0 6.029969
L MedianBlur 1 6.333871
F 0 6.333945
DQ 3 27
CrL M 3 27 0 6.334558
L MedianBlur 0 6.389845
F 1 6.483716
DQ 3 28
CrL M 3 28 1 6.483778
L MedianBlur 1 6.489861
F 0 6.539921
DQ 4 20
CrL M 4 20 0 6.539988
L MedianBlur 0 6.589839
F 1 6.639712
DQ 4 29
CrS MedianBlur GaussianBlur gaussianblur_0 6.639741
CrL G 4 29 1 6.639797
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-GaussianBlur_pblock_gaussianblur_0_partial.bit 6.650442
L GaussianBlur 1 6.928914
F 0 6.939925
F 1 7.059494
END 7.059513
