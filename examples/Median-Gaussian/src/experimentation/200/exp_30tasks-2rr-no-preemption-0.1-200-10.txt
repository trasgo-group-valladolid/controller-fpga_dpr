ARR 2 0 0.082821
CrS GaussianBlur GaussianBlur medianblur_0 0.082862
CrL M 2 0 0 0.082938
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-GaussianBlur_pblock_medianblur_0_partial.bit 0.100449
L MedianBlur 0 0.389368
ARR 0 1 0.422942
CrS MedianBlur GaussianBlur gaussianblur_0 0.422968
CrL M 0 1 1 0.423037
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-MedianBlur_pblock_gaussianblur_0_partial.bit 0.440595
L MedianBlur 1 0.738664
F 0 0.749787
ARR 4 2 0.795247
CrS GaussianBlur MedianBlur medianblur_0 0.795274
CrL G 4 2 0 0.795333
S /home/xilinx/Bitstreams-axi_firewall/GaussianBlur-MedianBlur_pblock_medianblur_0_partial.bit 0.820350
L GaussianBlur 0 1.153182
F 1 1.159897
ARR 2 3 1.171247
CrS MedianBlur GaussianBlur gaussianblur_0 1.171274
CrL G 2 3 1 1.171333
S /home/xilinx/Bitstreams-axi_firewall/GaussianBlur-GaussianBlur_pblock_gaussianblur_0_partial.bit 1.180383
L GaussianBlur 1 1.487527
F 0 1.489923
ARR 3 4 1.517747
CrS GaussianBlur MedianBlur medianblur_0 1.517772
CrL M 3 4 0 1.517834
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-GaussianBlur_pblock_medianblur_0_partial.bit 1.530386
L MedianBlur 0 1.846316
F 1 1.859785
ARR 1 5 1.871696
CrS MedianBlur GaussianBlur gaussianblur_0 1.871722
CrL M 1 5 1 1.871781
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-MedianBlur_pblock_gaussianblur_0_partial.bit 1.890359
L MedianBlur 1 2.212038
F 0 2.212130
ARR 1 6 2.213242
CrS GaussianBlur MedianBlur medianblur_0 2.213265
CrL G 1 6 0 2.213325
S /home/xilinx/Bitstreams-axi_firewall/GaussianBlur-MedianBlur_pblock_medianblur_0_partial.bit 2.250349
L GaussianBlur 0 2.562723
F 1 2.569920
ARR 4 7 2.572346
CrS MedianBlur GaussianBlur gaussianblur_0 2.572371
CrL G 4 7 1 2.572433
S /home/xilinx/Bitstreams-axi_firewall/GaussianBlur-GaussianBlur_pblock_gaussianblur_0_partial.bit 2.590382
L GaussianBlur 1 2.877710
F 0 2.889783
ARR 2 8 2.914762
CrL G 2 8 0 2.914821
ARR 0 9 2.918991
EQ 0 9
L GaussianBlur 0 2.939796
ARR 2 10 2.942706
EQ 2 10
ARR 2 11 2.953481
EQ 2 11
ARR 1 12 2.965514
EQ 1 12
ARR 3 13 2.987331
EQ 3 13
ARR 4 14 2.995554
EQ 4 14
ARR 4 15 2.998691
EQ 4 15
F 1 3.008338
DQ 0 9
CrS MedianBlur GaussianBlur gaussianblur_0 3.008362
CrL M 0 9 1 3.008415
S /home/xilinx/Bitstreams-axi_firewall/GaussianBlur-MedianBlur_pblock_gaussianblur_0_partial.bit 3.030353
L MedianBlur 1 3.345620
F 0 3.359786
DQ 1 12
CrS GaussianBlur MedianBlur medianblur_0 3.359814
CrL M 1 12 0 3.360429
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-MedianBlur_pblock_medianblur_0_partial.bit 3.390343
L MedianBlur 0 3.699251
F 1 3.699925
DQ 2 10
CrL M 2 10 1 3.699991
ARR 3 16 3.735884
EQ 3 16
L MedianBlur 1 3.749797
ARR 0 17 3.753361
EQ 0 17
ARR 2 18 3.754689
EQ 2 18
F 1 3.825168
DQ 0 17
CrL M 0 17 1 3.825221
ARR 1 19 3.828773
EQ 1 19
ARR 4 20 3.833779
EQ 4 20
F 0 3.849138
DQ 1 19
CrL M 1 19 0 3.849189
L MedianBlur 0 3.849799
ARR 0 21 3.857915
EQ 0 21
L MedianBlur 1 3.869798
ARR 3 22 3.876963
EQ 3 22
ARR 0 23 3.908628
EQ 0 23
ARR 4 24 3.920915
EQ 4 24
ARR 4 25 3.933244
EQ 4 25
ARR 2 26 3.943952
EQ 2 26
ARR 3 27 3.956952
EQ 3 27
ARR 3 28 3.958455
EQ 3 28
ARR 4 29 3.974357
EQ 4 29
F 0 4.074767
DQ 0 21
CrL M 0 21 0 4.074833
F 1 4.094779
DQ 0 23
CrL M 0 23 1 4.094836
L MedianBlur 1 4.109794
L MedianBlur 0 4.119803
F 1 4.259811
DQ 2 11
CrL M 2 11 1 4.259866
F 0 4.269791
L MedianBlur 1 4.269801
DQ 2 18
CrL M 2 18 0 4.269883
L MedianBlur 0 4.319797
F 1 4.494701
DQ 2 26
CrS MedianBlur GaussianBlur gaussianblur_0 4.494724
CrL G 2 26 1 4.494777
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-GaussianBlur_pblock_gaussianblur_0_partial.bit 4.510359
L GaussianBlur 1 4.809585
F 0 4.819868
DQ 3 13
CrL M 3 13 0 4.819936
L MedianBlur 0 4.859801
F 1 4.940368
DQ 3 16
CrS MedianBlur GaussianBlur gaussianblur_0 4.940393
CrL M 3 16 1 4.940451
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-MedianBlur_pblock_gaussianblur_0_partial.bit 4.960345
L MedianBlur 1 5.259211
F 0 5.279801
DQ 3 22
CrL M 3 22 0 5.280423
L MedianBlur 0 5.306722
F 1 5.334207
DQ 3 27
CrL M 3 27 1 5.334279
L MedianBlur 1 5.369792
F 0 5.456666
DQ 3 28
CrL M 3 28 0 5.456724
L MedianBlur 0 5.499788
F 1 5.519687
DQ 4 14
CrL M 4 14 1 5.519864
L MedianBlur 1 5.579831
F 0 5.649650
DQ 4 15
CrL M 4 15 0 5.649715
L MedianBlur 0 5.649796
F 0 5.799885
DQ 4 20
CrL M 4 20 0 5.799945
F 1 5.804850
DQ 4 24
CrS MedianBlur GaussianBlur gaussianblur_0 5.804876
CrL G 4 24 1 5.804932
L MedianBlur 0 5.809803
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-GaussianBlur_pblock_gaussianblur_0_partial.bit 5.820375
L GaussianBlur 1 6.127605
F 0 6.129904
DQ 4 25
CrL M 4 25 0 6.129974
L MedianBlur 0 6.169798
F 1 6.258395
DQ 4 29
CrL G 4 29 1 6.258455
L GaussianBlur 1 6.279799
F 0 6.394709
F 1 6.410656
END 6.410671
