ARR 2 0 0.082826
CrS GaussianBlur GaussianBlur medianblur_0 0.082869
CrL M 2 0 0 0.082947
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-GaussianBlur_pblock_medianblur_0_partial.bit 0.110427
L MedianBlur 0 0.440206
ARR 0 1 0.473326
CrS MedianBlur GaussianBlur gaussianblur_0 0.473350
CrL M 0 1 1 0.473416
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-MedianBlur_pblock_gaussianblur_0_partial.bit 0.490587
L MedianBlur 1 0.798902
F 0 0.799819
ARR 4 2 0.845253
CrS GaussianBlur MedianBlur medianblur_0 0.845280
CrL G 4 2 0 0.845341
S /home/xilinx/Bitstreams-axi_firewall/GaussianBlur-MedianBlur_pblock_medianblur_0_partial.bit 0.860344
L GaussianBlur 0 1.171607
F 1 1.179782
ARR 2 3 1.191143
CrS MedianBlur GaussianBlur gaussianblur_0 1.191168
CrL G 2 3 1 1.191227
S /home/xilinx/Bitstreams-axi_firewall/GaussianBlur-GaussianBlur_pblock_gaussianblur_0_partial.bit 1.200400
L GaussianBlur 1 1.501243
F 0 1.509913
ARR 3 4 1.537773
CrS GaussianBlur MedianBlur medianblur_0 1.537804
CrL M 3 4 0 1.537875
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-GaussianBlur_pblock_medianblur_0_partial.bit 1.550424
L MedianBlur 0 1.848766
F 1 1.859785
ARR 1 5 1.871689
CrS MedianBlur GaussianBlur gaussianblur_0 1.871716
CrL M 1 5 1 1.871776
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-MedianBlur_pblock_gaussianblur_0_partial.bit 1.900351
L MedianBlur 1 2.194987
F 0 2.195075
ARR 1 6 2.196717
CrS GaussianBlur MedianBlur medianblur_0 2.196742
CrL G 1 6 0 2.197224
S /home/xilinx/Bitstreams-axi_firewall/GaussianBlur-MedianBlur_pblock_medianblur_0_partial.bit 2.210333
L GaussianBlur 0 2.532251
F 1 2.549803
ARR 4 7 2.552784
CrS MedianBlur GaussianBlur gaussianblur_0 2.552808
CrL G 4 7 1 2.552865
S /home/xilinx/Bitstreams-axi_firewall/GaussianBlur-GaussianBlur_pblock_gaussianblur_0_partial.bit 2.560438
L GaussianBlur 1 2.878304
F 0 2.879822
ARR 2 8 2.904797
CrL G 2 8 0 2.904858
ARR 0 9 2.909027
EQ 0 9
L GaussianBlur 0 2.921952
ARR 2 10 2.932743
EQ 2 10
ARR 2 11 2.943523
EQ 2 11
ARR 1 12 2.955563
EQ 1 12
ARR 3 13 2.977383
EQ 3 13
ARR 4 14 2.985605
EQ 4 14
ARR 4 15 2.988741
EQ 4 15
F 1 3.008941
DQ 0 9
CrS MedianBlur GaussianBlur gaussianblur_0 3.008965
CrL M 0 9 1 3.009021
S /home/xilinx/Bitstreams-axi_firewall/GaussianBlur-MedianBlur_pblock_gaussianblur_0_partial.bit 3.020381
L MedianBlur 1 3.323480
F 0 3.329918
DQ 1 12
CrS GaussianBlur MedianBlur medianblur_0 3.329944
CrL M 1 12 0 3.330009
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-MedianBlur_pblock_medianblur_0_partial.bit 3.340395
L MedianBlur 0 3.649114
F 1 3.649939
DQ 2 10
CrL M 2 10 1 3.650009
L MedianBlur 1 3.659811
ARR 3 16 3.675374
EQ 3 16
ARR 0 17 3.692847
EQ 0 17
ARR 2 18 3.694177
EQ 2 18
F 1 3.734912
DQ 0 17
CrL M 0 17 1 3.734967
L MedianBlur 1 3.739802
ARR 1 19 3.768256
EQ 1 19
ARR 4 20 3.773264
EQ 4 20
ARR 0 21 3.797175
EQ 0 21
F 0 3.799930
DQ 0 21
CrL M 0 21 0 3.799981
ARR 3 22 3.816449
EQ 3 22
L MedianBlur 0 3.839789
ARR 0 23 3.848115
EQ 0 23
ARR 4 24 3.860397
EQ 4 24
ARR 4 25 3.872726
EQ 4 25
ARR 2 26 3.883438
EQ 2 26
ARR 3 27 3.896437
EQ 3 27
ARR 3 28 3.897941
EQ 3 28
ARR 4 29 3.913842
EQ 4 29
F 1 3.965277
DQ 0 23
CrL M 0 23 1 3.965335
F 0 3.989776
DQ 1 19
CrL M 1 19 0 3.989831
L MedianBlur 1 3.999801
L MedianBlur 0 4.029787
F 1 4.151093
DQ 2 11
CrL M 2 11 1 4.151153
L MedianBlur 1 4.169804
F 0 4.256195
DQ 2 18
CrL M 2 18 0 4.256251
L MedianBlur 0 4.299787
F 1 4.394687
DQ 2 26
CrS MedianBlur GaussianBlur gaussianblur_0 4.394712
CrL G 2 26 1 4.394766
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-GaussianBlur_pblock_gaussianblur_0_partial.bit 4.410353
L GaussianBlur 1 4.698919
F 0 4.709784
DQ 3 13
CrL M 3 13 0 4.710392
L MedianBlur 0 4.739799
F 1 4.829779
DQ 3 16
CrS MedianBlur GaussianBlur gaussianblur_0 4.829804
CrL M 3 16 1 4.829860
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-MedianBlur_pblock_gaussianblur_0_partial.bit 4.850373
L MedianBlur 1 5.139402
F 0 5.149883
DQ 3 22
CrL M 3 22 0 5.149949
L MedianBlur 0 5.159824
F 1 5.214420
DQ 3 27
CrL M 3 27 1 5.214476
L MedianBlur 1 5.249794
F 0 5.310687
DQ 3 28
CrL M 3 28 0 5.310752
L MedianBlur 0 5.349790
F 1 5.400865
DQ 4 14
CrL M 4 14 1 5.400988
L MedianBlur 1 5.449788
F 0 5.499717
DQ 4 15
CrL M 4 15 0 5.499794
L MedianBlur 0 5.549794
F 1 5.674697
DQ 4 20
CrL M 4 20 1 5.674775
F 0 5.699785
DQ 4 24
CrS GaussianBlur MedianBlur medianblur_0 5.699815
CrL G 4 24 0 5.699872
S /home/xilinx/Bitstreams-axi_firewall/GaussianBlur-MedianBlur_pblock_medianblur_0_partial.bit 5.710412
L MedianBlur 1 5.719911
L GaussianBlur 0 6.015873
F 1 6.029781
DQ 4 25
CrL M 4 25 1 6.030401
L MedianBlur 1 6.079792
F 0 6.146611
DQ 4 29
CrL G 4 29 0 6.146670
L GaussianBlur 0 6.169804
F 0 6.300713
F 1 6.304705
END 6.304719
