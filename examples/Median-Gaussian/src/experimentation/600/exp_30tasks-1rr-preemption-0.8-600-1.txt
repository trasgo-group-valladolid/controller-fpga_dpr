ARR 2 0 6.059900
CrS GaussianBlur  medianblur_0 6.059954
CrL M 2 0 0 6.060035
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-GaussianBlur_pblock_medianblur_0_partial.bit 6.070648
L MedianBlur 0 6.368836
F 0 7.035719
ARR 0 1 8.434442
CrL M 0 1 0 8.434511
L MedianBlur 0 8.469780
F 0 10.473657
ARR 4 2 11.493279
CrS GaussianBlur  medianblur_0 11.493323
CrL G 4 2 0 11.493398
S /home/xilinx/Bitstreams-axi_firewall/GaussianBlur-GaussianBlur_pblock_medianblur_0_partial.bit 11.510377
L GaussianBlur 0 11.824231
ARR 2 3 11.909036
EQ 2 3
F 0 12.998011
DQ 2 3
CrL G 2 3 0 12.998106
L GaussianBlur 0 13.049782
ARR 3 4 13.931495
R 0 13.931854
EQ 2 3
EV 3 4 0 13.932057
CrS MedianBlur  medianblur_0 13.932078
CrL M 3 4 0 13.932115
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-GaussianBlur_pblock_medianblur_0_partial.bit 13.932610
L MedianBlur 0 14.260678
ARR 1 5 14.349864
EQ 1 5
ARR 1 6 14.357162
EQ 1 6
ARR 4 7 14.375100
R 0 14.375397
EQ 3 4
EV 4 7 0 14.375578
CrS MedianBlur  medianblur_0 14.375598
CrL G 4 7 0 14.375636
S /home/xilinx/Bitstreams-axi_firewall/GaussianBlur-GaussianBlur_pblock_medianblur_0_partial.bit 14.380331
L GaussianBlur 0 14.688780
ARR 2 8 15.783589
EQ 2 8
ARR 0 9 15.815341
EQ 0 9
F 0 15.862536
DQ 0 9
CrS GaussianBlur  medianblur_0 15.862574
CrL M 0 9 0 15.862637
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-GaussianBlur_pblock_medianblur_0_partial.bit 15.870396
L MedianBlur 0 16.168156
ARR 2 10 17.211890
R 0 17.212242
EQ 0 9
EV 9 10 0 17.212438
CrL M 2 10 0 17.212469
L MedianBlur 0 17.239794
ARR 2 11 17.297196
EQ 2 11
ARR 1 12 17.392010
EQ 1 12
F 0 17.906593
DQ 0 9
CrL M 0 9 0 17.906663
L MedianBlur 0 17.919783
ARR 3 13 18.465789
R 0 18.466098
EQ 0 9
EV 9 13 0 18.466287
CrL M 3 13 0 18.466316
L MedianBlur 0 18.469799
ARR 4 14 18.530599
R 0 18.530870
EQ 3 13
EV 13 14 0 18.531052
CrL M 4 14 0 18.531081
ARR 4 15 18.554677
EQ 4 15
L MedianBlur 0 18.589785
F 0 20.589927
DQ 0 9
CrL M 0 9 0 20.590016
L MedianBlur 0 20.590063
F 0 21.000382
DQ 1 5
CrL M 1 5 0 21.000471
L MedianBlur 0 21.059790
ARR 3 16 21.617780
R 0 21.618122
EQ 1 5
EV 5 16 0 21.618311
CrL M 3 16 0 21.618341
L MedianBlur 0 21.639816
F 0 22.306632
DQ 1 6
CrS GaussianBlur  medianblur_0 22.306669
CrL G 1 6 0 22.306732
S /home/xilinx/Bitstreams-axi_firewall/GaussianBlur-GaussianBlur_pblock_medianblur_0_partial.bit 22.330342
L GaussianBlur 0 22.640562
ARR 0 17 22.999876
EQ 0 17
ARR 2 18 23.008978
R 0 23.009299
EQ 1 6
EV 6 18 0 23.009487
CrS MedianBlur  medianblur_0 23.009507
CrL M 2 18 0 23.009546
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-GaussianBlur_pblock_medianblur_0_partial.bit 23.040114
L MedianBlur 0 23.339411
F 0 25.339402
DQ 0 17
CrL M 0 17 0 25.339493
L MedianBlur 0 25.379783
F 0 27.379993
DQ 1 12
CrL M 1 12 0 27.380085
L MedianBlur 0 27.409785
ARR 1 19 28.429869
EQ 1 19
ARR 4 20 28.468429
R 0 28.468766
EQ 1 12
EV 12 20 0 28.468954
CrL M 4 20 0 28.468984
L MedianBlur 0 28.489790
ARR 0 21 29.559854
EQ 0 21
F 0 30.489939
DQ 0 21
CrL M 0 21 0 30.490014
L MedianBlur 0 30.529781
ARR 3 22 30.611082
R 0 30.611400
EQ 0 21
EV 21 22 0 30.611585
CrL M 3 22 0 30.611614
L MedianBlur 0 30.659791
F 0 31.993323
DQ 0 21
CrL M 0 21 0 31.993412
L MedianBlur 0 32.019786
ARR 0 23 32.664354
EQ 0 23
ARR 4 24 32.761201
R 0 32.761528
EQ 0 21
EV 21 24 0 32.761714
CrS MedianBlur  medianblur_0 32.761733
CrL G 4 24 0 32.761769
S /home/xilinx/Bitstreams-axi_firewall/GaussianBlur-GaussianBlur_pblock_medianblur_0_partial.bit 32.780427
L GaussianBlur 0 33.111897
ARR 4 25 33.216883
EQ 4 25
ARR 2 26 33.301121
EQ 2 26
F 0 34.285632
DQ 0 23
CrS GaussianBlur  medianblur_0 34.285674
CrL M 0 23 0 34.285743
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-GaussianBlur_pblock_medianblur_0_partial.bit 34.300347
L MedianBlur 0 34.612207
ARR 3 27 34.630299
R 0 34.630565
EQ 0 23
EV 23 27 0 34.630757
CrL M 3 27 0 34.630787
ARR 3 28 34.641336
EQ 3 28
L MedianBlur 0 34.649804
ARR 4 29 35.668075
R 0 35.668406
EQ 3 27
EV 27 29 0 35.668598
CrS MedianBlur  medianblur_0 35.668619
CrL G 4 29 0 35.668657
S /home/xilinx/Bitstreams-axi_firewall/GaussianBlur-GaussianBlur_pblock_medianblur_0_partial.bit 35.690331
L GaussianBlur 0 36.012376
F 0 37.186129
DQ 0 21
CrS GaussianBlur  medianblur_0 37.186179
CrL M 0 21 0 37.186255
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-GaussianBlur_pblock_medianblur_0_partial.bit 37.210356
L MedianBlur 0 37.521857
F 0 38.855235
DQ 0 23
CrL M 0 23 0 38.855324
L MedianBlur 0 38.879790
F 0 40.195154
DQ 1 5
CrL M 1 5 0 40.195238
L MedianBlur 0 40.199802
F 0 40.308430
DQ 1 6
CrS GaussianBlur  medianblur_0 40.308464
CrL G 1 6 0 40.308526
S /home/xilinx/Bitstreams-axi_firewall/GaussianBlur-GaussianBlur_pblock_medianblur_0_partial.bit 40.320402
L GaussianBlur 0 40.645142
F 0 41.818894
DQ 1 19
CrS GaussianBlur  medianblur_0 41.818944
CrL M 1 19 0 41.819030
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-GaussianBlur_pblock_medianblur_0_partial.bit 41.819552
L MedianBlur 0 42.109382
F 0 44.109596
DQ 1 12
CrL M 1 12 0 44.109686
L MedianBlur 0 44.139814
F 0 44.414491
DQ 2 3
CrS GaussianBlur  medianblur_0 44.414527
CrL G 2 3 0 44.414591
S /home/xilinx/Bitstreams-axi_firewall/GaussianBlur-GaussianBlur_pblock_medianblur_0_partial.bit 44.420402
L GaussianBlur 0 44.731832
F 0 45.905570
DQ 2 8
CrL G 2 8 0 45.905663
L GaussianBlur 0 45.939783
F 0 47.113519
DQ 2 11
CrS GaussianBlur  medianblur_0 47.113568
CrL M 2 11 0 47.113644
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-GaussianBlur_pblock_medianblur_0_partial.bit 47.140356
L MedianBlur 0 47.429409
F 0 49.429671
DQ 2 26
CrS GaussianBlur  medianblur_0 49.429721
CrL G 2 26 0 49.429817
S /home/xilinx/Bitstreams-axi_firewall/GaussianBlur-GaussianBlur_pblock_medianblur_0_partial.bit 49.460392
L GaussianBlur 0 49.770005
F 0 50.943744
DQ 3 4
CrS GaussianBlur  medianblur_0 50.943792
CrL M 3 4 0 50.943878
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-GaussianBlur_pblock_medianblur_0_partial.bit 50.944394
L MedianBlur 0 51.271109
F 0 51.937900
DQ 3 13
CrL M 3 13 0 51.937983
L MedianBlur 0 51.949787
F 0 53.888884
DQ 3 28
CrL M 3 28 0 53.888977
L MedianBlur 0 53.949780
F 0 55.283416
DQ 3 27
CrL M 3 27 0 55.283508
L MedianBlur 0 55.289800
F 0 55.604880
DQ 4 15
CrL M 4 15 0 55.604950
L MedianBlur 0 55.649779
F 0 56.983189
DQ 4 25
CrL M 4 25 0 56.983280
L MedianBlur 0 57.019784
F 0 59.019960
END 59.019988
