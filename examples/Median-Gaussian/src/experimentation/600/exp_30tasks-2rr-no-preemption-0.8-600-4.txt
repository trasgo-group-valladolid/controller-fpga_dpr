ARR 2 0 6.059906
CrS GaussianBlur GaussianBlur medianblur_0 6.059955
CrL M 2 0 0 6.060034
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-GaussianBlur_pblock_medianblur_0_partial.bit 6.090450
L MedianBlur 0 6.395692
F 0 7.062516
ARR 0 1 8.465418
CrL M 0 1 0 8.465482
L MedianBlur 0 8.529782
F 0 10.529849
ARR 4 2 11.524249
CrS GaussianBlur GaussianBlur medianblur_0 11.524287
CrL G 4 2 0 11.524377
S /home/xilinx/Bitstreams-axi_firewall/GaussianBlur-GaussianBlur_pblock_medianblur_0_partial.bit 11.540386
L GaussianBlur 0 11.861882
ARR 2 3 11.946683
CrL G 2 3 1 11.946755
L GaussianBlur 1 11.959821
F 0 13.037412
F 1 13.135323
ARR 3 4 13.969139
CrS GaussianBlur GaussianBlur medianblur_0 13.969168
CrL M 3 4 0 13.969231
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-GaussianBlur_pblock_medianblur_0_partial.bit 13.990359
L MedianBlur 0 14.302402
ARR 1 5 14.399086
CrS MedianBlur GaussianBlur gaussianblur_0 14.399114
CrL M 1 5 1 14.399184
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-MedianBlur_pblock_gaussianblur_0_partial.bit 14.410654
L MedianBlur 1 14.718694
ARR 1 6 14.729869
EQ 1 6
ARR 4 7 14.748376
EQ 4 7
F 0 14.974644
DQ 1 6
CrS GaussianBlur MedianBlur medianblur_0 14.974683
CrL G 1 6 0 14.974759
S /home/xilinx/Bitstreams-axi_firewall/GaussianBlur-MedianBlur_pblock_medianblur_0_partial.bit 14.975276
L GaussianBlur 0 15.307832
F 1 15.392218
DQ 4 7
CrS MedianBlur GaussianBlur gaussianblur_0 15.392251
CrL G 4 7 1 15.392320
S /home/xilinx/Bitstreams-axi_firewall/GaussianBlur-GaussianBlur_pblock_gaussianblur_0_partial.bit 15.400390
L GaussianBlur 1 15.732147
F 0 16.490473
ARR 2 8 16.525368
CrL G 2 8 0 16.525435
ARR 0 9 16.557162
EQ 0 9
L GaussianBlur 0 16.579782
F 1 16.918652
DQ 0 9
CrS MedianBlur GaussianBlur gaussianblur_0 16.918693
CrL M 0 9 1 16.918782
S /home/xilinx/Bitstreams-axi_firewall/GaussianBlur-MedianBlur_pblock_gaussianblur_0_partial.bit 16.920391
L MedianBlur 1 17.229055
F 0 17.761577
ARR 2 10 17.956194
CrS GaussianBlur MedianBlur medianblur_0 17.956226
CrL M 2 10 0 17.956291
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-MedianBlur_pblock_medianblur_0_partial.bit 17.970472
L MedianBlur 0 18.310899
ARR 2 11 18.395553
EQ 2 11
ARR 1 12 18.490384
EQ 1 12
F 0 18.988682
DQ 1 12
CrL M 1 12 0 18.988767
L MedianBlur 0 18.989791
F 1 19.250222
DQ 2 11
CrL M 2 11 1 19.250288
L MedianBlur 1 19.279812
ARR 3 13 19.564200
EQ 3 13
ARR 4 14 19.628494
EQ 4 14
ARR 4 15 19.652053
EQ 4 15
F 0 20.344380
DQ 3 13
CrL M 3 13 0 20.344440
L MedianBlur 0 20.379781
F 1 21.313166
DQ 4 14
CrL M 4 14 1 21.313247
L MedianBlur 1 21.349795
F 0 22.414551
DQ 4 15
CrL M 4 15 0 22.414619
L MedianBlur 0 22.449794
ARR 3 16 22.715032
EQ 3 16
F 1 23.383104
DQ 3 16
CrL M 3 16 1 23.383189
L MedianBlur 1 23.399788
ARR 0 17 23.753837
EQ 0 17
ARR 2 18 23.762907
EQ 2 18
F 0 23.805480
DQ 0 17
CrL M 0 17 0 23.805538
L MedianBlur 0 23.849792
F 1 24.075233
DQ 2 18
CrL M 2 18 1 24.075294
L MedianBlur 1 24.099786
F 0 25.879780
F 1 26.121842
ARR 1 19 28.855639
CrL M 1 19 0 28.855721
L MedianBlur 0 28.859806
ARR 4 20 28.894265
CrL M 4 20 1 28.894324
L MedianBlur 1 28.919785
ARR 0 21 29.985198
EQ 0 21
F 0 30.896279
DQ 0 21
CrL M 0 21 0 30.896363
L MedianBlur 0 30.899803
F 1 30.957244
ARR 3 22 31.036543
CrL M 3 22 1 31.036602
L MedianBlur 1 31.089782
F 0 32.254777
F 1 32.443939
ARR 0 23 33.089345
CrL M 0 23 0 33.089426
L MedianBlur 0 33.119785
ARR 4 24 33.186274
CrS MedianBlur GaussianBlur gaussianblur_0 33.186300
CrL G 4 24 1 33.186365
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-GaussianBlur_pblock_gaussianblur_0_partial.bit 33.190374
L GaussianBlur 1 33.541965
ARR 4 25 33.646895
EQ 4 25
ARR 2 26 33.731136
EQ 2 26
F 0 34.460974
DQ 2 26
CrS GaussianBlur MedianBlur medianblur_0 34.461012
CrL G 2 26 0 34.461095
S /home/xilinx/Bitstreams-axi_firewall/GaussianBlur-GaussianBlur_pblock_medianblur_0_partial.bit 34.480347
L GaussianBlur 0 34.811770
F 1 34.819786
DQ 4 25
CrS MedianBlur GaussianBlur gaussianblur_0 34.819843
CrL M 4 25 1 34.820467
S /home/xilinx/Bitstreams-axi_firewall/GaussianBlur-MedianBlur_pblock_gaussianblur_0_partial.bit 34.821087
L MedianBlur 1 35.138250
ARR 3 27 35.412800
EQ 3 27
ARR 3 28 35.423324
EQ 3 28
F 0 35.989738
DQ 3 27
CrS GaussianBlur MedianBlur medianblur_0 35.989796
CrL M 3 27 0 35.989862
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-MedianBlur_pblock_medianblur_0_partial.bit 36.000392
L MedianBlur 0 36.343609
ARR 4 29 36.809659
EQ 4 29
F 1 37.160524
DQ 3 28
CrL M 3 28 1 37.160598
L MedianBlur 1 37.189785
F 0 37.700996
DQ 4 29
CrS GaussianBlur MedianBlur medianblur_0 37.701027
CrL G 4 29 0 37.701099
S /home/xilinx/Bitstreams-axi_firewall/GaussianBlur-MedianBlur_pblock_medianblur_0_partial.bit 37.701619
L GaussianBlur 0 38.019322
F 1 38.539303
F 0 39.195194
END 39.195225
