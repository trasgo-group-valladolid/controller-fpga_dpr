ARR 2 0 0.082831
CrS GaussianBlur GaussianBlur medianblur_0 0.082880
CrL M 2 0 0 0.082969
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-GaussianBlur_pblock_medianblur_0_partial.bit 0.083564
L MedianBlur 0 0.379523
ARR 0 1 0.412930
CrS MedianBlur GaussianBlur gaussianblur_0 0.412962
CrL M 0 1 1 0.413029
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-MedianBlur_pblock_gaussianblur_0_partial.bit 0.430605
L MedianBlur 1 0.754146
ARR 4 2 0.798966
R 0 0.799270
EQ 2 0
EV 0 2 0 0.799473
CrS MedianBlur MedianBlur medianblur_0 0.799490
CrL G 4 2 0 0.799523
S /home/xilinx/Bitstreams-axi_firewall/GaussianBlur-MedianBlur_pblock_medianblur_0_partial.bit 0.800288
L GaussianBlur 0 1.124542
ARR 2 3 1.135272
R 1 1.135536
EQ 0 1
EV 1 3 1 1.135725
CrS MedianBlur MedianBlur gaussianblur_0 1.135741
CrL G 2 3 1 1.135775
S /home/xilinx/Bitstreams-axi_firewall/GaussianBlur-GaussianBlur_pblock_gaussianblur_0_partial.bit 1.140363
L GaussianBlur 1 1.453707
ARR 3 4 1.487554
R 1 1.487820
EQ 2 3
EV 3 4 1 1.488010
CrS MedianBlur MedianBlur gaussianblur_0 1.488026
CrL M 3 4 1 1.488060
S /home/xilinx/Bitstreams-axi_firewall/GaussianBlur-MedianBlur_pblock_gaussianblur_0_partial.bit 1.490263
L MedianBlur 1 1.795167
ARR 1 5 1.806453
EQ 1 5
ARR 1 6 1.807579
EQ 1 6
ARR 4 7 1.810017
R 1 1.810310
EQ 3 4
EV 4 7 1 1.810490
CrS MedianBlur MedianBlur gaussianblur_0 1.810506
CrL G 4 7 1 1.810541
S /home/xilinx/Bitstreams-axi_firewall/GaussianBlur-GaussianBlur_pblock_gaussianblur_0_partial.bit 1.830299
L GaussianBlur 1 2.152731
ARR 2 8 2.177083
EQ 2 8
ARR 0 9 2.181255
EQ 0 9
ARR 2 10 2.204984
EQ 2 10
ARR 2 11 2.215768
EQ 2 11
ARR 1 12 2.227804
EQ 1 12
ARR 3 13 2.249621
EQ 3 13
ARR 4 14 2.257848
EQ 4 14
ARR 4 15 2.260979
EQ 4 15
F 0 2.303872
DQ 0 1
CrS GaussianBlur MedianBlur medianblur_0 2.303902
CrL M 0 1 0 2.303964
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-GaussianBlur_pblock_medianblur_0_partial.bit 2.304478
L MedianBlur 0 2.635734
ARR 3 16 2.642426
R 0 2.642694
EQ 0 1
EV 1 16 0 2.642881
CrL M 3 16 0 2.642908
ARR 0 17 2.660412
EQ 0 17
ARR 2 18 2.661754
EQ 2 18
L MedianBlur 0 2.669802
ARR 1 19 2.735618
EQ 1 19
ARR 4 20 2.740625
R 0 2.740920
EQ 3 16
EV 16 20 0 2.741097
CrL M 4 20 0 2.741123
L MedianBlur 0 2.749787
ARR 0 21 2.765033
EQ 0 21
ARR 3 22 2.784084
EQ 3 22
ARR 0 23 2.815754
EQ 0 23
ARR 4 24 2.828043
EQ 4 24
ARR 4 25 2.840386
EQ 4 25
ARR 2 26 2.851116
EQ 2 26
ARR 3 27 2.864126
EQ 3 27
ARR 3 28 2.865642
EQ 3 28
ARR 4 29 2.881549
EQ 4 29
F 1 3.332842
DQ 0 9
CrS MedianBlur GaussianBlur gaussianblur_0 3.332884
CrL M 0 9 1 3.332952
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-MedianBlur_pblock_gaussianblur_0_partial.bit 3.350410
L MedianBlur 1 3.646024
F 0 4.771653
DQ 0 1
CrL M 0 1 0 4.771743
L MedianBlur 0 4.799785
F 1 5.681049
DQ 0 17
CrL M 0 17 1 5.681119
L MedianBlur 1 5.709776
F 0 6.828573
DQ 0 21
CrL M 0 21 0 6.828654
L MedianBlur 0 6.879771
F 1 7.743892
DQ 0 23
CrL M 0 23 1 7.743958
L MedianBlur 1 7.749788
F 0 8.236903
DQ 1 5
CrL M 1 5 0 8.236970
L MedianBlur 0 8.289770
F 0 8.968128
DQ 1 6
CrS GaussianBlur MedianBlur medianblur_0 8.968172
CrL G 1 6 0 8.968243
S /home/xilinx/Bitstreams-axi_firewall/GaussianBlur-MedianBlur_pblock_medianblur_0_partial.bit 8.990374
L GaussianBlur 0 9.321712
F 1 9.329970
DQ 1 12
CrL M 1 12 1 9.330046
L MedianBlur 1 9.349782
F 0 10.499962
DQ 1 19
CrS GaussianBlur MedianBlur medianblur_0 10.500010
CrL M 1 19 0 10.500085
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-MedianBlur_pblock_medianblur_0_partial.bit 10.520347
L MedianBlur 0 10.851563
F 1 10.859861
DQ 2 0
CrL M 2 0 1 10.859936
L MedianBlur 1 10.869795
F 1 11.122482
DQ 2 3
CrS MedianBlur GaussianBlur gaussianblur_0 11.122517
CrL G 2 3 1 11.122578
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-GaussianBlur_pblock_gaussianblur_0_partial.bit 11.150317
L GaussianBlur 1 11.473325
F 1 12.651706
DQ 2 8
CrL G 2 8 1 12.651800
L GaussianBlur 1 12.679808
F 0 12.866834
DQ 2 10
CrL M 2 10 0 12.866903
L MedianBlur 0 12.909802
F 0 13.581019
DQ 2 11
CrL M 2 11 0 13.581098
L MedianBlur 0 13.589787
F 1 13.857924
DQ 2 18
CrS MedianBlur GaussianBlur gaussianblur_0 13.857959
CrL M 2 18 1 13.858092
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-MedianBlur_pblock_gaussianblur_0_partial.bit 13.890348
L MedianBlur 1 14.184227
F 0 15.619413
DQ 2 26
CrS GaussianBlur MedianBlur medianblur_0 15.619464
CrL G 2 26 0 15.619541
S /home/xilinx/Bitstreams-axi_firewall/GaussianBlur-MedianBlur_pblock_medianblur_0_partial.bit 15.650349
L GaussianBlur 0 15.965438
F 1 16.212886
DQ 3 4
CrL M 3 4 1 16.212967
L MedianBlur 1 16.229779
F 1 16.886158
DQ 3 13
CrL M 3 13 1 16.886249
L MedianBlur 1 16.919772
F 0 17.143627
DQ 3 16
CrS GaussianBlur MedianBlur medianblur_0 17.143662
CrL M 3 16 0 17.143723
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-MedianBlur_pblock_medianblur_0_partial.bit 17.150408
L MedianBlur 0 17.475417
F 0 18.154602
DQ 3 22
CrL M 3 22 0 18.154693
L MedianBlur 0 18.179774
F 1 18.947184
DQ 3 27
CrL M 3 27 1 18.947275
L MedianBlur 1 18.999780
F 0 19.532767
DQ 3 28
CrL M 3 28 0 19.532837
L MedianBlur 0 19.569770
F 1 20.356789
DQ 4 14
CrL M 4 14 1 20.356855
L MedianBlur 1 20.399783
F 0 20.925225
DQ 4 15
CrL M 4 15 0 20.925310
L MedianBlur 0 20.939776
F 0 22.296722
DQ 4 24
CrS GaussianBlur MedianBlur medianblur_0 22.296769
CrL G 4 24 0 22.296839
S /home/xilinx/Bitstreams-axi_firewall/GaussianBlur-MedianBlur_pblock_medianblur_0_partial.bit 22.330357
L GaussianBlur 0 22.653559
F 1 22.659910
DQ 4 25
CrL M 4 25 1 22.660532
L MedianBlur 1 22.689790
F 0 23.831801
DQ 4 29
CrL G 4 29 0 23.831892
L GaussianBlur 0 23.869772
F 1 24.703165
F 0 25.046784
END 25.046808
