ARR 2 0 4.016143
CrS GaussianBlur GaussianBlur medianblur_0 4.016198
CrL M 2 0 0 4.016284
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-GaussianBlur_pblock_medianblur_0_partial.bit 4.040524
L MedianBlur 0 4.342057
F 0 5.008829
ARR 0 1 5.407671
CrL M 0 1 0 5.407738
L MedianBlur 0 5.419847
F 0 7.419933
ARR 4 2 7.431532
CrS GaussianBlur GaussianBlur medianblur_0 7.431562
CrL G 4 2 0 7.431621
S /home/xilinx/Bitstreams-axi_firewall/GaussianBlur-GaussianBlur_pblock_medianblur_0_partial.bit 7.460407
L GaussianBlur 0 7.790462
ARR 2 3 7.852914
CrL G 2 3 1 7.852985
L GaussianBlur 1 7.889873
ARR 3 4 8.892334
R 1 8.892687
EQ 2 3
EV 3 4 1 8.892888
CrS MedianBlur MedianBlur gaussianblur_0 8.892905
CrL M 3 4 1 8.892941
S /home/xilinx/Bitstreams-axi_firewall/GaussianBlur-MedianBlur_pblock_gaussianblur_0_partial.bit 8.893682
L MedianBlur 1 9.199603
F 0 9.209841
DQ 2 3
CrL G 2 3 0 9.210470
L GaussianBlur 0 9.239863
ARR 1 5 9.266854
EQ 1 5
ARR 1 6 9.271483
EQ 1 6
ARR 4 7 9.282768
R 0 9.283069
EQ 2 3
EV 3 7 0 9.283251
CrL G 4 7 0 9.283278
L GaussianBlur 0 9.319876
F 1 9.751311
DQ 1 5
CrL M 1 5 1 9.751392
L MedianBlur 1 9.799847
ARR 2 8 10.305282
R 1 10.305601
EQ 1 5
EV 5 8 1 10.305792
CrS MedianBlur MedianBlur gaussianblur_0 10.305812
CrL G 2 8 1 10.305848
S /home/xilinx/Bitstreams-axi_firewall/GaussianBlur-GaussianBlur_pblock_gaussianblur_0_partial.bit 10.320387
L GaussianBlur 1 10.618326
F 0 10.619981
DQ 1 6
CrL G 1 6 0 10.620603
L GaussianBlur 0 10.629869
ARR 0 9 10.640929
EQ 0 9
ARR 2 10 11.659622
R 0 11.660001
EQ 1 6
EV 6 10 0 11.660197
CrS MedianBlur MedianBlur medianblur_0 11.660218
CrL M 2 10 0 11.660257
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-GaussianBlur_pblock_medianblur_0_partial.bit 11.670428
L MedianBlur 0 11.987268
F 1 11.989979
DQ 0 9
CrS MedianBlur GaussianBlur gaussianblur_0 11.990016
CrL M 0 9 1 11.990089
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-MedianBlur_pblock_gaussianblur_0_partial.bit 11.990564
L MedianBlur 1 12.296051
ARR 2 11 12.348988
R 1 12.349256
EQ 0 9
EV 9 11 1 12.349450
CrL M 2 11 1 12.349481
L MedianBlur 1 12.369862
ARR 1 12 12.408801
EQ 1 12
F 0 12.659679
DQ 0 9
CrL M 0 9 0 12.659748
L MedianBlur 0 12.659848
ARR 3 13 13.418037
R 0 13.418348
EQ 0 9
EV 9 13 0 13.418536
CrL M 3 13 0 13.418565
ARR 4 14 13.458823
R 0 13.459099
EQ 3 13
EV 13 14 0 13.459282
CrL M 4 14 0 13.459313
ARR 4 15 13.474124
R 1 13.474403
EQ 2 11
EV 11 15 1 13.474588
CrL M 4 15 1 13.474617
L MedianBlur 1 13.489878
L MedianBlur 0 13.509844
F 1 14.594575
DQ 0 9
CrL M 0 9 1 14.594666
L MedianBlur 1 14.619847
ARR 3 16 15.502078
R 1 15.502411
EQ 0 9
EV 9 16 1 15.502605
CrL M 3 16 1 15.502634
F 0 15.532675
DQ 0 9
CrL M 0 9 0 15.532742
L MedianBlur 1 15.539860
L MedianBlur 0 15.559864
ARR 0 17 15.589376
EQ 0 17
ARR 2 18 15.595145
R 0 15.595460
EQ 0 9
EV 9 18 0 15.595648
CrL M 2 18 0 15.595678
L MedianBlur 0 15.599859
F 1 16.092327
DQ 0 17
CrL M 0 17 1 16.092412
L MedianBlur 1 16.129844
F 0 17.622665
DQ 0 9
CrL M 0 9 0 17.622750
L MedianBlur 0 17.639867
F 0 17.727252
DQ 1 5
CrL M 1 5 0 17.727325
L MedianBlur 0 17.727367
F 0 17.783663
DQ 1 6
CrS GaussianBlur MedianBlur medianblur_0 17.783701
CrL G 1 6 0 17.783760
S /home/xilinx/Bitstreams-axi_firewall/GaussianBlur-MedianBlur_pblock_medianblur_0_partial.bit 17.800485
L GaussianBlur 0 18.118494
F 1 18.129845
DQ 1 12
CrL M 1 12 1 18.130472
L MedianBlur 1 18.139884
ARR 1 19 19.013021
EQ 1 19
ARR 4 20 19.037207
R 0 19.037522
EQ 1 6
EV 6 20 0 19.037711
CrS MedianBlur MedianBlur medianblur_0 19.037731
CrL M 4 20 0 19.037770
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-MedianBlur_pblock_medianblur_0_partial.bit 19.038255
L MedianBlur 0 19.373927
F 1 19.389844
DQ 1 19
CrL M 1 19 1 19.390473
L MedianBlur 1 19.429847
ARR 0 21 20.409948
EQ 0 21
ARR 3 22 20.504421
R 1 20.504759
EQ 1 19
EV 19 22 1 20.504952
CrL M 3 22 1 20.504983
L MedianBlur 1 20.539849
F 0 21.396358
DQ 0 21
CrL M 0 21 0 21.396427
L MedianBlur 0 21.409846
ARR 0 23 21.562818
EQ 0 23
ARR 4 24 21.623429
R 0 21.623750
EQ 0 21
EV 21 24 0 21.623936
CrS MedianBlur MedianBlur medianblur_0 21.623956
CrL G 4 24 0 21.623994
S /home/xilinx/Bitstreams-axi_firewall/GaussianBlur-MedianBlur_pblock_medianblur_0_partial.bit 21.650431
L GaussianBlur 0 21.968007
F 1 21.969978
DQ 0 23
CrL M 0 23 1 21.970055
L MedianBlur 1 22.009858
ARR 4 25 22.030847
R 1 22.031149
EQ 0 23
EV 23 25 1 22.031340
CrL M 4 25 1 22.031369
L MedianBlur 1 22.049858
ARR 2 26 22.084106
EQ 2 26
ARR 3 27 22.148278
EQ 3 27
ARR 3 28 22.154939
EQ 3 28
ARR 4 29 22.233654
EQ 4 29
F 0 23.147287
DQ 0 21
CrS GaussianBlur MedianBlur medianblur_0 23.147331
CrL M 0 21 0 23.147397
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-MedianBlur_pblock_medianblur_0_partial.bit 23.160456
L MedianBlur 0 23.485283
F 1 23.705437
DQ 0 23
CrL M 0 23 1 23.705531
L MedianBlur 1 23.709865
F 1 24.793411
DQ 1 6
CrS MedianBlur GaussianBlur gaussianblur_0 24.793449
CrL G 1 6 1 24.793513
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-GaussianBlur_pblock_gaussianblur_0_partial.bit 24.800471
L GaussianBlur 1 25.111795
F 0 25.111874
DQ 1 19
CrL M 1 19 0 25.111950
L MedianBlur 0 25.129853
F 0 25.837624
DQ 2 3
CrS GaussianBlur MedianBlur medianblur_0 25.837672
CrL G 2 3 0 25.837745
S /home/xilinx/Bitstreams-axi_firewall/GaussianBlur-GaussianBlur_pblock_medianblur_0_partial.bit 25.850501
L GaussianBlur 0 26.154573
F 1 26.159962
DQ 2 11
CrS MedianBlur GaussianBlur gaussianblur_0 26.159999
CrL M 2 11 1 26.160070
S /home/xilinx/Bitstreams-axi_firewall/GaussianBlur-MedianBlur_pblock_gaussianblur_0_partial.bit 26.180404
L MedianBlur 1 26.491939
F 0 27.333348
DQ 2 26
CrL G 2 26 0 27.333429
L GaussianBlur 0 27.349855
F 1 28.146964
DQ 3 13
CrL M 3 13 1 28.147056
L MedianBlur 1 28.159851
F 0 28.529338
DQ 3 27
CrS GaussianBlur MedianBlur medianblur_0 28.529375
CrL M 3 27 0 28.529439
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-MedianBlur_pblock_medianblur_0_partial.bit 28.540468
L MedianBlur 0 28.865489
F 1 29.816464
DQ 3 28
CrL M 3 28 1 29.816556
L MedianBlur 1 29.869843
F 0 30.213844
DQ 4 29
CrS GaussianBlur MedianBlur medianblur_0 30.213880
CrL G 4 29 0 30.213942
S /home/xilinx/Bitstreams-axi_firewall/GaussianBlur-MedianBlur_pblock_medianblur_0_partial.bit 30.220465
L GaussianBlur 0 30.546964
F 1 30.973952
F 0 31.722834
END 31.722866
