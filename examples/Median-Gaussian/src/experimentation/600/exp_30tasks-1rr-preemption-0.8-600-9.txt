ARR 2 0 6.059876
CrS GaussianBlur  medianblur_0 6.059931
CrL M 2 0 0 6.060010
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-GaussianBlur_pblock_medianblur_0_partial.bit 6.090449
L MedianBlur 0 6.413190
F 0 7.079963
ARR 0 1 8.485417
CrL M 0 1 0 8.485500
L MedianBlur 0 8.509759
F 0 10.509871
ARR 4 2 11.544278
CrS GaussianBlur  medianblur_0 11.544315
CrL G 4 2 0 11.544383
S /home/xilinx/Bitstreams-axi_firewall/GaussianBlur-GaussianBlur_pblock_medianblur_0_partial.bit 11.550421
L GaussianBlur 0 11.863414
ARR 2 3 11.948212
EQ 2 3
F 0 13.037141
DQ 2 3
CrL G 2 3 0 13.037220
L GaussianBlur 0 13.079757
ARR 3 4 13.969852
R 0 13.970237
EQ 2 3
EV 3 4 0 13.970450
CrS MedianBlur  medianblur_0 13.970471
CrL M 3 4 0 13.970510
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-GaussianBlur_pblock_medianblur_0_partial.bit 13.971007
L MedianBlur 0 14.290755
ARR 1 5 14.380012
EQ 1 5
ARR 1 6 14.387306
EQ 1 6
ARR 4 7 14.405240
R 0 14.405535
EQ 3 4
EV 4 7 0 14.405718
CrS MedianBlur  medianblur_0 14.405738
CrL G 4 7 0 14.405776
S /home/xilinx/Bitstreams-axi_firewall/GaussianBlur-GaussianBlur_pblock_medianblur_0_partial.bit 14.410319
L GaussianBlur 0 14.725742
ARR 2 8 15.824584
EQ 2 8
ARR 0 9 15.856345
EQ 0 9
F 0 15.899489
DQ 0 9
CrS GaussianBlur  medianblur_0 15.899526
CrL M 0 9 0 15.899591
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-GaussianBlur_pblock_medianblur_0_partial.bit 15.920335
L MedianBlur 0 16.229332
ARR 2 10 17.275477
R 0 17.275779
EQ 0 9
EV 9 10 0 17.275972
CrL M 2 10 0 17.276003
L MedianBlur 0 17.299765
ARR 2 11 17.360727
EQ 2 11
ARR 1 12 17.455541
EQ 1 12
F 0 17.966636
DQ 0 9
CrL M 0 9 0 17.966720
L MedianBlur 0 17.989760
ARR 3 13 18.529360
R 0 18.529696
EQ 0 9
EV 9 13 0 18.529924
CrL M 3 13 0 18.529956
L MedianBlur 0 18.569764
ARR 4 14 18.594251
R 0 18.594531
EQ 3 13
EV 13 14 0 18.594717
CrL M 4 14 0 18.594748
L MedianBlur 0 18.609764
ARR 4 15 18.618339
EQ 4 15
F 0 20.609975
DQ 0 9
CrL M 0 9 0 20.610064
L MedianBlur 0 20.639764
F 0 21.054006
DQ 1 5
CrL M 1 5 0 21.054074
L MedianBlur 0 21.079761
ARR 3 16 21.681413
R 0 21.681751
EQ 1 5
EV 5 16 0 21.681944
CrL M 3 16 0 21.681974
L MedianBlur 0 21.719762
F 0 22.386570
DQ 1 6
CrS GaussianBlur  medianblur_0 22.386607
CrL G 1 6 0 22.386671
S /home/xilinx/Bitstreams-axi_firewall/GaussianBlur-GaussianBlur_pblock_medianblur_0_partial.bit 22.400361
L GaussianBlur 0 22.704306
ARR 0 17 23.043730
EQ 0 17
ARR 2 18 23.052815
R 0 23.053122
EQ 1 6
EV 6 18 0 23.053310
CrS MedianBlur  medianblur_0 23.053331
CrL M 2 18 0 23.053369
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-GaussianBlur_pblock_medianblur_0_partial.bit 23.080282
L MedianBlur 0 23.392986
F 0 25.392987
DQ 0 17
CrL M 0 17 0 25.393081
L MedianBlur 0 25.429760
F 0 27.429838
DQ 1 12
CrL M 1 12 0 27.429927
L MedianBlur 0 27.459771
ARR 1 19 28.483682
EQ 1 19
ARR 4 20 28.522253
R 0 28.522598
EQ 1 12
EV 12 20 0 28.522786
CrL M 4 20 0 28.522816
L MedianBlur 0 28.559802
ARR 0 21 29.613662
EQ 0 21
F 0 30.559942
DQ 0 21
CrL M 0 21 0 30.560026
L MedianBlur 0 30.589786
ARR 3 22 30.664881
R 0 30.665199
EQ 0 21
EV 21 22 0 30.665387
CrL M 3 22 0 30.665418
L MedianBlur 0 30.689762
F 0 32.023217
DQ 0 21
CrL M 0 21 0 32.023304
L MedianBlur 0 32.023382
ARR 0 23 32.718181
EQ 0 23
ARR 4 24 32.815030
R 0 32.815348
EQ 0 21
EV 21 24 0 32.815536
CrS MedianBlur  medianblur_0 32.815556
CrL G 4 24 0 32.815594
S /home/xilinx/Bitstreams-axi_firewall/GaussianBlur-GaussianBlur_pblock_medianblur_0_partial.bit 32.820346
L GaussianBlur 0 33.109359
ARR 4 25 33.206456
EQ 4 25
ARR 2 26 33.290690
EQ 2 26
F 0 34.283123
DQ 0 23
CrS GaussianBlur  medianblur_0 34.283172
CrL M 0 23 0 34.283246
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-GaussianBlur_pblock_medianblur_0_partial.bit 34.310323
L MedianBlur 0 34.616597
ARR 3 27 34.629973
R 0 34.630239
EQ 0 23
EV 23 27 0 34.630430
CrL M 3 27 0 34.630461
ARR 3 28 34.641002
EQ 3 28
L MedianBlur 0 34.649778
ARR 4 29 35.667751
R 0 35.668093
EQ 3 27
EV 27 29 0 35.668287
CrS MedianBlur  medianblur_0 35.668307
CrL G 4 29 0 35.668347
S /home/xilinx/Bitstreams-axi_firewall/GaussianBlur-GaussianBlur_pblock_medianblur_0_partial.bit 35.690304
L GaussianBlur 0 35.993896
F 0 37.167602
DQ 0 21
CrS GaussianBlur  medianblur_0 37.167644
CrL M 0 21 0 37.167716
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-GaussianBlur_pblock_medianblur_0_partial.bit 37.190315
L MedianBlur 0 37.495919
F 0 38.829414
DQ 0 23
CrL M 0 23 0 38.829505
L MedianBlur 0 38.829555
F 0 40.149692
DQ 1 5
CrL M 1 5 0 40.149801
L MedianBlur 0 40.179761
F 0 40.244712
DQ 1 6
CrS GaussianBlur  medianblur_0 40.244746
CrL G 1 6 0 40.244809
S /home/xilinx/Bitstreams-axi_firewall/GaussianBlur-GaussianBlur_pblock_medianblur_0_partial.bit 40.260382
L GaussianBlur 0 40.581648
F 0 41.755387
DQ 1 19
CrS GaussianBlur  medianblur_0 41.755435
CrL M 1 19 0 41.755510
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-GaussianBlur_pblock_medianblur_0_partial.bit 41.790317
L MedianBlur 0 42.094087
F 0 44.094293
DQ 1 12
CrL M 1 12 0 44.094387
L MedianBlur 0 44.109763
F 0 44.380707
DQ 2 3
CrS GaussianBlur  medianblur_0 44.380742
CrL G 2 3 0 44.380804
S /home/xilinx/Bitstreams-axi_firewall/GaussianBlur-GaussianBlur_pblock_medianblur_0_partial.bit 44.390381
L GaussianBlur 0 44.700032
F 0 45.873773
DQ 2 8
CrL G 2 8 0 45.873865
L GaussianBlur 0 45.909756
F 0 47.083443
DQ 2 11
CrS GaussianBlur  medianblur_0 47.083480
CrL M 2 11 0 47.083546
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-GaussianBlur_pblock_medianblur_0_partial.bit 47.110320
L MedianBlur 0 47.424427
F 0 49.424721
DQ 2 26
CrS GaussianBlur  medianblur_0 49.424773
CrL G 2 26 0 49.424862
S /home/xilinx/Bitstreams-axi_firewall/GaussianBlur-GaussianBlur_pblock_medianblur_0_partial.bit 49.425413
L GaussianBlur 0 49.761144
F 0 50.934857
DQ 3 4
CrS GaussianBlur  medianblur_0 50.934900
CrL M 3 4 0 50.934972
S /home/xilinx/Bitstreams-axi_firewall/MedianBlur-GaussianBlur_pblock_medianblur_0_partial.bit 50.940389
L MedianBlur 0 51.280307
F 0 51.947185
DQ 3 13
CrL M 3 13 0 51.947273
L MedianBlur 0 51.959803
F 0 53.935216
DQ 3 28
CrL M 3 28 0 53.935305
L MedianBlur 0 53.949762
F 0 55.283321
DQ 3 27
CrL M 3 27 0 55.283395
L MedianBlur 0 55.319757
F 0 55.635137
DQ 4 15
CrL M 4 15 0 55.635227
L MedianBlur 0 55.679758
F 0 57.013107
DQ 4 25
CrL M 4 25 0 57.013181
L MedianBlur 0 57.039760
F 0 59.039968
END 59.039998
