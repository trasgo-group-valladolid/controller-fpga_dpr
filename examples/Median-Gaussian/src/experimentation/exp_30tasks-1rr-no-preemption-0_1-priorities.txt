ARR 2 0 0.083051
CrS GaussianBlur  medianblur_0 0.083095
CrL M 2 0 0 0.083118
S /home/xilinx/Bitstreams/MedianBlur-GaussianBlur_pblock_medianblur_0_partial.bit 0.197369
L MedianBlur 0 0.532729
ARR 0 1 0.573100
EQ 0 1
ARR 4 2 0.618460
EQ 4 2
ARR 2 3 0.629613
EQ 2 3
ARR 3 4 0.657829
EQ 3 4
ARR 1 5 0.669498
EQ 1 5
ARR 1 6 0.670962
EQ 1 6
ARR 4 7 0.673694
EQ 4 7
ARR 2 8 0.698404
EQ 2 8
ARR 0 9 0.702873
EQ 0 9
ARR 2 10 0.726934
EQ 2 10
ARR 2 11 0.738041
EQ 2 11
ARR 1 12 0.750383
EQ 1 12
ARR 3 13 0.772513
EQ 3 13
ARR 4 14 0.781039
EQ 4 14
ARR 4 15 0.784471
EQ 4 15
ARR 3 16 0.830199
EQ 3 16
ARR 0 17 0.847955
EQ 0 17
ARR 2 18 0.849576
EQ 2 18
ARR 1 19 0.923766
EQ 1 19
ARR 4 20 0.929087
EQ 4 20
ARR 0 21 0.953306
EQ 0 21
ARR 3 22 0.972666
EQ 3 22
ARR 0 23 1.004636
EQ 0 23
ARR 4 24 1.017227
EQ 4 24
ARR 4 25 1.029874
EQ 4 25
ARR 2 26 1.040883
EQ 2 26
ARR 3 27 1.054181
EQ 3 27
ARR 3 28 1.055978
EQ 3 28
ARR 4 29 1.072183
EQ 4 29
F 0 1.166347
DQ 0 1
CrL M 0 1 0 1.166366
L MedianBlur 0 1.209806
F 0 3.110088
DQ 0 9
CrL M 0 9 0 3.110119
L MedianBlur 0 3.119841
F 0 5.020551
DQ 0 17
CrL M 0 17 0 5.020581
L MedianBlur 0 5.059821
F 0 6.960089
DQ 0 21
CrL M 0 21 0 6.960120
L MedianBlur 0 7.009808
F 0 8.276630
DQ 0 23
CrL M 0 23 0 8.276655
L MedianBlur 0 8.309821
F 0 9.576752
DQ 1 5
CrL M 1 5 0 9.576784
L MedianBlur 0 9.629844
F 0 10.263429
DQ 1 6
CrS GaussianBlur  medianblur_0 10.263470
CrL G 1 6 0 10.263494
S /home/xilinx/Bitstreams/GaussianBlur-GaussianBlur_pblock_medianblur_0_partial.bit 10.355743
L GaussianBlur 0 10.690793
F 0 11.831584
DQ 1 12
CrS GaussianBlur  medianblur_0 11.831622
CrL M 1 12 0 11.831645
S /home/xilinx/Bitstreams/MedianBlur-GaussianBlur_pblock_medianblur_0_partial.bit 11.937110
L MedianBlur 0 12.281794
F 0 13.548736
DQ 1 19
CrL M 1 19 0 13.548772
L MedianBlur 0 13.579809
F 0 15.480290
DQ 2 3
CrS GaussianBlur  medianblur_0 15.480332
CrL G 2 3 0 15.480356
S /home/xilinx/Bitstreams/GaussianBlur-GaussianBlur_pblock_medianblur_0_partial.bit 15.567675
L GaussianBlur 0 15.903649
F 0 17.044465
DQ 2 8
CrL G 2 8 0 17.044502
L GaussianBlur 0 17.059815
F 0 18.200600
DQ 2 10
CrS GaussianBlur  medianblur_0 18.200635
CrL M 2 10 0 18.200656
S /home/xilinx/Bitstreams/MedianBlur-GaussianBlur_pblock_medianblur_0_partial.bit 18.317040
L MedianBlur 0 18.627423
F 0 19.260984
DQ 2 11
CrL M 2 11 0 19.261017
L MedianBlur 0 19.279827
F 0 21.180206
DQ 2 18
CrL M 2 18 0 21.180243
L MedianBlur 0 21.189828
F 0 23.090202
DQ 2 26
CrS GaussianBlur  medianblur_0 23.090249
CrL G 2 26 0 23.090290
S /home/xilinx/Bitstreams/GaussianBlur-GaussianBlur_pblock_medianblur_0_partial.bit 23.207061
L GaussianBlur 0 23.560269
F 0 24.701083
DQ 3 4
CrS GaussianBlur  medianblur_0 24.701127
CrL M 3 4 0 24.701152
S /home/xilinx/Bitstreams/MedianBlur-GaussianBlur_pblock_medianblur_0_partial.bit 24.807044
L MedianBlur 0 25.128684
F 0 25.762248
DQ 3 13
CrL M 3 13 0 25.762281
L MedianBlur 0 25.799809
F 0 27.700102
DQ 3 16
CrL M 3 16 0 27.700142
L MedianBlur 0 27.749820
F 0 28.383400
DQ 3 22
CrL M 3 22 0 28.383433
L MedianBlur 0 28.430487
F 0 29.697358
DQ 3 27
CrL M 3 27 0 29.697387
L MedianBlur 0 29.749818
F 0 31.016828
DQ 3 28
CrL M 3 28 0 31.016868
L MedianBlur 0 31.059847
F 0 32.326643
DQ 4 2
CrS GaussianBlur  medianblur_0 32.326684
CrL G 4 2 0 32.326708
S /home/xilinx/Bitstreams/GaussianBlur-GaussianBlur_pblock_medianblur_0_partial.bit 32.427226
L GaussianBlur 0 32.747177
F 0 33.887967
DQ 4 7
CrL G 4 7 0 33.888000
L GaussianBlur 0 33.899814
F 0 35.040631
DQ 4 14
CrS GaussianBlur  medianblur_0 35.040675
CrL M 4 14 0 35.040700
S /home/xilinx/Bitstreams/MedianBlur-GaussianBlur_pblock_medianblur_0_partial.bit 35.147184
L MedianBlur 0 35.481976
F 0 37.382262
DQ 4 15
CrL M 4 15 0 37.382300
L MedianBlur 0 37.439819
F 0 38.706724
DQ 4 20
CrL M 4 20 0 38.706764
L MedianBlur 0 38.739845
F 0 40.640411
DQ 4 24
CrS GaussianBlur  medianblur_0 40.640455
CrL G 4 24 0 40.640480
S /home/xilinx/Bitstreams/GaussianBlur-GaussianBlur_pblock_medianblur_0_partial.bit 40.757108
L GaussianBlur 0 41.068037
F 0 42.208837
DQ 4 25
CrS GaussianBlur  medianblur_0 42.208876
CrL M 4 25 0 42.208899
S /home/xilinx/Bitstreams/MedianBlur-GaussianBlur_pblock_medianblur_0_partial.bit 42.307308
L MedianBlur 0 42.619415
F 0 44.519884
DQ 4 29
CrS GaussianBlur  medianblur_0 44.519929
CrL G 4 29 0 44.519953
S /home/xilinx/Bitstreams/GaussianBlur-GaussianBlur_pblock_medianblur_0_partial.bit 44.617209
L GaussianBlur 0 44.960801
F 0 46.101647
END 46.101679
