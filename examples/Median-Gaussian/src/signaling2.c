#include <signal.h>
#include <unistd.h>
#include <stdio.h>
#include <time.h>
#include <errno.h>
#include <sys/time.h>

#define LOOP_LIMIT  1E6

volatile int sigcount=0;

void catcher( int sig ) {
    printf( "Signal catcher called for signal %d\n", sig );
    sigcount = 1;
}

int main( int argc, char *argv[] )  {
    struct timeval it_interval = {
        .tv_sec = 0,
        .tv_usec = 0
    };
    struct timeval it_value = {
        .tv_sec = 1,
        .tv_usec = 0
    };

    struct itimerval itimerinterval = {
        .it_interval = it_interval,
        .it_value = it_value
    };

    setitimer(ITIMER_REAL, &itimerinterval, NULL);

    struct sigaction sact;
    volatile double count;
    time_t t;

    sigemptyset( &sact.sa_mask );
    sact.sa_flags = 0;
    sact.sa_handler = catcher;
    sigaction( SIGALRM, &sact, NULL );

    //alarm(1);  /* timer will pop in five seconds */

    time( &t );
    printf( "Before loop, time is %s", ctime(&t) );

    //for( count=0; ((count<LOOP_LIMIT) && (sigcount==0)); count++ );
    for( count=0; (sigcount==0); count++ );

    time( &t );
    printf( "After loop, time is %s\n", ctime(&t) );

    if( sigcount == 0 )
        printf( "The signal catcher never gained control\n" );
    else
        printf( "The signal catcher gained control\n" );

    printf( "The value of count is %.0f\n", count );

    return( 0 );
}
