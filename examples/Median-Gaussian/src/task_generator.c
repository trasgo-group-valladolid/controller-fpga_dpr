#include "scheduler.h"

CTRL_HOST_TASK_PROTO( Init_Images, 4,
        OUT, HitTile_int, input_image,
        OUT, HitTile_int, output_image,
        INVAL, int, H,
        INVAL, int, W
);

CTRL_HOST_TASK( Init_Tiles, HitTile_int in_matrix, HitTile_int out_matrix, HitTile_int mat ) {
    FILE * image;
    //image = fopen("/home/xilinx/controller-DPR/controllers/examples/Median-Gaussian/src/Saltpepper.pgm", "r");
    image = fopen(image_path, "r");
    //image = fread("Saltpepper.pgm", "r");
    char * line = NULL;
    size_t len = 0;
    ssize_t size = getline(&line, &len, image);

    int file_H, file_W;
    fscanf(image, "%d", &file_H);
    fscanf(image, "%d", &file_W);

    getline(&line, &len, image);

    int * data = malloc(H_NROW*H_NCOL * sizeof(int));
    //int * output = malloc(H_NROW*H_NCOL * sizeof(int));

    for(int i = 0; i < H_NROW; i++) {
        for(int j = 0; j < H_NCOL; j++) {
                fscanf(image, "%d", &data[i*W+j]);
        }
    } 

    for(int i = 0; i < H_NROW; i++) {
        for(int j = 0; j < H_NCOL; j++) {
            hit(in_matrix, i, j) = data[i * W + j];
            hit(out_matrix, i, j) = 0;
            //hit(mat, i, j) = 0;
        }
    }
    printf("Finishing Init_Tiles\n");
}

void task_generator(PCtrl ctrl, int n_tasks, int total_time, int enabled_priorities, int H, int W) {
    int H_NROW = H+2;
    int H_NCOL = W+2;

    for(int priority = 0; priority < n_priorities(enabled_priorities); priority++)
        task_queue_head[priority] = 0; 

    //struct context contexts[n_tasks];
    input_images = (HitTile_int*)malloc(n_tasks * sizeof(HitTile_int));
    output_images = (HitTile_int*)malloc(n_tasks * sizeof(HitTile_int));
    k_data = (kernel_data*)malloc(n_tasks * sizeof(kernel_data));

    int priority;

    arrival_times = (double *)malloc(n_tasks * sizeof(double));

    for(int task = 0; task < n_tasks; task++) {
        k_data[task].task_type = rand() % 4; 
        k_data[task].arrival_time = (double)rand() / (double)RAND_MAX * (double)total_time * 60;
        //arrival_times[task] = (double)rand() / (double)RAND_MAX * (double)total_time * 60;

        if(k_data[task].task_type <= 2) // MEDIAN
            k_data[task].iters = k_data[task].task_type+1;

        input_images[task] = Ctrl_DomainAlloc(ctrl, int, hitShapeSize(H_NROW, H_NCOL));
        output_images[task] = Ctrl_DomainAlloc(ctrl, int, hitShapeSize(H_NROW, H_NCOL));

        //initialise_image(input_images[task], H_NROW, H_NCOL, 0);
        //initialise_image(output_images[task], H_NROW, H_NCOL, 0);
        Ctrl_HostTask(ctrl, Init_Images, input_images[task], output_images[task], H_NROW, H_NCOL);

        if(enabled_priorities)
            priority = rand() % 5; 
        else
            priority = 0;
        
        int * queue_head = &task_queue_head[priority];
        task_queue[priority][*queue_head].id = task;
        task_queue[priority][*queue_head].k_data = k_data[task];
        task_queue[priority][*queue_head].input_image = &input_images[task];
        task_queue[priority][*queue_head].output_image = &output_images[task];
        task_queue_head[priority]++;
    }    
    Ctrl_GlobalSync(ctrl);
   
    double min_time;
    int j_min;
    // Sort queues by arrival time
    for(int priority = 0; priority < n_priorities(enabled_priorities); priority++) {
        for(int i = 0; i < task_queue_head[priority]; i++) {
            //min_time = task_queue[priority][i].arrival_time;
            j_min = i; 
            for(int j = i+1; j < task_queue_head[priority]; j++) {
                if(task_queue[priority][j].k_data.arrival_time < task_queue[priority][j_min].k_data.arrival_time)
                    j_min = j; 
            }

            HitTile_int * aux_input_image = task_queue[priority][i].input_image;
            HitTile_int * aux_output_image = task_queue[priority][i].output_image;
            kernel_data aux_k_data = task_queue[priority][i].k_data;

            task_queue[priority][i].input_image = task_queue[priority][j_min].input_image;
            task_queue[priority][i].output_image = task_queue[priority][j_min].output_image;
            task_queue[priority][i].k_data = task_queue[priority][j_min].k_data;

            task_queue[priority][j_min].input_image = aux_input_image;
            task_queue[priority][j_min].output_image = aux_output_image;
            task_queue[priority][j_min].k_data = aux_k_data;
        }

    }
    

    // Test sorted
    for(int priority = 0; priority < n_priorities(enabled_priorities); priority++) {
        printf("PRIORITY %d\n", priority);
        for(int task = 0; task < task_queue_head[priority]; task++) {
            printf("%lf\n", task_queue[priority][task].k_data.arrival_time);
        }
    }    

    for(int priority = 0; priority < N_PRIORITIES; priority++) {
        task_queue_tail[priority] = task_queue_head[priority];
        task_queue_head[priority] = 0;
    }
}
