/*
 * <license>
 * 
 * Controller v2.1
 * 
 * This software is provided to enhance knowledge and encourage progress in the scientific
 * community. It should be used only for research and educational purposes. Any reproduction
 * or use for commercial purpose, public redistribution, in source or binary forms, with or 
 * without modifications, is NOT ALLOWED without the previous authorization of the copyright 
 * holder. The origin of this software must not be misrepresented; you must not claim that you
 * wrote the original software. If you use this software for any purpose (e.g. publication),
 * a reference to the software package and the authors must be included.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDER AND CONTRIBUTORS "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 * THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * Copyright (c) 2007-2020, Trasgo Group, Universidad de Valladolid.
 * All rights reserved.
 * 
 * More information on http://trasgo.infor.uva.es/
 * 
 * </license>
*/
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <omp.h>
#include <assert.h>
#include "Ctrl.h"

#define SEED 6834723
#define EPSILON 0.0001

#define HCOL (1024+2)

double main_clock;
double exec_clock;

Ctrl_NewType(int);

CTRL_KERNEL_CHAR(Jacobi, MANUAL, LOCAL_SIZE_0, LOCAL_SIZE_0);

CTRL_HOST_TASK( Init_Tiles, HitTile_int in_matrix, HitTile_int out_matrix) {
    printf("Hello from HOSTTASK\n");

    for(int i = 0; i < HCOL; i++) {
        for(int j = 0; j < HCOL; j++) {
            hit(in_matrix, i, j) = i * HCOL + j;
            hit(out_matrix, i, j) = 0;
        }
    }
}

CTRL_HOST_TASK_PROTO( Init_Tiles, 2,
	OUT, HitTile_int, in_matrix,
	OUT, HitTile_int, out_matrix
);


CTRL_KERNEL_PROTO( Jacobi,
        1, PYNQ, DEFAULT, 3,
        IN, HitTile_int, in_matrix,
        OUT, HitTile_int, out_matrix,
        INVAL, int, iters);


int main(int argc, char *argv[]) {
    int SIZE = 1024;
    int iters = 5;

	main_clock = omp_get_wtime();


	Ctrl_Thread threads;
	Ctrl_ThreadInit(threads, SIZE, SIZE);

	Ctrl_Thread group;
	Ctrl_ThreadInit(group, LOCAL_SIZE_0, LOCAL_SIZE_1);
    
	__ctrl_block__(1,1)
	{
        Ctrl_Policy policy = 0; // Synchronous for now
        int device = 0;
		PCtrl ctrl = Ctrl_Create(CTRL_TYPE_PYNQ, policy, device);
		
        HitTile_int in_matrix;
        HitTile_int out_matrix;

        in_matrix = Ctrl_DomainAlloc(ctrl, int, hitShapeSize(HCOL, HCOL) );
        out_matrix = Ctrl_DomainAlloc(ctrl, int, hitShapeSize(HCOL, HCOL) );

        Ctrl_HostTask(ctrl, Init_Tiles, in_matrix, out_matrix);

        Ctrl_GlobalSync(ctrl);
        exec_clock = omp_get_wtime();

        int rr = 0;

		Ctrl_Launch(ctrl, Jacobi, threads, group, rr, in_matrix, out_matrix, iters);

		Ctrl_GlobalSync(ctrl);
        exec_clock = omp_get_wtime() - exec_clock;


		Ctrl_Free(ctrl, in_matrix, out_matrix);
        Ctrl_Destroy(ctrl);
    }

	main_clock = omp_get_wtime() - main_clock;

	#ifdef _CTRL_EXAMPLES_EXP_MODE_
		printf("%lf, %lf\n", main_clock, exec_clock);
		fflush(stdout);
	#else
		printf("\n ----------------------- TIME ----------------------- \n\n");
		printf(" Clock main: %lf\n", main_clock);
		printf(" Clock exec: %lf\n", exec_clock);
		printf("\n ---------------------------------------------------- \n");
	#endif

	return 0;
}
