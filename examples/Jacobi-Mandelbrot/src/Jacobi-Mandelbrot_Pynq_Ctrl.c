/*
 * <license>
 * 
 * Controller v2.1
 * 
 * This software is provided to enhance knowledge and encourage progress in the scientific
 * community. It should be used only for research and educational purposes. Any reproduction
 * or use for commercial purpose, public redistribution, in source or binary forms, with or 
 * without modifications, is NOT ALLOWED without the previous authorization of the copyright 
 * holder. The origin of this software must not be misrepresented; you must not claim that you
 * wrote the original software. If you use this software for any purpose (e.g. publication),
 * a reference to the software package and the authors must be included.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDER AND CONTRIBUTORS "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 * THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * Copyright (c) 2007-2020, Trasgo Group, Universidad de Valladolid.
 * All rights reserved.
 * 
 * More information on http://trasgo.infor.uva.es/
 * 
 * </license>
*/
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <omp.h>
#include <assert.h>
#include "Ctrl.h"

#define SEED 6834723
#define EPSILON 0.0001

#define HCOL (1024+2)

double main_clock;
double exec_clock;

#include "mandelbrot.h"
#include "jacobi.h"


CTRL_KERNEL_CHAR(Jacobi, MANUAL, LOCAL_SIZE_0, LOCAL_SIZE_0);
CTRL_KERNEL_CHAR(Mandelbrot, MANUAL, 8, 8);

CTRL_HOST_TASK( Init_Tiles, HitTile_int in_matrix, HitTile_int out_matrix, HitTile_int mat ) {
    for(int i = 0; i < HCOL; i++) {
        for(int j = 0; j < HCOL; j++) {
            hit(in_matrix, i, j) = i * HCOL + j;
            hit(out_matrix, i, j) = 0;
            hit(mat, i, j) = 0;
        }
    }
}


CTRL_HOST_TASK_PROTO( Init_Tiles, 3,
	OUT, HitTile_int, in_matrix,
	OUT, HitTile_int, out_matrix,
	OUT, HitTile_int, mat
);

CTRL_KERNEL_PROTO( Mandelbrot,
        1, PYNQ, DEFAULT, 6,
        OUT, HitTile_int, mat,
        INVAL, int, threshold,
        INVAL, float, x1,
        INVAL, float, x2,
        INVAL, float, y1,
        INVAL, float, y2);

CTRL_KERNEL_PROTO( Jacobi,
        1, PYNQ, DEFAULT, 3,
        IN, HitTile_int, in_matrix,
        OUT, HitTile_int, out_matrix,
        INVAL, int, iters);

int main(int argc, char *argv[]) {
    int SIZE = 1024;
    int iters = 5;
    int threshold = 3000;
    float x1 = -1.75;
    float x2 = -1.50;
    float y1 = -0.25;
    float y2 = 0.25;

	main_clock = omp_get_wtime();


	Ctrl_Thread threads;
	Ctrl_ThreadInit(threads, SIZE, SIZE);

	Ctrl_Thread group;
	Ctrl_ThreadInit(group, LOCAL_SIZE_0, LOCAL_SIZE_1);

	__ctrl_block__(1,1)
	{
        Ctrl_Policy policy = 1;
        int device = 0;
		PCtrl ctrl = Ctrl_Create(CTRL_TYPE_PYNQ, policy, device);

        HitTile_int in_matrix;
        HitTile_int out_matrix;
        HitTile_int mat;

        in_matrix = Ctrl_DomainAlloc(ctrl, int, hitShapeSize(HCOL, HCOL) );
        out_matrix = Ctrl_DomainAlloc(ctrl, int, hitShapeSize(HCOL, HCOL) );
        mat = Ctrl_DomainAlloc(ctrl, int, hitShapeSize(HCOL, HCOL) );

        Ctrl_HostTask(ctrl, Init_Tiles, in_matrix, out_matrix, mat);

        Ctrl_GlobalSync(ctrl);
        exec_clock = omp_get_wtime();

        int rr_jacobi = 0;
        int rr_mandelbrot = 1;

        int reset_jacobi = 0;
        int reset_mandelbrot = 0;

        //Ctrl_Task task = Ctrl_KernelTaskCreate_Swap(0, "/home/xilinx/controller-DPR/scheduler/Bitstreams/mandelbrot-mandelbrot_pblock_jacobi_0_partial.bit");
        //Ctrl_LaunchKernel(ctrl, task);


        //task = Ctrl_KernelTaskCreate_Swap(0, "/home/xilinx/controller-DPR/scheduler/Bitstreams/jacobi-mandelbrot_pblock_jacobi_0_partial.bit");
        //Ctrl_LaunchKernel(ctrl, task);

        //// Proof of migrations CPU <-> FPGA. Execution plan:
        //// (1) Mandelbrot (FPGA) -> Mandelbrot (CPU)
        //// (2) Jacobi (FPGA) -> Jacobi (CPU) -> Jacobi (FPGA)
        //printf("Launching mandelbrot FPGA\n");
		//Ctrl_Launch(ctrl, Mandelbrot, threads, group, rr_mandelbrot, mat, threshold, x1, x2, y1, y2);
        //sleep(2);
        //reset(1);
		//Ctrl_HostTask(ctrl, Mandelbrot, rr_mandelbrot, reset_mandelbrot, mat, threshold, x1, x2, y1, y2);
		Ctrl_Launch(ctrl, Jacobi, threads, group, rr_jacobi, in_matrix, out_matrix, iters);
        //sleep(3);
        //reset(0);
		//Ctrl_HostTask(ctrl, Jacobi, rr_jacobi, reset_jacobi, in_matrix, out_matrix, iters);
        //wait_finish();
        for(int rr = 0; rr < 3; rr++) printf("interr: %d\n", generated_interrupts[rr]);
        //sleep(3);
        //reset_jacobi = 1;
        //printf("RESET JACOBI CPU\n");
        //while(reset_jacobi != -1) {}
        //printf("Second half\n");
        //reset_jacobi = 0;
		//Ctrl_Launch(ctrl, Jacobi, threads, group, rr_jacobi, in_matrix, out_matrix, iters);

		Ctrl_GlobalSync(ctrl);
        exec_clock = omp_get_wtime() - exec_clock;

        printf("jacobi sum: %d\n", RR[0].return_var);
        printf("mandelbrot sum: %d\n", RR[1].return_var);


		Ctrl_Free(ctrl, in_matrix, out_matrix);
        Ctrl_Destroy(ctrl);
    }

	main_clock = omp_get_wtime() - main_clock;

	#ifdef _CTRL_EXAMPLES_EXP_MODE_
		printf("%lf, %lf\n", main_clock, exec_clock);
		fflush(stdout);
	#else
		printf("\n ----------------------- TIME ----------------------- \n\n");
		printf(" Clock main: %lf\n", main_clock);
		printf(" Clock exec: %lf\n", exec_clock);
		printf("\n ---------------------------------------------------- \n");
	#endif

	return 0;
}
