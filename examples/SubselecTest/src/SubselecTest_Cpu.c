#include <stdio.h>
#include <stdlib.h>
#include <omp.h>

#include "Ctrl.h"

//#define STRIDE

Ctrl_NewType(long);

CTRL_KERNEL_CHAR(some_kernel, MANUAL, 16, 16);
CTRL_KERNEL_CHAR(big_kernel, MANUAL, 16, 16);

CTRL_KERNEL(some_kernel, CPU, DEFAULT, KHitTile_long tile,
{
	hit(tile, thread_id_x, thread_id_y) *= hit(tile, thread_id_x, thread_id_y);
	//hitStrided(tile, thread_id_x, thread_id_y) *= hitStrided(tile, thread_id_x, thread_id_y);
});

CTRL_KERNEL(big_kernel, CPU, DEFAULT, KHitTile_long tile1, KHitTile_long tile2, KHitTile_long tile3, KHitTile_long tile4, KHitTile_long tile5, KHitTile_long tile6, KHitTile_long dummy1, KHitTile_long dummy2, KHitTile_long dummy3, KHitTile_long dummy4, KHitTile_long dummy5, KHitTile_long dummy6,
{
	hit(tile1, thread_id_x, thread_id_y) *= hit(tile1, thread_id_x, thread_id_y);
	hit(tile2, thread_id_y, thread_id_x) = -hit(tile2, thread_id_y, thread_id_x);
	hit(tile3, thread_id_x, thread_id_y) = 0;
	hit(tile4, thread_id_y, thread_id_x) %= 2;
	hit(tile5, thread_id_x, thread_id_y) = hit(tile6, thread_id_y, thread_id_x);

});

CTRL_HOST_TASK(init_tile, HitTile_long tile)
{
	for (int i = 0; i < hit_tileDimCard(tile, 0); i++)
	{
		for (int j = 0; j < hit_tileDimCard(tile, 1); j++)
		{
			hit(tile, i, j) = i * hit_tileDimCard(tile, 1) + j;
		}
	}
}

CTRL_HOST_TASK(print_matrix, HitTile_long tile)
{
	for (int i = 0; i < hit_tileDimCard(tile, 0); i++)
	{
		for (int j = 0; j < hit_tileDimCard(tile, 1); j++)
		{
			printf("%5ld ", hit(tile, i, j));
		}
		printf("\n");
	}
	printf("\n");
	fflush(stdout);
}

CTRL_KERNEL_PROTO(some_kernel,
	1, CPU, DEFAULT, 1,
	IO, HitTile_long, tile
);

CTRL_KERNEL_PROTO(big_kernel,
	1, CPU, DEFAULT, 12,
	IO, HitTile_long, tile1,
	IO, HitTile_long, tile2,
	OUT, HitTile_long, tile3,
	IO, HitTile_long, tile4,
	OUT, HitTile_long, tile5,
	IN, HitTile_long, tile6,
	IO, HitTile_long, dummy1,
	IO, HitTile_long, dummy2,
	IO, HitTile_long, dummy3,
	IO, HitTile_long, dummy4,
	IO, HitTile_long, dummy5,
	IO, HitTile_long, dummy6
);

CTRL_HOST_TASK_PROTO(init_tile, 1,
	OUT, HitTile_long, tile
);

CTRL_HOST_TASK_PROTO(print_matrix, 1,
	IN, HitTile_long, tile
);

int main(int argc, char *argv[])
{
	if (argc != 2)
	{
		fprintf(stderr, "\nUsage: %s <size>\n", argv[0]);
		exit(EXIT_FAILURE);
	}

	int SIZE = atoi(argv[1]);

	Ctrl_Thread subselec_whole_threads, subselec_rows_threads, subselec_cols_threads, subselec_submatrix_threads, subselec_phantom_matrix_threads1, subselec_phantom_matrix_threads2;
	Ctrl_ThreadInit(subselec_whole_threads, SIZE, SIZE);
	Ctrl_ThreadInit(subselec_rows_threads, SIZE / 4, SIZE);
	Ctrl_ThreadInit(subselec_cols_threads, SIZE, SIZE / 4);
	Ctrl_ThreadInit(subselec_submatrix_threads, SIZE / 2, SIZE / 2);

	#ifdef STRIDE
	Ctrl_Thread subselec_stride_threads, subselec_corners_threads, subselec_null_threads;
	Ctrl_ThreadInit(subselec_stride_threads, SIZE / 2, SIZE / 2);
	Ctrl_ThreadInit(subselec_corners_threads, 2, 2);
	Ctrl_ThreadInit(subselec_null_threads, hit_shapeSigCard(HIT_SHAPE_NULL, 0), hit_shapeSigCard(HIT_SHAPE_NULL, 1));
	#endif // STRIDE

	Ctrl_ThreadInit(subselec_phantom_matrix_threads1, SIZE / 2, SIZE);
	Ctrl_ThreadInit(subselec_phantom_matrix_threads2, SIZE, SIZE / 2);

	__ctrl_block__(1, 1)
	{
		int a_one = 1;
		PCtrl ctrl;
		ctrl = Ctrl_Create(CTRL_TYPE_CPU, CTRL_POLICY_ASYNC, 8, &a_one, 1, 1);
		Ctrl_SetDependanceMode(ctrl, CTRL_MODE_EXPLICIT);

		HitTile_long matrix, subselec_whole, subselec_rows, subselec_cols, subselec_submatrix, phantom_matrix, subselec_phantom_matrix1, subselec_phantom_matrix2;


		HitShape matrix_shape = hitShapeSize(SIZE, SIZE);
		HitShape subselec_whole_shape = HIT_SHAPE_WHOLE;
		HitShape subselec_rows_shape = hitShapeSize(SIZE / 4, SIZE);
		HitShape subselec_cols_shape = hitShapeSize(SIZE, SIZE / 4);
		HitShape subselec_submatrix_shape = hitShape((SIZE / 4, 3 * SIZE / 4 - 1, 1), (SIZE / 4, 3 * SIZE / 4 - 1, 1));

		#ifdef STRIDE
		HitShape subselec_stride_shape = hitShape((0, SIZE - 1, 2), (0, SIZE - 1, 2));
		HitShape subselec_corners_shape = hitShape((0, SIZE - 1, SIZE - 1), (0, SIZE - 1, SIZE - 1));
		HitShape subselec_null_shape = HIT_SHAPE_NULL;
		#endif // STRIDE

		HitShape subselec_phantom_matrix1_shape = hitShapeSize(SIZE / 2, SIZE);
		HitShape subselec_phantom_matrix2_shape = hitShapeSize(SIZE, SIZE / 2);

		matrix = Ctrl_Domain(ctrl, long, matrix_shape);
		Ctrl_Alloc(ctrl, matrix, CTRL_MEM_ALLOC_BOTH);

		subselec_whole = Ctrl_Select(ctrl, long,  matrix, subselec_whole_shape, CTRL_SELECT_DEFAULT);
		subselec_rows = Ctrl_Select(ctrl, long, matrix, subselec_rows_shape, CTRL_SELECT_DEFAULT);
		subselec_cols = Ctrl_Select(ctrl, long, matrix, subselec_cols_shape, CTRL_SELECT_DEFAULT);
		subselec_submatrix = Ctrl_Select(ctrl, long, matrix, subselec_submatrix_shape, CTRL_SELECT_DEFAULT);

		#ifdef STRIDE
		HitTile_long subselec_stride, subselec_corners, subselec_null;
		subselec_stride = Ctrl_Select(ctrl, long, matrix, subselec_stride_shape, CTRL_SELECT_DEFAULT);
		subselec_corners = Ctrl_Select(ctrl, long, matrix, subselec_corners_shape, CTRL_SELECT_DEFAULT);
		subselec_null = Ctrl_Select(ctrl, long, matrix, subselec_null_shape, CTRL_SELECT_DEFAULT);
		#endif // STRIDE

		phantom_matrix = Ctrl_Domain(ctrl, long, matrix_shape);

		subselec_phantom_matrix1 = Ctrl_Select(ctrl, long, phantom_matrix, subselec_phantom_matrix1_shape, CTRL_SELECT_DEFAULT);
		subselec_phantom_matrix2 = Ctrl_Select(ctrl, long, phantom_matrix, subselec_phantom_matrix2_shape, CTRL_SELECT_DEFAULT);

		Ctrl_Alloc(ctrl, subselec_phantom_matrix1, CTRL_MEM_ALLOC_BOTH);
		Ctrl_Alloc(ctrl, subselec_phantom_matrix2, CTRL_MEM_ALLOC_BOTH);

		Ctrl_HostTask(ctrl, init_tile, matrix);

		printf("=== ORIGINAL MATRIX ===\n\n");
		Ctrl_HostTask(ctrl, print_matrix, matrix);
		Ctrl_GlobalSync(ctrl);



		Ctrl_MoveTo(ctrl, matrix);
		Ctrl_WaitTile(ctrl, matrix);

		Ctrl_Launch(ctrl, some_kernel, subselec_whole_threads, CTRL_THREAD_NULL, subselec_whole);
		Ctrl_MoveFrom(ctrl, subselec_whole);
		Ctrl_WaitTile(ctrl, subselec_whole);

		printf("=== WHOLE SUBSELECTION MODIFIED ===\n\n");
		Ctrl_HostTask(ctrl, print_matrix, matrix);
		Ctrl_GlobalSync(ctrl);



		Ctrl_HostTask(ctrl, init_tile, matrix);
		Ctrl_MoveTo(ctrl, matrix);
		Ctrl_WaitTile(ctrl, matrix);

		Ctrl_Launch(ctrl, some_kernel, subselec_rows_threads, CTRL_THREAD_NULL, subselec_rows);
		Ctrl_MoveFrom(ctrl, subselec_rows);
		Ctrl_WaitTile(ctrl, subselec_rows);

		printf("=== ROWS SUBSELECTION MODIFIED ===\n\n");
		Ctrl_HostTask(ctrl, print_matrix, matrix);
		Ctrl_GlobalSync(ctrl);



		Ctrl_HostTask(ctrl, init_tile, matrix);
		Ctrl_MoveTo(ctrl, matrix);
		Ctrl_WaitTile(ctrl, matrix);

		Ctrl_Launch(ctrl, some_kernel, subselec_cols_threads, CTRL_THREAD_NULL, subselec_cols);
		Ctrl_MoveFrom(ctrl, subselec_cols);
		Ctrl_WaitTile(ctrl, subselec_cols);

		printf("=== COLUMNS SUBSELECTION MODIFIED ===\n\n");
		Ctrl_HostTask(ctrl, print_matrix, matrix);
		Ctrl_GlobalSync(ctrl);



		Ctrl_HostTask(ctrl, init_tile, matrix);
		Ctrl_MoveTo(ctrl, matrix);
		Ctrl_WaitTile(ctrl, matrix);

		Ctrl_Launch(ctrl, some_kernel, subselec_submatrix_threads, CTRL_THREAD_NULL, subselec_submatrix);
		Ctrl_MoveFrom(ctrl, subselec_submatrix);
		Ctrl_WaitTile(ctrl, subselec_submatrix);

		printf("=== SUBMATRIX SUBSELECTION MODIFIED ===\n\n");
		Ctrl_HostTask(ctrl, print_matrix, matrix);
		Ctrl_GlobalSync(ctrl);
		

		#ifdef STRIDE
		Ctrl_HostTask(ctrl, init_tile, matrix);
		Ctrl_MoveTo(ctrl, matrix);
		Ctrl_WaitTile(ctrl, matrix);

		Ctrl_Launch(ctrl, some_kernel, subselec_stride_threads, CTRL_THREAD_NULL, subselec_stride);
		Ctrl_MoveFrom(ctrl, subselec_stride);
		Ctrl_WaitTile(ctrl, subselec_stride);

		printf("=== STRIDED SUBSELECTION MODIFIED ===\n\n");
		Ctrl_HostTask(ctrl, print_matrix, matrix);
		Ctrl_GlobalSync(ctrl);



		Ctrl_HostTask(ctrl, init_tile, matrix);
		Ctrl_MoveTo(ctrl, matrix);
		Ctrl_WaitTile(ctrl, matrix);

		Ctrl_Launch(ctrl, some_kernel, subselec_corners_threads, CTRL_THREAD_NULL, subselec_corners);
		Ctrl_MoveFrom(ctrl, subselec_corners);
		Ctrl_WaitTile(ctrl, subselec_corners);

		printf("=== CORNERS SUBSELECTION MODIFIED ===\n\n");
		Ctrl_HostTask(ctrl, print_matrix, matrix);
		Ctrl_GlobalSync(ctrl);



		Ctrl_HostTask(ctrl, init_tile, matrix);
		Ctrl_MoveTo(ctrl, matrix);
		Ctrl_WaitTile(ctrl, matrix);

		Ctrl_Launch(ctrl, some_kernel, subselec_null_threads, CTRL_THREAD_NULL, subselec_null);
		Ctrl_MoveFrom(ctrl, subselec_null);
		Ctrl_WaitTile(ctrl, subselec_null);

		printf("=== NULL SUBSELECTION MODIFIED ===\n\n");
		Ctrl_HostTask(ctrl, print_matrix, matrix);
		Ctrl_GlobalSync(ctrl);
	
		Ctrl_Free(ctrl, subselec_stride, subselec_corners, subselec_null);
		#endif // STRIDE
		

		Ctrl_HostTask(ctrl, init_tile, subselec_phantom_matrix1);
		Ctrl_HostTask(ctrl, init_tile, subselec_phantom_matrix2);

		Ctrl_MoveTo(ctrl, subselec_phantom_matrix1);
		Ctrl_Launch(ctrl, some_kernel, subselec_phantom_matrix_threads1, CTRL_THREAD_NULL, subselec_phantom_matrix1);
		Ctrl_MoveFrom(ctrl, subselec_phantom_matrix1);
		Ctrl_MoveTo(ctrl, subselec_phantom_matrix2);
		Ctrl_Launch(ctrl, some_kernel, subselec_phantom_matrix_threads2, CTRL_THREAD_NULL, subselec_phantom_matrix2);
		Ctrl_MoveFrom(ctrl, subselec_phantom_matrix2);

		printf("=== GHOST MATRIX ===\n\n");
		Ctrl_HostTask(ctrl, print_matrix, subselec_phantom_matrix1);
		Ctrl_HostTask(ctrl, print_matrix, subselec_phantom_matrix2);
		Ctrl_GlobalSync(ctrl);



		Ctrl_HostTask(ctrl, init_tile, subselec_phantom_matrix1);
		Ctrl_HostTask(ctrl, init_tile, subselec_phantom_matrix2);

		HitTile_long subselec_phantom_matrix11, subselec_phantom_matrix12, subselec_phantom_matrix21, subselec_phantom_matrix22;

		subselec_phantom_matrix11 = Ctrl_Select(ctrl, long, subselec_phantom_matrix1, hitShapeSize(1, SIZE), CTRL_SELECT_DEFAULT);
		subselec_phantom_matrix12 = Ctrl_Select(ctrl, long, subselec_phantom_matrix1, hitShapeSize(SIZE / 2, 1), CTRL_SELECT_DEFAULT);
		subselec_phantom_matrix21 = Ctrl_Select(ctrl, long, subselec_phantom_matrix2, hitShapeSize(1, SIZE / 2), CTRL_SELECT_DEFAULT);
		subselec_phantom_matrix22 = Ctrl_Select(ctrl, long, subselec_phantom_matrix2, hitShapeSize(SIZE, 1), CTRL_SELECT_DEFAULT);

		Ctrl_MoveTo(ctrl, subselec_phantom_matrix1);
		Ctrl_MoveTo(ctrl, subselec_phantom_matrix2);
		Ctrl_Launch(ctrl, big_kernel, subselec_phantom_matrix_threads1, CTRL_THREAD_NULL, subselec_phantom_matrix1, subselec_phantom_matrix2, subselec_phantom_matrix11, subselec_phantom_matrix21, subselec_phantom_matrix12, subselec_phantom_matrix22, subselec_phantom_matrix1, subselec_phantom_matrix2, subselec_phantom_matrix1, subselec_phantom_matrix2, subselec_phantom_matrix1, subselec_phantom_matrix2);
		Ctrl_MoveFrom(ctrl, subselec_phantom_matrix1);
		Ctrl_MoveFrom(ctrl, subselec_phantom_matrix2);

		printf("=== BIG KERNEL ===\n\n");
		Ctrl_HostTask(ctrl, print_matrix, subselec_phantom_matrix1);
		Ctrl_HostTask(ctrl, print_matrix, subselec_phantom_matrix2);
		Ctrl_GlobalSync(ctrl);



		printf("Phantom matrix: %p\n First half: %p (%p)\n Second half %p (%p)\n",
			phantom_matrix.data,
			subselec_phantom_matrix1.data, subselec_phantom_matrix1.memPtr,
			subselec_phantom_matrix2.data, subselec_phantom_matrix2.memPtr);

		Ctrl_Free(ctrl, matrix, subselec_whole, subselec_rows, subselec_cols, subselec_submatrix);
		Ctrl_Free(ctrl, phantom_matrix, subselec_phantom_matrix1, subselec_phantom_matrix2);
		Ctrl_Destroy(ctrl);
	}

	return 0;
}


