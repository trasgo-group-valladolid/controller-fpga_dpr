HitShape shp_jacobi2d9nc = hitShape( (-2, 2), (-2, 2) );

float patt_jacobi2d9nc [] = {
       0, 0, 1, 0, 0,
       0, 0, 1, 0, 0,
       1, 1, 1, 1, 1, 
       0, 0, 1, 0, 0,
       0, 0, 1, 0, 0 };

float factor_jacobi2d9nc = 9;

