#include "Ctrl.h"

#define H 1024
#define W 1024

#ifndef _CTRL_NEWTYPE_INT_
#define _CTRL_NEWTYPE_INT_
Ctrl_NewType(int);
#endif // _CTRL_NEWTYPE_INT_

CTRL_KERNEL_FUNCTION(Mandelbrot, PYNQ, DEFAULT, KTILE_ARGS(KHitTile_int mat), INT_ARGS(int threshold), FLOAT_ARGS(float x1, float x2, float y1, float y2));
