#define _CTRL_PYNQ_KERNEL_FILE_

#include "mandelbrot.h"

CTRL_KERNEL_FUNCTION(Mandelbrot, PYNQ, DEFAULT, KTILE_ARGS(KHitTile_int mat), INT_ARGS(int threshold), FLOAT_ARGS(float x1, float x2, float y1, float y2)) {
        DEF_KTILE_INTERFACES(mat);
        DEF_INT_INTERFACES(threshold);
        DEF_FLOAT_INTERFACES(x1,x2,y1,y2);
        DEF_RETURN_INTERFACE();

        KTILE(mat, int);
        
        float x0;
        float y0;

        float x = 0.0;
        float y = 0.0;
        int iteration;
        int sum = 0;

        int i, j;


        context_vars(i,j,sum);
        retrieve_context(sum);
        float xtemp;

        float xscale = (x2-x1)/(float)W;
        float yscale = (y2-y1)/(float)H;

        for_save(i, 0, H, 1) {
                for_save(j, 0, W, 1) {
                        x = 0.0;
                        y = 0.0;

                        x0 = x1 + xscale * (float)j;
                        y0 = y1 + yscale * (float)i;
                        iteration = 0;

                        while (x*x + y*y <= 4.0 && iteration < threshold) {
                                xtemp = x*x - y*y + x0;
                                y = 2.0*x*y + y0;
                                x = xtemp;
                                iteration++;
                        }
                        hit(mat, i * W + j) = iteration;
                        sum += iteration;

                        iteration = 0;
                        for_checkpoint(j, sum);
                }
                
                for_checkpoint(i)
        }

        ctrl_return(sum);
}
