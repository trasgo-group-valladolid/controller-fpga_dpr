#define _CTRL_PYNQ_KERNEL_FILE_

#include "jacobi.h"
#include <string.h>

CTRL_KERNEL_FUNCTION(Jacobi, PYNQ, DEFAULT, KTILE_ARGS(KHitTile_int in_matrix, KHitTile_int out_matrix), INT_ARGS(int iters), FLOAT_ARGS(NO_FLOAT_ARG)) {
  DEF_KTILE_INTERFACES(in_matrix, out_matrix);
  DEF_INT_INTERFACES(iters);
  DEF_FLOAT_INTERFACES(NO_FLOAT_INTERFACE);
  DEF_RETURN_INTERFACE();

  KTILE(in_matrix, int);
  KTILE(out_matrix, int);
  
  int weights[9];
  for(int i = 0; i < 9; i++) weights[i] = 0;
  weights[1] = 3;
  weights[3] = 3;
  weights[4] = 1;
  weights[5] = 3;
  weights[7] = 3;
  

  int sum;

  int up, down, left, right, current;

  int k, i, j;

  sum = 0;

  context_vars(k,i,j,sum);
  retrieve_context(sum);


  for_save(k, 0, iters, 1) {
    for_save(i, 1, H_NCOL-1, 1) {
        for_save(j, 1, H_NCOL-1, 1) {
          up = (i-1) * H_NCOL + j;
          down = (i+1) * H_NCOL + j;
          left = i * H_NCOL + (j-1);
          right = i * H_NCOL + (j+1);
          current = i * H_NCOL + j;
          hit(out_matrix, current) = k * weights[1] * hit(in_matrix, up) +
                                      k * weights[7] * hit(in_matrix, down) +
                                      k * weights[3] * hit(in_matrix, left) +
                                      k * weights[5] * hit(in_matrix, right) +
                                      k * weights[4] * hit(in_matrix, current) + i*j;

          sum = (sum + hit(out_matrix, current)) % 4379;
          
          for_checkpoint(j, sum);
        }
        for_checkpoint(i);
    }
    for_checkpoint(k);
  }

  ctrl_return(sum);
}
#undef _CTRL_PYNQ_KERNEL_FILE_
