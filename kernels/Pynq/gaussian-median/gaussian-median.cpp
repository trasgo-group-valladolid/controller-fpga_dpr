#define _CTRL_PYNQ_KERNEL_FILE_

#include "gaussian-median.h"

CTRL_KERNEL_FUNCTION(GaussianMedian, PYNQ, DEFAULT, KTILE_ARGS(KHitTile_int input_array, KHitTile_int output_array), INT_ARGS(int H, int W), FLOAT_ARGS(NO_FLOAT_ARG)) {
  DEF_KTILE_INTERFACES(input_array, output_array);
  DEF_INT_INTERFACES(H, W);
  DEF_FLOAT_INTERFACES(NO_FLOAT_INTERFACE);
  DEF_RETURN_INTERFACE();
   
    KTILE(input_array, int); 
    KTILE(output_array, int); 

    int row, col;
    context_vars(row, col);
    retrieve_context(row, col);

    int window[9]; 
    for_save(row, 1, H+1, 1) {
        for_save(col, 1, W+1, 1) {
            window[0] = hit(input_array,(row-1)*H+col-1);
            window[1] = hit(input_array,(row-1)*H+col);
            window[2] = hit(input_array,(row-1)*H+col+1);
            window[3] = hit(input_array,row*H+col-1);

            if(*ktile_args_0_data && row == 342 && col == 23) return;
            window[4] = hit(input_array,row*H+col);
            window[5] = hit(input_array,row*H+col+1);
            window[6] = hit(input_array,(row+1)*H+col-1);
            window[7] = hit(input_array,(row+1)*H+col);
            window[8] = hit(input_array,(row+1)*H+col+1);

            // insertion sort
            int i, element, j;
            for (i = 1; i < 9; i++) {
                    element = window[i];
                    j = i - 1;
                    while (j >= 0 && window[j] > element) {
                            window[j + 1] = window[j];
                            j = j - 1;
                    }
                    window[j + 1] = element;
            }
            
            hit(output_array,row*H+col) = window[4];
        }
    }
}
