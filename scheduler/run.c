#include "scheduler.h"
#ifdef _TEST_PARTIAL
#include <time.h>
#endif // _TEST_PARTIAL

//#include "kernels.h"

CTRL_HOST_TASK(Zero_Array, HitTile_int input_image, int H, int W) {
//void initialise_image(HitTile_int image, int H, int W, int init_zeros) {
    for(int i = 0; i < H; i++) {
        for(int j = 0; j < W; j++) {
                hit(input_image, i, j) = 0;
        }
    }
}

CTRL_HOST_TASK_PROTO( Zero_Array, 3,
    OUT, HitTile_int, in_matrix,
    INVAL, int, H,
    INVAL, int, W
);
 


void restore_context(schedulerTask * task, int rr, int n_rr) {
    if(task->preempted) {
#ifdef _DEBUG
        printf("(rr %d) Kernel %d was evicted before, running_task: %d\n\n", rr, task->id, FPGA.running_tasks[rr]->id);
#endif
        RR[rr].context = task->context;
        if(rr < n_rr)
            PYNQ_writeMMIO(&BRAMController[rr].bram, &RR[rr].context, 0x0, sizeof(struct context));

        #ifdef _DEBUG_PARTIAL
        // For testing purposes
        if (task->id == 0) {
        printf("Storing output of preempted task %d\n", task->id);
        printf("task_id: %d\n", task->id);
            for(int i = 0; i < H_NROW; i++) {
                for(int j = 0; j < H_NCOL; j++) {
                    // Save the generated output so far in a separate HitTile and clear the original so that 
                    // after the next execution of the HLS kernel the remaining image is generated there.
                    hit(partial_output, i, j) = hit(pregeneratedTasks.output_images[task->id], i, j);
                    //printf("%d ", hit(pregeneratedTasks.output_images[task->id], i, j));
                    //hit(pregeneratedTasks.output_images[task->id], i, j) = 0;
                    //hit(partial_output, i, j) = 0;
                }
            //printf("\n");
            }

            Ctrl_HostTask(CtrlConstructs.ctrl, Zero_Array, pregeneratedTasks.output_images[task->id], H_NROW, H_NCOL);
            Ctrl_GlobalSync(CtrlConstructs.ctrl);

            printf("Output after zeroing the matrix\n");
            _print_output(rr);

            for(int i = 0; i < H_NROW; i++) {
                for(int j = 0; j < H_NCOL; j++) {
                    if(hit(pregeneratedTasks.output_images[task->id], i, j) != 0)
                        printf("ERRROR!!!!!!\n");
                }
            }
        }



        #endif // _DEBUG_PARTIAL
        evicted_launch_task_id[rr] = FPGA.running_tasks[rr]->id;
            
    }
    else {
#ifdef _DEBUG
        printf("(restore_context) Before cleaining BRAM\n");
#endif
        clean_bram(rr);
#ifdef _DEBUG
        printf("(restore_context) After cleaining BRAM\n");
#endif
    }
}

void evict_kernel(int rr, int n_rr, schedulerTask * incoming_task) {
    if(rr < n_rr) {
#ifdef _DEBUG
        printf("(evict_kernel) Before reset\n");
#endif
        reset(rr);
#ifdef _DEBUG
        printf("(evict_kernel) After reset\n");
#elif _EXP_MODE
        fprintf(exp_file, "R %d %lf\n", rr, omp_get_wtime() - init_time);
#endif


        _print_output(rr);


        // The context is saved for future use when the task is swapped in again
        PYNQ_readMMIO(&BRAMController[rr].bram, &RR[rr].context, 0x0, sizeof(struct context));
        FPGA.running_tasks[rr]->context = RR[rr].context;
        FPGA.running_tasks[rr]->preempted = 1;

        enqueue(FPGA.running_tasks[rr]);


        evicted_launch_task_id[rr] = FPGA.running_tasks[rr]->id;

        restore_context(incoming_task, rr, n_rr);
#ifdef _DEBUG
        printf("(evict_kernel) Context after restoring, (incoming_task: %d, running_task: %d):\n", incoming_task->id, FPGA.running_tasks[rr]->id);
#elif _EXP_MODE
        fprintf(exp_file, "EV %d %d %d %lf\n", FPGA.running_tasks[rr]->id, incoming_task->id, rr, omp_get_wtime() - init_time);
#endif
#ifdef _DEBUG
        _print_output(rr);
#endif

        omp_unset_lock(&lock_RR[rr]);

        #pragma omp atomic write
        launched_kernel[rr] = 0;

    }
    else { // TODO: implement for CPU
        #pragma omp atomic write
        host_task_ready = 0;

#ifdef _DEBUG
        printf("(evict kernel) Before reset CPU\n");
#endif
        reset_host = 1;    
#ifdef _DEBUG
        printf("(evict kernel) After reset CPU\n");
#endif

        FPGA.running_tasks[rr]->context = RR[rr].context;
        FPGA.running_tasks[rr]->preempted = 1;

        enqueue(FPGA.running_tasks[rr]);

        evicted_host_task_id = FPGA.running_tasks[rr]->id;

        restore_context(incoming_task, rr, n_rr);
#ifdef _DEBUG
        printf("(evict_kernel) Context after restoring, (incoming_task: %d, running_task: %d) - CPU:\n", incoming_task->id, FPGA.running_tasks[rr]->id);
#endif

        reset_host = 0;
        #pragma omp atomic write
        host_task_ready = 1;
    }
#ifdef _DEBUG
    printf("evicted kernel %d to replace with %d\n", FPGA.running_tasks[rr]->id, incoming_task->id);
#endif
}


// TODO: add preemption
void serve_task(schedulerTask * task, int arrived, int use_CPU, int n_rr, int enabled_preemption) {
    if(task != NULL) {
#ifdef _DEBUG
        if(FPGA.running_tasks[0] != NULL && FPGA.running_tasks[1] != NULL)
            printf("(serve_task) arrived_task %d, running_tasks: %d, %d\n", task->id, FPGA.running_tasks[0]->id, FPGA.running_tasks[1]->id);
        else
            printf("(serve_task) arrived_task %d\n", task->id);
#endif
        int rr;
        int available_region = 0;

        // Try to find a region with no kernel running
        for(rr = 0; rr < ((use_CPU) ? N_RR+1 : n_rr); rr++) {
            if(FPGA.loaded_kernel_state[rr] == FINISHED) {
                available_region = 1;
                break;
            }
        }

        if(enabled_preemption && available_region) {
#ifdef _DEBUG
            printf("(serve_task) found an available region\n");
            _print_output(rr);
#endif
            restore_context(task, rr, n_rr);
#ifdef _DEBUG
            _print_output(rr);
#endif
        }

        // If a kernel that can be evicted was not found (just for preemption),
        // try to find a region with no kernel running
        if(enabled_preemption && !available_region) {
            // Restore context if the task was interrupted in the past
            for(rr = 0; rr < ((use_CPU) ? N_RR+1 : n_rr); rr++) {
                if(FPGA.loaded_kernel_state[rr] == RUNNING && task->priority > FPGA.running_tasks[rr]->priority) {
                    evict_kernel(rr, n_rr, task);
                   
                    //// Replace the context by the context of the new task 
                    //RR[rr].context = task->context;
                    //PYNQ_writeMMIO(&BRAMController[rr].bram, &RR[rr].context, 0x0, sizeof(struct context));

                    available_region = 1;
                    break;
                }
            }
        }

        
        if(available_region)
            run(task, rr, n_rr);
        else
            enqueue(task);

#ifdef _DEBUG
        printf("(serve_task) bye\n");
#endif
    }
}

int reset_host = 0;

void swap(schedulerTask * task, int rr) {
    int do_swap = 0;

    if(task->k_data.task_type <= 2 && FPGA.loaded_kernels[rr] == GAUSSIAN) {
        FPGA.loaded_kernels[rr] = MEDIAN;
        do_swap = 1;
    }
    else if(task->k_data.task_type == 3 && FPGA.loaded_kernels[rr] == MEDIAN)  {
        FPGA.loaded_kernels[rr] = GAUSSIAN;
        do_swap = 1;
    }

    if(do_swap) {
        char bitname[512];
        //sprintf(bitname, "/home/xilinx/controller-DPR/controllers/Bitstreams/%s-%s_pblock_%s_partial.bit", kernel_name[FPGA.loaded_kernels[0]], kernel_name[FPGA.loaded_kernels[1]], FPGA.pblock_names[rr]);
        sprintf(bitname, "/home/xilinx/Bitstreams-axi_firewall/%s-%s_pblock_%s_partial.bit", kernel_name[FPGA.loaded_kernels[0]], kernel_name[FPGA.loaded_kernels[1]], FPGA.pblock_names[rr]);
        //sprintf(bitname, "/home/xilinx/Bitstreams-shift_median/%s-%s_pblock_%s_partial.bit", kernel_name[FPGA.loaded_kernels[0]], kernel_name[FPGA.loaded_kernels[1]], FPGA.pblock_names[rr]);
#ifdef _DEBUG
        printf("(rr: %d) %s\n", task->rr, bitname);
#elif _EXP_MODE
        fprintf(exp_file, "CrS %s %s %s %lf\n", kernel_name[FPGA.loaded_kernel_state[0]], kernel_name[FPGA.loaded_kernel_state[1]], FPGA.pblock_names[rr], omp_get_wtime() - init_time);
#endif
        Ctrl_Task ctrl_task = Ctrl_KernelTaskCreate_Swap(task->rr, bitname);
        Ctrl_LaunchKernel(CtrlConstructs.ctrl, ctrl_task);
    }
}

void launch(schedulerTask * p_task, int rr, int n_rr) {
    // TODO: receive as an argument when preemption is implemented.
    //int reset_host = 0;

    if(rr < n_rr)
        current_launch_task_id = p_task->id;
    else
        current_host_task_id = p_task->id;

#ifdef _DEBUG
    printf("Before launching. current_launch_task_id: %d\n", current_launch_task_id);
#endif

    #ifdef _TEST_PARTIAL

    #ifdef _TEST_PARTIAL_GAUSSIANBLUR
    p_task->k_data.task_type = 3;
    #elif _TEST_PARTIAL_MEDIANBLUR
    p_task->k_data.task_type = 2;
    p_task->k_data.iters = 3;
    #endif

    // for(int i = 0; i < 10; i++) {
    //     RR[rr].context.var[i] = 200;
    //     RR[rr].context.saved[i] = 1;
    // }
    // PYNQ_writeMMIO(&BRAMController[rr].bram, &RR[rr].context, 0x0, sizeof(struct context));
    #endif // _TEST_PARTIAL


    (*f_launch[p_task->k_data.task_type])(p_task, CtrlConstructs.ctrl, CtrlConstructs.threads, CtrlConstructs.group);

    if(rr < n_rr) {
        #pragma omp atomic write
        launched_kernel[rr] = 1;
    }

    FPGA.running_tasks[rr] = p_task;
    FPGA.loaded_kernel_state[rr] = RUNNING;

    // For testing partial
    #ifdef _TEST_PARTIAL
    struct timespec time_rst = {
        .tv_sec = 0,
        .tv_nsec = 100000000
    };

    nanosleep(&time_rst, NULL);

    // We're only executing one task for this demonstration, therefore it's run on RR 0
    reset(0);

    printf("Output after reset\n");
    _print_output(0);

    for(int i = 0; i < H_NROW; i++) {
        for(int j = 0; j < H_NCOL; j++) {
            // Save the generated output so far in a separate HitTile and clear the original so that 
            // after the next execution of the HLS kernel the remaining image is generated there.
            hit(partial_output, i, j) = hit(pregeneratedTasks.output_images[0], i, j);
            //printf("%d ", hit(pregeneratedTasks.output_images[task->id], i, j));
            //hit(pregeneratedTasks.output_images[task->id], i, j) = 0;
            //hit(partial_output, i, j) = 0;
        }
        //printf("\n");
    }

    Ctrl_HostTask(CtrlConstructs.ctrl, Zero_Array, pregeneratedTasks.output_images[0], H_NROW, H_NCOL);
    Ctrl_GlobalSync(CtrlConstructs.ctrl); 

    omp_unset_lock(&lock_RR[rr]);

    #pragma omp atomic write
    launched_kernel[rr] = 0;
    //if(rr < n_rr) {
    //    #pragma omp atomic write
    //    launched_kernel[rr] = 0;
    //}

    (*f_launch[p_task->k_data.task_type])(p_task, CtrlConstructs.ctrl, CtrlConstructs.threads, CtrlConstructs.group);

    if(rr < n_rr) {
        #pragma omp atomic write
        launched_kernel[rr] = 1;
    }
    #endif // _TEST_PARTIAL
    ////////////////////////////////////
}

void run(schedulerTask * task, int rr, int n_rr) {
    task->rr = rr;

    if(rr < n_rr)
        swap(task, rr);
   
    launch(task, rr, n_rr); 
}

// For testing purposes
//void run_host_task(int n_task, int kernel) {
//    int * rr = (int*)malloc(sizeof(int));    
//    int * iters = (int*)malloc(sizeof(int));
//    *rr = 2;
//    *iters = 2;
//
//    if(kernel == 0) {
//        Ctrl_HostTask(CtrlConstructs.ctrl, MedianBlur, *rr, reset_host, pregeneratedTasks.input_images[n_task], pregeneratedTasks.output_images[n_task], H, W, *iters); 
//    }
//    else if (kernel == 1) {
//        Ctrl_HostTask(CtrlConstructs.ctrl, GaussianBlur, *rr, reset_host, pregeneratedTasks.input_images[n_task], pregeneratedTasks.output_images[n_task], H, W);
//    }
//}
//
//void run_kernel_task(int n_task) {
//    int * rr = (int*)malloc(sizeof(int));    
//    *rr = 1;
//     
//    Ctrl_Launch(CtrlConstructs.ctrl, GaussianBlur, CtrlConstructs.threads, CtrlConstructs.group, *rr, pregeneratedTasks.input_images[n_task], pregeneratedTasks.output_images[n_task], H, W);
//}
