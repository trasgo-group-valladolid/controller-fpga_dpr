#include "scheduler.h"

void PYNQ_scheduler(PCtrl ctrl, Ctrl_Thread threads, Ctrl_Thread group, int n_tasks, int use_CPU, int n_rr, int enabled_priorities, int enabled_preemption) {
    setup_PYNQ_scheduler(ctrl, threads, group, n_tasks, use_CPU, n_rr);

    int last_arrived = 0;
    schedulerTask * task;
    int arrived;
    int tasks_to_arrive = 1;


    update_timeout(&timeout, last_arrived);

    while(1) {
        wait_finish(&timeout);
        context_switch_thread();
        if(has_finished(n_tasks, use_CPU, n_rr, &tasks_to_arrive))
            break;

        if(tasks_to_arrive && timeout.tv_sec == 0 && timeout.tv_usec == 0) {
#ifdef _DEBUG
            printf("TIMEOUT!\n");
#endif
            task = get_arrived_task();
#ifdef _EXP_MODE
            fprintf(exp_file, "ARR %d %d %lf\n", task->priority, task->id, omp_get_wtime() - init_time);
#endif
            last_arrived++;
            arrived = 1;
        }
        else {
            task = get_task_from_queue(enabled_priorities, enabled_preemption, use_CPU);
            arrived = 0;
        }
        serve_task(task, arrived, use_CPU, n_rr, enabled_preemption);
        update_timeout(&timeout, last_arrived);
#ifdef _EXP_MODE
        printf("---- End of scheduler iteration | last arrived: %d ----\n", last_arrived);
#endif
    }

#ifdef _EXP_MODE
    fprintf(exp_file, "END %lf\n", omp_get_wtime() - init_time);
    fclose(exp_file);
#endif
}
